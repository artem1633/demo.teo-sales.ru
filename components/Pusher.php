<?php

namespace app\components;

use yii\base\Component;
use yii\base\InvalidConfigException;

class Pusher extends Component
{
	/**
	 * @var String Api ключ от Firebase Cloud messages service
	 */
	public $apiKey;

	/**
	 * @var String Url для отправки пушей 
	 */
	public $pushUrl = 'https://fcm.googleapis.com/fcm/send';

	/**
	 * @var array Получатели
	 */
	protected $receivers = [];

	/**
	 * @var array Тело сообщения
	 */
	protected $message;


	public function init()
	{
		if($this->apiKey === null)
			throw new InvalidConfigException('option $apiKey must be not empty');

		parent::init();
	}

	/**
	 *	Добавляет токен или токены, владельцам которых будут отправлены уведомления
	 *  @param String|array $receivers
	 *	@return \app\components\Pusher $this
	 */
	public function addReceivers($receivers)
	{
		if(is_array($receivers)){
			$this->receivers = array_merge($this->receivers, $receivers);
		} else {
			$this->receivers[] = $receivers;
		}

		return $this;
	}

	/**
	 * Устанавливает тело сообщения
	 * @param array $message Тело сообщения
	 * - `title`: _string_, Заголовок пуша
	 * - `body`: _string_, Сообщение пуша
	 * - `icon`: _string_, Ссылка на изображение пуша
	 * - `click_action`: _string_, Ссылка, по которой пользователь переходит
	 * при нажатии на пуш
	 * @return \app\components\Pusher $this
	 */
	public function compose($message)
	{
		$this->message = $message;
		return $this;
	}

	/**
	 * Отправляет сообщения, возвращая ответ от сервера Firebase
	 * @return array
	 */
	public function send()
	{
		$result = [];

        foreach($this->receivers as $token)
        {
          $request_body = [
              'to' => $token,
              'notification' => $this->message,
          ];
          $fields = json_encode($request_body);

          $request_headers = [
              'Content-Type: application/json',
              'Authorization: key=' . $this->apiKey,
          ];

          try{
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $this->pushUrl);
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
              curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
              $response = curl_exec($ch);
              $result[] = $response;
              curl_close($ch);
          } catch (\Exception $e) {
              // TODO: EXCEPTION HANDLING
          }
        }

        return $result;
	}

}

?>