<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property int $id
 * @property string $answer0
 * @property int $typorgId
 * @property int $orgId
 * @property int $questId
 * @property int $subquestId
 * @property int $depId
 * @property int $userId
 * @property string $createdate
 *
 * @property Questions $quest
 * @property Subquestions $subquest
 * @property Departments $dep
 * @property Organization $org
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typorgId', 'orgId', 'questId', 'subquestId', 'depId', 'userId'], 'integer'],
            [['questId', 'subquestId', 'depId', 'userId', 'createdate'], 'required'],
            [['createdate'], 'safe'],
            [['answer0'], 'string', 'max' => 300],
            [['questId'], 'exist', 'skipOnError' => true, 'targetClass' => Questions::className(), 'targetAttribute' => ['questId' => 'id']],
            [['subquestId'], 'exist', 'skipOnError' => true, 'targetClass' => Subquestions::className(), 'targetAttribute' => ['subquestId' => 'Id']],
            [['depId'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['depId' => 'Id']],
            [['orgId'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['orgId' => 'Id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer0' => 'Answer0',
            'typorgId' => 'Typorg ID',
            'orgId' => 'Org ID',
            'questId' => 'Quest ID',
            'subquestId' => 'Subquest ID',
            'depId' => 'Dep ID',
            'userId' => 'User ID',
            'createdate' => 'Createdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuest()
    {
        return $this->hasOne(Questions::className(), ['id' => 'questId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubquest()
    {
        return $this->hasOne(Subquestions::className(), ['Id' => 'subquestId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDep()
    {
        return $this->hasOne(Departments::className(), ['Id' => 'depId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(Organization::className(), ['Id' => 'orgId']);
    }
}
