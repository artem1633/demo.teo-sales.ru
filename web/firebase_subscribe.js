  var config = {
    apiKey: "AIzaSyC-joKNd81dky4eFYD4ABDW0WUpEvRBhTY",
    authDomain: "zvonki-e53ac.firebaseapp.com",
    databaseURL: "https://zvonki-e53ac.firebaseio.com",
    projectId: "zvonki-e53ac",
    storageBucket: "zvonki-e53ac.appspot.com",
    messagingSenderId: "554503147703"
  };
  firebase.initializeApp(config);

  var messaging = firebase.messaging();


  messaging.requestPermission()
  	.then(function() {
  		console.log('Have permission');
  		return messaging.getToken();
  	})
  	.then(function(token){
  		console.log(token);

      if(token){
        console.log('okey');
        sendTokenToServer(token);
      } else {
        setTokenSentToServer(false);
      }
  	})
  	.catch(function(){
  		console.log('Error Ocured');
  	});

 messaging.onMessage(function(payload){
 	console.log(payload);
 });


 function sendTokenToServer(currentToken){
    if(!isTokenSentToServer(currentToken)) {
      console.log('Регистрация токена');

      var url = 'https://zvonki.teo-stroy.ru/site/save-token';
      var csrfToken = $('meta[name="csrf-token"]').attr('content');

      $.post(url, {
          '_csrf': csrfToken,
          'token': currentToken
      });

      setTokenSentToServer(currentToken);
    } else {
      console.log('Токен уже зарегистрирован');
    } 
 }

 // используем localStorage для отметки того,
 // что пользователь уже подписался на уведомления
 function isTokenSentToServer(currrentToken){
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currrentToken;
 }

 function setTokenSentToServer(currrentToken){
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currrentToken ? currrentToken : ''
      );
 } 