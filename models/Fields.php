<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\behaviors\BlameableBehavior;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "fields".
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $type
 * @property string $data
 * @property int $project_id
 * @property int $client_id
 * @property int $company_id
 *
 * @property Clients $client
 * @property Companies $company
 * @property Project $project
 */
class Fields extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }


    const TYPE_TEXT = 'text';
    const TYPE_NUMBER = 'number';
    const TYPE_DROPDOWN = 'dropdown';
    public $datas;

    public static function tableName()
    {
        return 'fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'string'],
            ['name', 'match', 'pattern' => '/^[a-z._-]+$/i', 'message' => '{attribute} должно состоять только из английских букв и символов . - _ а так же не должно быть с пробелом'],
            //[['name'], 'unique'],
            [['name', 'label', 'type', 'project_id'], 'required'],
            [['project_id', 'company_id'], 'integer'],
            [['name', 'label', 'type'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Ключ',
            'label' => 'Наименование',
            'type' => 'Тип поля',
            'data' => 'Вспомогательные поля',
            'project_id' => 'Проект',
            'company_id' => 'Компания',
            'datas' => 'Вспомогательные поля',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsFields()
    {
        return $this->hasMany(ClientsField::className(), ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentsFields()
    {
        return $this->hasMany(DocumentsField::className(), ['field_id' => 'id']);
    }

    public static function getTypesList()
    {
        return [
            self::TYPE_TEXT => 'Текст',
            self::TYPE_NUMBER => 'Число',
            self::TYPE_DROPDOWN => 'Выпадающий список',
        ];
    }

    public function getDescription()
    {        
        if( 'text' == $this->type) return 'Текст';
        if( 'number' == $this->type) return 'Число';
        if( 'dropdown' == $this->type) return 'Выпадающий список';        
    }

    public function getProjectList()
    {
        $project = Project::find()/*->where(['company_id' => Yii::$app->user->identity->company_id ])*/->all();
        return ArrayHelper::map($project, 'id', 'name');
    }
}
