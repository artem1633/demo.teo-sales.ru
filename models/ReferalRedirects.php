<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "referal_redirects".
 *
 * @property int $id
 * @property int $refer_company_id Компания
 * @property string $ip IP-адрес
 * @property string $user_agent Браузер
 * @property string $created_at Дата и время
 *
 * @property Companies $referCompany
 */
class ReferalRedirects extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referal_redirects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refer_company_id'], 'integer'],
            [['created_at'], 'safe'],
            [['ip', 'user_agent'], 'string', 'max' => 255],
            [['refer_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['refer_company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'refer_company_id' => 'Refer Company ID',
            'ip' => 'Ip',
            'user_agent' => 'User Agent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferCompany()
    {
        return $this->hasOne(Users::className(), ['id' => 'refer_company_id']);
    }
}
