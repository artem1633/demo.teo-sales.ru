<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
 * ClientsSearch represents the model behind the search form about `app\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'menejer','project_id', 'client'], 'integer'],
            [['date_cr', 'fio', 'city', 'telephone', 'email', 'comment', 'planned_date', 'status', 'source_id', 'company_id',  'lid_id', 'date_up'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $statuses = UsersStatuses::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
        $status = [];
        foreach ($statuses as $value) {
            $status [] = $value->status_id;
        }

        $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
        $project = [];
        foreach ($projects as $value) {
            $project [] = $value->project_id;
        }

        /*echo "<pre>";
        print_r($params);
        echo "</pre>";
        echo "phone = ".$params['ClientsSearch']['telephone'];*/
        if($params['ClientsSearch']['telephone'] != null)
        {
            if(Yii::$app->user->identity->type == 1) $clients = Clients::find()->where(['client' => 1])->all();
            else {
                if($status == null) $clients = Clients::find()->where(['project_id' => $project, 'client' => 1 ])->all();
                else $clients = Clients::find()->where(['status' => $status, 'project_id' => $project, 'client' => 1 ])->all();
            }
            $result = [];
            foreach ($clients as $client) {
                $contact = Contacts::find()->where(['client_id' => $client->id])->andWhere(['like', 'phone', '%'.$params['ClientsSearch']['telephone'].'%', false])->one();
                if($contact != null) $result [] = $client->id;
            }

            $query = Clients::find()->where(['clients.id' => $result]);
            $this->load($params);
        }
        else
        {
            if(Yii::$app->user->identity->type == 1) $query = Clients::find()->where(['client' => 1]);
            else {
                if($status == null) {
                    $query = Clients::find()->where(['project_id' => $project, 'client' => 1, 'status' => null ]);
                }
                else $query = Clients::find()->where(['status' => $status, 'project_id' => $project, 'client' => 1 ]);
            }
            $this->load($params);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 40],
            'sort' => [
                'defaultOrder' => [
                    'date_up' => SORT_DESC,
                ],
            ],
        ]);


        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('status0');
        $query->joinWith('source');
        $query->joinWith('project');

        $query->joinWith('company.admin');

        $query->andFilterWhere([
            'clients.id' => $this->id,
            'clients.date_cr' => $this->date_cr,
            'clients.date_up' => $this->date_up,
            'clients.menejer' => $this->menejer,
            'clients.client' => $this->client,
            'clients.planned_date' => $this->planned_date,
            'clients.company_id' => $this->company_id,
            'project.id' => $this->project_id,
        ]);

        $query->andFilterWhere(['like', 'clients.fio', $this->fio])
            ->andFilterWhere(['like', 'clients.city', $this->city])
            ->andFilterWhere(['like', 'statuses.name', $this->status])
            ->andFilterWhere(['like', 'source.name', $this->source_id])
            ->andFilterWhere(['like', 'clients.comment', $this->comment]);


        return $dataProvider;
    }

    public function searchLids($params)
    {
        $statuses = UsersLidStatuses::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
        $status = [];
        foreach ($statuses as $value) {
            $status [] = $value->status_lid_id;
        }

        $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
        $project = [];
        foreach ($projects as $value) {
            $project [] = $value->project_id;
        }

        if($params['ClientsSearch']['telephone'] != null)
        {
            if(Yii::$app->user->identity->type == 1) $clients = Clients::find()->where(['client' => 0])->all();
            else {
                if($status == null) $clients = Clients::find()->where([ 'project_id' => $project , 'client' => 0])->all();
                else $clients = Clients::find()->where(['lid_id' => $status, 'project_id' => $project , 'client' => 0])->all();
            }

            $result = [];
            foreach ($clients as $client) {
                $contact = Contacts::find()->where(['client_id' => $client->id])->andWhere(['like', 'phone', '%'.$params['ClientsSearch']['telephone'].'%', false])->one();
                if($contact != null) $result [] = $client->id;
            }

            $query = Clients::find()->where(['clients.id' => $result]);

        }
        else
        {
            if(Yii::$app->user->identity->type == 1) $query = Clients::find()->where(['client' => 0]);
            else {
                if($status == null) $query = Clients::find()->where([ 'project_id' => $project , 'client' => 0, 'lid_id' => null ]);
                else $query = Clients::find()->where(['lid_id' => $status, 'project_id' => $project , 'client' => 0]);
            }
            
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 40],
            'sort' => [
                'defaultOrder' => [
                    'date_up' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //$query->joinWith('statusLid');
        $query->joinWith('source');
        $query->joinWith('project');

        $query->joinWith('company.admin');

        $query->andFilterWhere([
            'clients.id' => $this->id,
            'clients.date_cr' => $this->date_cr,
            'clients.date_up' => $this->date_up,
            'clients.menejer' => $this->menejer,
            'clients.client' => $this->client,
            'clients.planned_date' => $this->planned_date,
            'clients.company_id' => $this->company_id,
            'clients.lid_id' => $this->lid_id,
            'project.id' => $this->project_id,
        ]);

        $query->andFilterWhere(['like', 'clients.fio', $this->fio])
            ->andFilterWhere(['like', 'clients.city', $this->city])
            //->andFilterWhere(['like', 'lid.name', $this->lid_id])
            ->andFilterWhere(['like', 'source.name', $this->source_id])
            ->andFilterWhere(['like', 'clients.comment', $this->comment]);


        return $dataProvider;
    }
}
