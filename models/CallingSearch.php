<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calling;

/**
 * CallingSearch represents the model behind the search form about `app\models\Calling`.
 */
class CallingSearch extends Calling
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'user_id'], 'integer'],
            [['fio', 'status','city', 'email', 'telephone', 'planned_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calling::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 40],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('status0');

        $query->andFilterWhere([
            'calling.id' => $this->id,
            //'status' => $this->status,
            'calling.user_id' => $this->user_id,
            'calling.planned_date' => $this->planned_date,
        ]);

        $query->andFilterWhere(['like', 'calling.fio', $this->fio])
            ->andFilterWhere(['like', 'calling.city', $this->city])
            ->andFilterWhere(['like', 'statuses.name', $this->status])
            ->andFilterWhere(['like', 'calling.email', $this->email])
            ->andFilterWhere(['like', 'calling.telephone', $this->telephone]);

        return $dataProvider;
    }
}
