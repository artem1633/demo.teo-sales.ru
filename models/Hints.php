<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lessons".
 *
 * @property int $id
 * @property int $group_id Група
 * @property int $step № шага
 * @property string $hint Текст подсказки
 */
class Hints extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','hint'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'hint' => 'Текст подсказки',
        ];
    }

    public static function getHintsById($id)
    {
        return static::findOne($id);
    }
}
