<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Class UsersQuery
 * @package app\models
 * @see \app\models\Users
 */
class UsersQuery extends ActiveQuery
{
    /**
	 * Делает выборку пользователей у которых присутствует FCM токен
	 * @return $this
	 */
	public function hasToken()
	{
		return $this->andWhere(['IS NOT', 'fcm_token', null]);
	}

    /**
     * Делает выборку пользователей, которые не состаят ни в одной из компаний
     * @return $this
     */
    public function noCompany()
    {
        // TODO: noCompany()
    }
}



?>