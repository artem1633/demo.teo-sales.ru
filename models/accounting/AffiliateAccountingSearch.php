<?php

namespace app\models\accounting;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AffiliateAccounting;

/**
 * AffiliateAccountingSearch represents the model behind the search form about `app\models\AffiliateAccounting`.
 */
class AffiliateAccountingSearch extends AffiliateAccounting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'referal_id'], 'integer'],
            [['date_transaction'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AffiliateAccounting::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_transaction' => $this->date_transaction,
            'company_id' => $this->company_id,
            'referal_id' => $this->referal_id,
            'amount' => $this->amount,
        ]);

        return $dataProvider;
    }
}
