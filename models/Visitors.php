<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "visitors".
 *
 * @property int $id
 * @property int $site_id
 * @property string $ip
 * @property string $number
 * @property string $utm
 * @property string $link
 * @property string $date_cr
 *
 * @property Sites $site
 */
class Visitors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_id'], 'integer'],
            [['site_id'], 'required'],
            [['date_cr'], 'safe'],
            [['ip', 'number', 'utm', 'link'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sites::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Сайт',
            'ip' => 'Ip',
            'number' => 'Номер посещения',
            'utm' => 'Utm',
            'link' => 'Link',
            'date_cr' => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_cr = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Sites::className(), ['id' => 'site_id']);
    }

    public function getSitesList()
    {
        $sites = Sites::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($sites, 'id', 'name');
    }
}
