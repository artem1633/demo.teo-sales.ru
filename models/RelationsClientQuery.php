<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Class RelationsClientQuery
 * @package app\models
 * @see \app\models\Clients
 */
class RelationsClientQuery extends ActiveQuery
{
    /** @var integer Если пользователь является членом какой либо компании, то он должен видеть только своих конрагентов
     *  Эта переменная хранит в себе id компании к которой привязан пользователь
     */
    public $companyId;

    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        if($this->companyId != null)
        {
            $this->joinWith('createdBy');
            $this->andWhere(['users.parent_company_id' => $this->companyId]);
            $this->orWhere(['users.id' => $this->companyId]);
        }

        return parent::all($db);
    }
}