<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "sites".
 *
 * @property int $id
 * @property string $name
 * @property int $source_id
 * @property int $project_id
 * @property int $status
 * @property int $menejer
 * @property string $key
 * @property int $company_id
 *
 * @property Companies $company
 * @property Users $menejer0
 * @property Source $source
 * @property Statuses $status0
 */
class Sites extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $applications_user;
    public $visitors_user;
    public static function tableName()
    {
        return 'sites';
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }
    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status',], 'required'],
            [['name'], 'unique'],
            [['source_id', 'project_id', 'status', 'menejer', 'company_id', 'new_visitor', 'new_application', 'notice_vk', 'notice_telegram'/*, 'applications_user', 'visitors_user'*/], 'integer'],
            [['name', 'key', 'url'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['menejer'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['menejer' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Source::className(), 'targetAttribute' => ['source_id' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => StatusLid::className(), 'targetAttribute' => ['status' => 'id']],
            /*[['applications_user'], 'required', 'when' => function() {
                   if($this->new_application == 1) return TRUE;
                   else return FALSE;
            }],
            [['visitors_user'], 'required', 'when' => function() {
                   if($this->new_visitor == 1) return TRUE;
                   else return FALSE;
            }],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование сайта',
            'source_id' => 'Источник',
            'project_id' => 'Проект',
            'status' => 'Статус',
            'menejer' => 'Менеджер',
            'key' => 'Ключ',
            'company_id' => 'Компания',
            'new_visitor' => 'Уведомления о новых посетителях',
            'new_application' => 'Уведомления о заявках',
            'notice_vk' => 'Уведомления в вк',
            'notice_telegram' => 'Уведомления в телеграмм',
            'applications_user' => 'Получатель оповещения о заявках',
            'visitors_user' => 'Получатель оповещения о посетителях',
            'url' => 'Ссылка',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->key = md5($this->name . $this->company->name);            
        }
        
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenejer0()
    {
        return $this->hasOne(Users::className(), ['id' => 'menejer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(StatusLid::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitesUsers()
    {
        return $this->hasMany(SitesUsers::className(), ['site_id' => 'id']);
    }

    public function getManager()
    {
        $user = Yii::$app->user->identity;
        $companyId = $user->getCompany();

        if($companyId != null) {
            $managers = Users::find()->where(['type' => 2])->andWhere(['or', ['company_id' => $companyId], ['id' => $companyId]])->all();
        } else {
            $managers = Users::find()->where(['type' => 2])->all();
        }
        return ArrayHelper::map($managers, 'id', 'fio');
    }

    public function getUsersList()
    {
        $user = Yii::$app->user->identity;
        $companyId = $user->getCompany();

        if($companyId != null) {
            $users = Users::find()->where(['or', ['company_id' => $companyId], ['id' => $companyId]])->all();
        } else {
            $users = Users::find()->all();
        }
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public function ApplicationsUsersList()
    {
        $user = Yii::$app->user->identity;
        $companyId = $user->getCompany();
        $users = SitesUsers::find()->where(['type' => 'application', 'site_id' => $this->id])->all();
        $result = [];

        foreach ($users as $value) {
            $result [] = $value->user_id;
        }
        return $result;
    }

    public function VisitorsUsersList()
    {
        $user = Yii::$app->user->identity;
        $companyId = $user->getCompany();
        $users = SitesUsers::find()->where(['type' => 'visitor', 'site_id' => $this->id])->all();
        $result = [];

        foreach ($users as $value) {
            $result [] = $value->user_id;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        $managers = StatusLid::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($managers, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getProjects()
    {
        $projects = Project::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($projects, 'id', 'name');
    }
    /**
     * @return array
     */
    public function getSourceList()
    {
        $managers = Source::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($managers, 'id', 'name');
    }
}
