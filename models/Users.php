<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $type
 * @property string $fcm_token
 * @property string $temporary_access_code
 * @property string $phone
 * @property string $position
 * @property integer $company_id
 * @property string $last_activity_datetime
 *
 * @property Calling[] $callings
 * @property Clients[] $clients
 * @property Companies $company
 */
class Users extends \yii\db\ActiveRecord
{

    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_MANAGER = 2;
    const USER_TYPE_PARTNER = 3;

    public $new_password;
    public $projects;
    public $statuses;
    public $lid_statuses;
    private $_models;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'password'], 'required'],
            [['company_id'], 'integer'],
            [['type'], 'default', 'value' => self::USER_TYPE_MANAGER],
            [['fio', 'login', 'password', 'new_password', 'temporary_access_code', 'fcm_token', 'phone', 'position', 'vk_id', 'telegram_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    /*public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Должность',
            'new_password' => 'Новый пароль',
            'company_id' => 'Компания, к которой принадлежит пользователь',
            'temporary_access_code' => 'Временый код доступа',
            'phone' => 'Телефон',
            'position' => 'Должность',
            'last_activity_datetime' => 'Последняя активность',
            'vk_id' => 'Ид VK',
            'telegram_id' => 'Телеграм ид',
            'projects' => 'Проекты',
            'statuses' => 'Статусы',
            'lid_statuses' => 'Статусы лида',
            'register_date' => 'Дата регистрации',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $user = Yii::$app->user->identity;
            $this->password = md5($this->password);
            $this->register_date = date("Y-m-d H:i:s");
        }

        if($this->new_password != null) $this->password = md5($this->new_password);
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->unlinkAll('clients');
        $this->unlinkAll('callings');

        return parent::beforeDelete();
    }

    /**
     * @return array
     */
    public function getType()
    {
        return ArrayHelper::map([
            ['id' => self::USER_TYPE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_TYPE_MANAGER, 'name' => 'Менеджер',],
            ['id' => self::USER_TYPE_PARTNER, 'name' => 'Партнер',],
        ], 'id', 'name');
    }

    public static function getAttributesValues($models, $attribute)
    {
        if(count($models) === 0)
        {
            $models = self::find()->all();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallings()
    {
        return $this->hasMany(Calling::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['menejer' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPays()
    {
        return $this->hasMany(Pays::className(), ['user_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentations()
    {
        return $this->hasMany(Documentation::className(), ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Sites::className(), ['menejer' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitesUsers()
    {
        return $this->hasMany(SitesUsers::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersProjects()
    {
        return $this->hasMany(UsersProjects::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersStatuses()
    {
        return $this->hasMany(UsersStatuses::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersLidStatuses()
    {
        return $this->hasMany(UsersLidStatuses::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['user_id' => 'id']);
    }
    public function UsersProjectList()
    {
        $users = UsersProjects::find()->where(['user_id' => $this->id])->all();
        $result = [];

        foreach ($users as $value) {
            $result [] = $value->project_id;
        }
        return $result;
    }

    public function getProjectsList()
    {
        $projects = Project::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($projects, 'id', 'name');
    }

    public function UsersStatusList()
    {
        $status = UsersStatuses::find()->where(['user_id' => $this->id])->all();
        $result = [];

        foreach ($status as $value) {
            $result [] = $value->status_id;
        }
        return $result;
    }

    public function UsersLidStatusList()
    {
        $status = UsersLidStatuses::find()->where(['user_id' => $this->id])->all();
        $result = [];

        foreach ($status as $value) {
            $result [] = $value->status_lid_id;
        }
        return $result;
    }

    public function getStatusList()
    {
        $statuses = Statuses::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($statuses, 'id', 'name');
    }

    public function getLidStatusList()
    {
        $statuses = StatusLid::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($statuses, 'id', 'name');
    }

    public static function getAllInArray()
    {
        $companies = self::find()
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'fio');
    }
    public static function getAllAdminInArray()
    {
        $companies = self::find()->where(['type'=>self::USER_TYPE_ADMIN])
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'fio');
    }

}
