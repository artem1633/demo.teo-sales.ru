<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RelationsClient;

/**
 * RelationsClientSearch represents the model behind the search form about `app\models\RelationsClient`.
 */
class RelationsClientSearch extends RelationsClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'relation', 'manager', 'client'], 'integer'],
            [['data', 'time', 'text', 'date_cr', 'date_up'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->identity->type == 1 || (Yii::$app->user->identity->type == 3)) $query = RelationsClient::find();
        else if (Yii::$app->user->identity->type == 2) $query = RelationsClient::find()->where(['manager' => Yii::$app->user->identity->id]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'time' => $this->time,
            'relation' => $this->relation,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'manager' => $this->manager,
            'client' => $this->client,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
