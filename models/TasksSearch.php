<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'manager', 'client'], 'integer'],
            [['data', 'time', 'text', 'date_cr', 'date_up', 'result_text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$post)
    {
        if(Yii::$app->user->identity->type == 1 || (Yii::$app->user->identity->type == 3)) $query = Tasks::find();
        else if (Yii::$app->user->identity->type == 2) $query = Tasks::find()->where(['manager' => Yii::$app->user->identity->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 40],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'data' => $this->data,
            'time' => $this->time,
            //'status' => $this->status,
            'date_cr' => $this->date_cr,
            'date_up' => $this->date_up,
            'manager' => $this->manager,
            'client' => $this->client,
        ]);

        $query->andFilterWhere(['status' => $post['TasksSearch']['search_status']]);
        if($post['date_time_from'] != null) 
            $query->andFilterWhere(['between', 'data', date('Y-m-d',(strtotime($post['date_time_from']))) ,date('Y-m-d',(strtotime($post['date_time_to']) +86399 ))]);
        if($post == null)  $query->andFilterWhere([ 'data' => date('Y-m-d') ]);

        $query->andFilterWhere(['like', 'text', $this->text])
              ->andFilterWhere(['like', 'result_text', $this->result_text]);

        return $dataProvider;
    }
}
