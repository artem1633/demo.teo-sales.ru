<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents_field".
 *
 * @property int $id
 * @property int $document_id
 * @property int $field_id
 * @property string $value
 *
 * @property DocumentTemplates $document
 * @property Fields $field
 */
class DocumentsField extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_id', 'field_id'], 'integer'],
            [['value'], 'string', 'max' => 500],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentTemplates::className(), 'targetAttribute' => ['document_id' => 'id']],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => Fields::className(), 'targetAttribute' => ['field_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_id' => 'Document ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(DocumentTemplates::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(Fields::className(), ['id' => 'field_id']);
    }
}
