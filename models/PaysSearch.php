<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pays;

/**
 * PaysSearch represents the model behind the search form about `app\models\Pays`.
 */
class PaysSearch extends Pays
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'client_id'], 'integer'],
            [['data', 'comment'], 'safe'],
            [['summa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(Yii::$app->user->identity->type != 1){
            $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
            $project = [];
            foreach ($projects as $value) {
                $project [] = $value->project_id;
            }
            $all_client = Clients::find()->where(['project_id' => $project])->all();
            $project = [];
            foreach ($projects as $value) {
                $project [] = $value->project_id;
            }
            $clients = Clients::find()->where(['project_id' => $project])->all();
            $clients_id = [];
            foreach ($clients as $value) {
                $clients_id [] = $value->id;
            }
            $query = Pays::find()->where(['client_id' => $clients_id]);
        }
        else $query = Pays::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 40],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'summa' => $this->summa,
            'client_id' => $this->client_id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
