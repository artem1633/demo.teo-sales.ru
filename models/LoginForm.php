<?php

namespace app\models;

use Yii;
use yii\base\Event;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    const EVENT_AUTHORIZED = 'event_authorized';

    public $username;
    public $password;
    public $rememberMe = true;

    public $_user = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \app\event_handlers\LoginFormEventHandler::class,
            ]
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный логин или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $this->_user = $this->getUser();
            $this->trigger(self::EVENT_AUTHORIZED);
            $res =  Yii::$app->user->login($this->_user, $this->rememberMe ? 3600*24*30 : 0);
            if ($res && Yii::$app->user->identity->getCompanyInstance()->start_fill === 0) {
                $this->startFill();
            }
            return $res;
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        if($this->_user->company->access_end_datetime === null || (new \DateTime($this->_user->company->access_end_datetime))->getTimestamp() > time()){
            return $this->_user;
        } else {
            $this->addError('password', 'Срок действия лицензии истек');
        }
    }

    protected function startFill()
    {
//Создать справочники
        //Тип задач
        $startData = ['Звонок','Почта','Встреча','Телеграмм','Скайп','Ответ на площадке'];
        foreach ($startData as $data) {
            $task_type = new TasksType();
            $task_type->name = $data;
            $task_type->save();
        };
        //Статус звонков
        $startData = [
            ['name'=>'Почта','color' => '#f0f60b'],
            ['name'=>'Звонок','color' => '#00ff15'],
        ];
        foreach ($startData as $data) {
            $relation = new Relations();
            $relation->name = $data['name'];
            $relation->color = $data['color'];
            $relation->save();
        };
        //Статуc задач
        $startData = [
            ['name'=>'Новая','color' => '#1771f1'],
            ['name'=>'В работе','color' => '#f0f60b'],
            ['name'=>'Готово','color' => '#00ff15'],
            ['name'=>'Важная','color' => '#f11717'],
        ];
        foreach ($startData as $data) {
            $task_status = new TaskStatuses();
            $task_status->name = $data['name'];
            $task_status->color = $data['color'];
            $task_status->save();
        };
        //Статуc клиента
        $startData = [
            ['name'=>'На подписании','number'=>'2','color' => '#da6449'],
            ['name'=>'Ждем оплаты','number'=>'2','color' => '#da3d25'],
            ['name'=>'Оказываем услугу','number'=>'9','color' => '#e4dc4a'],
            ['name'=>'Выполнили услугу','number'=>'4','color' => '#367ec7'],
            ['name'=>'Запросил информацию','number'=>'5','color' => '#e23477'],
            ['name'=>'Передали в работу','number'=>'6','color' => '#f4b03e'],
            ['name'=>'Отложили вопрос','number'=>'7','color' => '#d3e14b'],
            ['name'=>'Прошел тест','number'=>'4','color' => '#72f44a'],
            ['name'=>'На тесте','number'=>'10','color' => '#f4e04c'],
            ['name'=>'Ждет звонка','number'=>'1','color' => '#eb4025'],
            ['name'=>'Ждет КП','number'=>'11','color' => '#eb3223'],
            ['name'=>'Передали партнерам','number'=>'12','color' => '#eb3223'],
        ];
        foreach ($startData as $data) {
            $statuses = new Statuses();
            $statuses->name = $data['name'];
            $statuses->color = $data['color'];
            $statuses->number = $data['number'];
            $statuses->save();
        };
        //Статуc лида
        $startData = [
            ['name'=>'Новый','number'=>'1','color' => '#cde67d'],
            ['name'=>'Ждет звонка','number'=>'2','color' => '#e5e76b'],
            ['name'=>'Ждет КП','number'=>'3','color' => '#f9ef51'],
            ['name'=>'Заявка с сайта','number'=>'1','color' => '#ef7930'],
            ['name'=>'Отказался','number'=>'4','color' => '#e23542'],
            ['name'=>'Заинтересован','number'=>'2','color' => '#54b879'],
            ['name'=>'Ждем ответа','number'=>'1','color' => '#54b879'],
            ['name'=>'Не отвечает','number'=>'8','color' => '#eb3223'],
            ['name'=>'Слабо заинтересова','number'=>'9','color' => '#ebd865'],
        ];
        foreach ($startData as $data) {
            $statusLid = new StatusLid();
            $statusLid->name = $data['name'];
            $statusLid->color = $data['color'];
            $statusLid->number = $data['number'];
            $statusLid->save();
        };
        //Проект
        $project = new Project();
        $project->name = 'Тестовый проект';
        $project->save();
        //Источники
        $startData = ['Таргет ВК','Партнерка','Рассылка ВК','Рассылка Инстограмм',
            'Входящие Инстограмм','Холодный звонок','Заявка с сайта','Площадка'];
        foreach ($startData as $data) {
            $source = new Source();
            $source->name = $data;
            $source->save();
        };
        //Етап документов
        $startData = ['Отправили клиенту','Утвержден','Ждем оплаты','Оплачен'];
        foreach ($startData as $data) {
            $stagesDocument = new StagesDocument();
            $stagesDocument->name = $data;
            $stagesDocument->save();
        };
        //тип документов
        $startData = ['Техническое задание','Смета','Договор','Счет','Архив проекта','КП'];
        foreach ($startData as $data) {
            $documentstype = new Documentstype();
            $documentstype->name = $data;
            $documentstype->save();
        };
        //Создание лида
        $lid = new Clients();
        $lid->client=0;
        $lid->fio = 'Максим';
        $lid->telephone = '1234567890';
        $lid->project_id = Project::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->one()->id;
        $lid->source_id = Source::find()->where(['name'=>'Заявка с сайта'])
            ->andWhere(['company_id'=>Yii::$app->user->identity->company_id])->one()->id;
        $lid->lid_id = StatusLid::find()->where(['name'=>'Заинтересован'])
            ->andWhere(['company_id'=>Yii::$app->user->identity->company_id])->one()->id;
        $lid->save();
        //Создание клиента
        $client = new Clients();
        $client->client = 1;
        $client->fio ='Алексей Кораблев';
        $client->telephone = '1234567890';
        $client->city = 'Ливны';
        $client->project_id = Project::find()->where(['company_id'=>Yii::$app->user->identity->company_id])->one()->id;
        $client->source_id = Source::find()->where(['name'=>'Заявка с сайта'])
            ->andWhere(['company_id'=>Yii::$app->user->identity->company_id])->one()->id;
        $client->save();

        $company = Companies::findOne(Yii::$app->user->identity->company_id);
        $company->start_fill = 1;
        $company->save();


    }
}
