<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $date_cr
 * @property string $fio
 * @property string $city
 * @property string $telephone
 * @property string $email
 * @property int $menejer
 * @property string $comment
 * @property int $client
 * @property int $source_id
 * @property int $status
 * @property string $access
 * @property string $id_send
 * @property string $planned_date
 * @property int $company_id Компания, к которой принадлжит запись
 *
 * @property Users $createdBy
 * @property Users $menejer0bv
 * @property Source $source
 * @property Statuses $status0
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }
    public $telephone;
    public $contacts;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if(Yii::$app->user->identity){
            return [
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'company_id',
                    'updatedByAttribute' => null,
                    'value' => function($event) {
                        return Yii::$app->user->identity->company_id;
                    },
                ],
            ];
        }
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
           'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false) {
            //echo "string=".Yii::$app->user->identity->isSuperAdmin();die;
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_cr', 'planned_date', 'date_up'], 'safe'],
            [['fio', 'project_id'], 'required'],
            [['balance'], 'number'],
            [['menejer', 'client', 'source_id', 'status', 'project_id', 'lid_id'], 'integer'],
            [['comment','id_send'], 'string'],
            [['fio', 'city', 'utm', 'link', 'ip', 'telephone'], 'string', 'max' => 255],
            [['menejer'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['menejer' => 'id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Source::class, 'targetAttribute' => ['source_id' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::class, 'targetAttribute' => ['status' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['lid_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusLid::className(), 'targetAttribute' => ['lid_id' => 'id']],
            //['status', 'required', 'when' => function($model) { return $model->client == 1; }, 'enableClientValidation' => false],
            //['lid_id', 'required', 'when' => function($model) { return $model->client == 0; }, 'enableClientValidation' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_cr' => 'Дата создания',
            'fio' => 'ФИО',
            'city' => 'Город',
            'telephone' => 'Телефон',
            //'email' => 'Email',
            'menejer' => 'Менеджер',
            'comment' => 'Комментария',
            'client' => 'Клиент',
            'source_id' => 'Источник',
            'status' => 'Статус',
            //'access' => 'Доступ',
            'planned_date' => 'Планируемая дата',
            'balance' => 'Баланс',
            'project_id' => 'Проект',
            'utm' => 'Utm',
            'link' => 'Ip',
            'ip' => 'Link',
            'lid_id' => 'Статус лида',
            'date_up' => 'Дата изменений',
            'id_send' => 'Айди чата',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_cr = date('Y-m-d');
        }

        $this->date_up = date('Y-m-d H:i:s');
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenejer0()
    {
        return $this->hasOne(Users::class, ['id' => 'menejer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::class, ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Statuses::class, ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentations()
    {
        return $this->hasMany(Documentation::className(), ['client_id' => 'id']);
    }

    public function getContacts()
    {
        return $this->hasMany(Contacts::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPays()
    {
        return $this->hasMany(Pays::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLid()
    {
        return $this->hasOne(StatusLid::className(), ['id' => 'lid_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientsFields()
    {
        return $this->hasMany(ClientsField::className(), ['client_id' => 'id']);
    }

    public function getClient()
    {
        return ArrayHelper::map([
            ['id' => '0','name' => 'Нет',],
            ['id' => '1','name' => 'Да',]
        ], 'id', 'name');
    }

    /**
     * @return array
     */
    public function getManager()
    {
        $user = Yii::$app->user->identity;
        $companyId = $user->getCompany();

        if($companyId != null) {
            $managers = Users::find()->where(['type' => 2])->andWhere(['or', ['company_id' => $companyId], ['id' => $companyId]])->all();
        } else {
            $managers = Users::find()->where(['type' => 2])->all();
        }
        return ArrayHelper::map($managers, 'id', 'fio');
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        $managers = Statuses::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($managers, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getStatusLids()
    {
        $status = StatusLid::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($status, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getProjects()
    {
        $projects = Project::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($projects, 'id', 'name');
    }
    /**
     * @return array
     */
    public function getSourceList()
    {
        $managers = Source::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all();
        return ArrayHelper::map($managers, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getUsersProjects()
    {
        if(Yii::$app->user->identity->type == 1)
        {
            $projects = Project::find()->all();
            return ArrayHelper::map($projects, 'id', 'name');
        }
        else
        {
            $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
            $result = [];
            foreach ($projects as $value) {
                $result [] = [
                    'id' => $value->project_id,
                    'title' => $value->project->name,
                ];
            
            }
            return ArrayHelper::map($result,'id', 'title');            
        }
    }

    /**
     * @param $old
     * @param $new
     */
    public function setChanging($old, $new)
    {        
        if($old->planned_date != $new->planned_date)$this->setToChangeTable('Даты звонка', $old->planned_date, $new->planned_date );
        if($old->status != $new->status)$this->setToChangeTable('Смена статуса',$old->status0->name, $new->status0->name );
    }

    /**
     * @param $field
     * @param $old_value
     * @param $new_value
     */
    public function setToChangeTable($field,$old_value,$new_value)
    {
        $old_value = $old_value.'';
        $new_value = $new_value.'';
        $model = new Changing();
        $model->table_name = 'client';
        $model->line_id = $this->id;
        $model->date_time = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->field = $field;
        $model->old_value = $old_value;
        $model->new_value = $new_value;
        $model->save();
    }
}
