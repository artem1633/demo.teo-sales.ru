<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $phone;
    public $login;
    public $position;
    public $password;
    public $rate_id;
    public $agree;


    /**
     * @var Users содержит созраненную модель users для возможности доступа к ней
     * через обработчик событий
     * @see \app\event_handlers\RegisteredFormEventHandler
     */
    public $_user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class' => \app\event_handlers\RegisteredFormEventHandler::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'phone', 'login', 'password', 'agree'], 'required'],
            [['position'], 'string'],
            [['login'], 'email'],
            [['agree'], 'integer'],
            [['login'], 'unique', 'targetClass' => '\app\models\Users'],
            ['agree', 'validateAgree'],
        ];
    }

    public function validateAgree($attribute)
    {
        if($this->agree != 1) $this->addError($attribute, 'Необходимо Ваше согласие');
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'login' => 'E-mail',
            'position' => 'Должность',
            'password' => 'Пароль',
            'agree' => 'Я согласен с обработкой персональных данных',
        ];
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($ref, $type = Users::USER_TYPE_ADMIN)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new Users();
        $user->attributes = $this->attributes;
        $dateNow = time();
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ 1 days", $dateNow));

        $user->type = $type;
        $user->agree = $this->agree;

        if($user->save())
        {
            $this->_user = $user;
            //Создать компанию
            $company = new Companies([
                'admin_id' => $user->id,
                'access_end_datetime' => $accessEndDateTime,
                'rate_id' => null,
                'main_balance' => Settings::find()->where(['key' => 'bonus_register'])->one()->value,
                'partner_balance' => 0,
                'referal_id' => $ref,
                'prosent_referal' => Settings::find()->where(['key' => 'partner_percent_register'])->one()->value,
                'cost_day' => Settings::find()->where(['key' => 'cost_day'])->one()->value,
                'cost_person' => Settings::find()->where(['key' => 'cost_person'])->one()->value,

            ]);

            $company->save();



            $user->link('company', $company);

            $this->trigger(self::EVENT_REGISTERED);

            $template = EmailTemplates::findByKey('success_registration');

            if($template != null){
                $body = $template->applyTags($user);

                try {
                    Yii::$app->mailer->compose()
                        ->setFrom('zvonki.crm@mail.ru')
                        ->setTo($user->login)
                        ->setSubject('Успешная регистрация')
                        ->setHtmlBody($body)
                        ->send();
                } catch(\Exception $e){

                }
            }

            return $user;
        }

        return null;
    }

}