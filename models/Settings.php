<?php

namespace app\models;


/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $value Значение
 * @property string $label Комментарий
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['label','value'], 'string'],
            [['key'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'value' => 'Значение',
            'label' => 'Комментарий',
        ];
    }

    /**
     * Ищет запись в БД по ключу
     * @param $key
     * @return null|static
     */
    public static function findByKey($key)
    {
        return self::findOne(['key' => $key]);
    }
}
