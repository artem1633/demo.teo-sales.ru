<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_lid_statuses".
 *
 * @property int $id
 * @property int $user_id
 * @property int $status_lid_id
 *
 * @property StatusLid $statusLid
 * @property Users $user
 */
class UsersLidStatuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_lid_statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status_lid_id'], 'integer'],
            [['status_lid_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusLid::className(), 'targetAttribute' => ['status_lid_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status_lid_id' => 'Status Lid ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusLid()
    {
        return $this->hasOne(StatusLid::className(), ['id' => 'status_lid_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
