<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
/*    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],*/
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'url',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'key',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'source_id',
        'content' => function($data){
            return $data->source->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project_id',
        'content' => function($data){
            return $data->project->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($data){
            return $data->status0->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'menejer',
        'content' => function($data){
            return $data->menejer0->fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'applications_user',
        'content' => function($data){
        	return Html::a('Список пользователей', ['/sites/applications-users-list', 'id' => $data->id], [
                    'title' => Yii::t('yii', 'Просмотр'),
                    'role' => 'modal-remote',
                ]
            );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'visitors_user',
        'content' => function($data){
        	return Html::a('Список пользователей', ['/sites/visitors-users-list', 'id' => $data->id], [
                    'title' => Yii::t('yii', 'Просмотр'),
                    'role' => 'modal-remote',
                ]
            );
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'], 
    ],

];   