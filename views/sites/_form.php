<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sites-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'source_id')->dropDownList($model->getSourceList(), ['prompt' => 'Выберите статуса']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'project_id')->dropDownList($model->getProjects(), ['prompt' => 'Выберите проекта']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => 'Выберите статуса']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'menejer')->dropDownList($model->getManager(), ['prompt' => 'Выберите менежера']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'new_visitor')->checkbox(['id' => 'visitor_user_id']); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'new_application')->checkbox(['id' => 'applications_user_id']); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'notice_vk')->checkbox(); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'notice_telegram')->checkbox(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6" id="licensed_user" <?php if($model->new_application != 1) echo 'style=" display: none;"';?> >
            <?= $form->field($model, 'applications_user')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getUsersList(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->ApplicationsUsersList(),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-6" id="visitor_user" <?php if($model->new_visitor != 1) echo 'style=" display: none;"';?> >
            <?= $form->field($model, 'visitors_user')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getUsersList(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'multiple' => true,
                    'value' => $model->isNewRecord ? null : $model->VisitorsUsersList(),
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'company_id')->textInput() ?>        
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS
    $('#applications_user_id').on('click', function() 
    {  
        var applications_user_id = this.value; //console.log(applications_user_id);
        $('#licensed_user').hide();
        if(applications_user_id == 1) $('#licensed_user').show();
    }
);

$('#visitor_user_id').on('click', function() 
    {  
        var visitor_user_id = this.value; //console.log(visitor_user_id);
        $('#visitor_user').hide();
        if(visitor_user_id == 1) $('#visitor_user').show();
    }
);

JS
);
?>