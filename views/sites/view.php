<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sites */
?>
<div class="sites-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'key',
            'source.name',
            'project.name',
            'status0.name',
            'menejer0.fio',
            [
                'attribute' => 'new_visitor',
                'value' => function($data){
                    if($data->new_visitor == 1) return "Есть";
                    else return "Нет";
                },
            ],
            [
                'attribute' => 'new_application',
                'value' => function($data){
                    if($data->new_application == 1) return "Есть";
                    else return "Нет";
                },
            ],
            [
                'attribute' => 'notice_vk',
                'value' => function($data){
                    if($data->notice_vk == 1) return "Есть";
                    else return "Нет";
                },
            ],
            [
                'attribute' => 'notice_telegram',
                'value' => function($data){
                    if($data->notice_telegram == 1) return "Есть";
                    else return "Нет";
                },
            ],
            /*[
                'attribute' => 'applicationsUser.fio',
                'label' => 'Получатель оповещения о заявках',
            ],
            [
                'attribute' => 'visitorsUser.fio',
                'label' => 'Получатель оповещения о посетителях',
            ],*/
            //'company_id',
        ],
    ]) ?>
    <!-- <label for="">
        Для подключения сайта вставте код на сайт <br>
        <a target="blank" href="http://<?php // $_SERVER['SERVER_NAME'] ?>/api/client/newclient/?access=<?php //$model->key?>&name=demo&phone=demo&email=demo">http://<?php // $_SERVER['SERVER_NAME'] ?>/api/client/newclient/?access=<?php //$model->key?>&name=demo&phone=demo&email=demo</a>  или обратитесь к администратору
    </label> -->
    <label for="">
        <!-- Для подключения сайта вставте код на сайт <br>
        $get = "https://demo.teo-sales.ru/api/client/newclient/?<br>access=вашключ&name=".$_POST['name']."&phone=".$_POST['phone']."&email=".$_POST['email'];<br>
        $get = str_replace(" ", "%20", $get);<br>
        file_get_contents($get);<br>
        или обратитесь к администратору -->

        Для подключения сайта вставте код на сайт <br>
        <?=Html::encode('
        <script type="text/javascript">window.name = "'.$model->key.'"; </script> '); ?>
        <br>
        <?=Html::encode('<script type="text/javascript" src="https://demo.teo-sales.ru/js/sendGet.js"></script>;
        '); ?><br>
        или обратитесь к администратору
    </label>

</div>
