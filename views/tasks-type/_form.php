<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TasksType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-type-form">

    <?php $form = ActiveForm::begin(); ?>

	<div style="display: none;">
	    <?= $form->field($model, 'company_id')->textInput() ?>
	    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>		
	</div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
