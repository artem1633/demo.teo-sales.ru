<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TasksType */
?>
<div class="tasks-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'company_id',
            //'color',
            'name',
        ],
    ]) ?>

</div>
