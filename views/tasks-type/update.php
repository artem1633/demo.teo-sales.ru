<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TasksType */
?>
<div class="tasks-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
