<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
             <?= $form->field($model, 'client')->dropDownList($model->getClients(), ['prompt' => 'Выберите клиента']) ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'manager')->dropDownList(
                $model->getManager(), 
                [
                    'prompt' => 'Выберите менежера',
                    'disabled'=> Yii::$app->user->identity->type == 2 ? true : false,
                ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
             <?= $form->field($model, 'relation')->dropDownList($model->getRelations(), ['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">  
            <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]]); ?>
        </div>
        <div class="col-md-4">
             <?= $form->field($model, 'data')->widget(
                DatePicker::className(), [
                    'inline' => false,
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
             ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
             <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
  
        <div style="display: none;">
             <?= $form->field($model, 'date_cr')->textInput() ?>
             <?= $form->field($model, 'date_up')->textInput() ?>
        </div>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
    </div>
</div>

