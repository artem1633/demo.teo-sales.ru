<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Relations;
use app\models\Clients;
use app\models\Users;
return [
    //[
        //'class' => 'kartik\grid\CheckboxColumn',
        //'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client',
        'filter' => ArrayHelper::map(Clients::find()->all(), 'id', 'fio'),
        'content' => function ($data) {
           return '<a data-pjax=0 class="label label-warning" href="'.Url::toRoute(['clients/view', 'id' => $data->client]).'">'.$data->client0->fio.'</a>';
       },
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manager',
         'filter' => ArrayHelper::map(Users::find()->all(), 'id', 'fio'),
         'content' => function ($data) {
           return $data->manager0->fio;
       },
     ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'relation',
         'filter' => ArrayHelper::map(Relations::find()->all(), 'id', 'name'),
         'content' => function ($data) {
           return $data->relation0->name;
       },
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'contentOptions' => ['style' => 'width:30%; white-space: normal;'],
        'attribute'=>'text',
        'content' => function ($data) {
            return substr($data->text, 0, 300) . '...';             
         },
    ],
    //[
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'date_cr',
    //],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_up',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'], 
    ],

];   