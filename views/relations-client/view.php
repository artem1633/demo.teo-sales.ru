<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RelationsClient */
?>
<div class="relations-client-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'manager',
                'value' => function ($data) {
                   return $data->manager0->fio;
                },
            ],
            [
                'attribute' => 'client',
                'value' => function ($data) {
                   return $data->client0->fio;
                },
            ],
            [
                'attribute' => 'relation',
                'value' => function ($data) {
                   return $data->relation0->name;
                },
            ],
            'data',
            'time',
            'text:ntext',
            'date_cr',
            'date_up',
        ],
    ]) ?>

</div>
