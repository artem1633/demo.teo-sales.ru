<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RelationsClient */

?>
<div class="relations-client-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
