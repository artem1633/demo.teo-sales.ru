<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelationsClient */
?>
<div class="relations-client-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
