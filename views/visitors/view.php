<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Visitors */
?>
<div class="visitors-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'site_id',
            'ip',
            'number',
            'utm',
            'link',
            'date_cr',
        ],
    ]) ?>

</div>
