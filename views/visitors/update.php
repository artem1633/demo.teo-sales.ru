<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Visitors */
?>
<div class="visitors-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
