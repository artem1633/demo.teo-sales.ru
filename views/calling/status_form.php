<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calling-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusesList(), ['prompt' => 'Выберите статуса']) ?>
        </div>         
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
