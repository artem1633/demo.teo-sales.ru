<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Calling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calling-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div> 
        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>         
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'planned_date')->widget(
                DatePicker::className(), [
                    'inline' => false,
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusesList(), ['prompt' => 'Выберите статуса']) ?>
        </div>         
        <div class="col-md-3" style="margin-top: 27px;">
            <?= $form->field($model, 'new_client')->checkbox() ?>
        </div>         
    </div>

    
    <div style="display: none;">
        <?= $form->field($model, 'user_id')->textInput(['value' => Yii::$app->user->identity->id]) ?>        
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
