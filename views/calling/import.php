<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Clients */

?>
<div class="clients-create">
    <?= $this->render('import_form', [
        'model' => $model,
    ]) ?>
</div>
