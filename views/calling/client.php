<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calling-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $id == 1 ? '<span style = "color:blue; font-size:17px;"><center> <b>Успешно выполнено </b></center></span>' : '<span style = "color:red; font-size:17px;"><center> <b>Этот клиент есть уже в этой системе </b></center></span>'?>
        </div>         
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
