<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use teo_crm\yii\module\Comments;
use yii\helpers\HtmlPurifier;



\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Просмотр/Изменение';

$dostup = 0;
$tip = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;

if($tip == 6 | $tip == 1 | $tip == 3) $dostup = 1;

?>
    
    <div class="box box-default">
        <div class="box-body">
<div class="clients-view">

    <div class="row">

        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs">Общая информация</span>
                        <span class="hidden-xs">Общая информация</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">История с звонка</span>
                        <span class="hidden-xs">История с звонка</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="default-tab-1">
                    <div class="row">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'base-pjax']) ?>
                            <div class="col-md-11" style="margin-left: 40px;"><br>
                            <h3>
                                Общие элементы                                 
                                <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>                                
                            </h3>
                            <div style="margin-top: 10px" class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('fio')?></b></td>
                                        <td colspan="3"><?=Html::encode($model->fio)?></td>                                        
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('city')?></b></td>
                                        <td><?=Html::encode($model->city)?></td>
                                        <td><b><?=$model->getAttributeLabel('telephone')?></b></td>
                                        <td><?=Html::encode($model->telephone)?></td>
                                    </tr>
                                    <tr>

                                    <td><b><?=$model->getAttributeLabel('email')?></b></td>
                                        <td><?=Html::encode($model->email)?></td>
                                        <td><b><?=$model->getAttributeLabel('user_id')?></b></td>
                                        <td><?= $model->user->fio?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('status')?></b></td>
                                        <td><?=Html::encode(\app\models\Statuses::findOne($model->status)->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('planned_date')?></b></td>
                                        <td><?=Html::encode($model->planned_date)?></td>
                                    </tr>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php Pjax::end() ?>                        
                    </div>
                </div>
                <div class="tab-pane fade" id="default-tab-2">
                    <div class="row">
                   
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                            <div class="col-md-11" style="margin-left: 40px;"><br>
                            <br><br>
                                <?php $changes =\app\models\Changing::find()->where(['table_name' => 'calling', 'line_id' => $model->id])->all();
                                    if($changes == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет изменений </b></center></span>';}
                                    else {
                                ?> 
                                <table class="table table-bordered table-condensed">
                                    <tr>
                                        <!-- <th>Статус</th> -->
                                        <th>Дата/Время</th>
                                        <th>Пользователь (Логин)</th>
                                        <th>Поле</th>
                                        <th>Значение</th>
                                        <th>Изменение</th>
                                    </tr>
                                    <?php 
                                    foreach ($changes as $change) {
                                        $username = Users::findOne($change->user_id);
                                    ?>
                                    <tr>
                                        <!-- <td><?php //$change->status?></td> -->
                                        <td><?=\Yii::$app->formatter->asDate($change->date_time, 'php:H:i, d.m.Y')?></td>
                                        <td><?=$username->login?></td>
                                        <td><?=$change->field?></td>
                                        <td><?=$change->old_value?></td>
                                        <td><?=$change->new_value?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <?php } ?>
                            </div>
                        <?php Pjax::end() ?>                 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>