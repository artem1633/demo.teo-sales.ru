<?php
use yii\helpers\Url;

return [
    //[
        //'class' => 'kartik\grid\CheckboxColumn',
        //'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function ($data) {
           $status = \app\models\Statuses::findOne($data->status);
           return '<a role="modal-remote" class="label label-warning" href="'.Url::toRoute(['status-change', 'id' => $data->id]).'">'.$status->name.'</a>';
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'planned_date',
        'content' => function ($data) {
            return '<a role="modal-remote" class="label label-info" href="'.Url::toRoute(['date-change', 'id' => $data->id]).'">'.$data->planned_date.'</a>';
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'new_client',
        'content' => function ($data) {
            return '<a role="modal-remote" class="label label-danger" href="'.Url::toRoute(['add-client', 'id' => $data->id]).'">Cделать клиентом</a>';
       },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax' => 0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'], 
    ],

];   