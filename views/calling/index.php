<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Обзвон";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<!-- <div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse users-index">
            <div class="panel-heading">
                <h4 class="panel-title">Импорт</h4>
            </div>
            <div class="panel-body">
                <?php // $form = ActiveForm::begin(['action'=>'/calling/import', 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>
                <div class="col-md-1">
                    <?php // $form->field($model, 'file')->label('<span class="btn btn-default btn-sm">Загрузить</span>',[])->fileInput(['class'=>'sr-only']) ?>
                </div>

                <div class="col-md-1">
                    <button type="submit" class="btn btn-success btn-flat"><?php // Yii::t('app', 'Импорт') ?></button>
                </div>

                <?php // ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div> -->

<div class="panel panel-inverse users-index">
    <div class="panel-heading">
        <h4 class="panel-title">Обзвон</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить звонка','class'=>'btn btn-success']).
            '&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']).
                Html::a('Импорт <i class="fa fa-plus"></i>', ['import'],
                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-warning']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>

