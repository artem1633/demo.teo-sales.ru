<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Sites;

CrudAsset::register($this);
$intro = \app\models\Lessons::getLessonsGroup(1);
?>
<?php if(Yii::$app->user->isGuest == false): ?>
<div id="ajaxCrudDatatable">
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand"><span class="navbar-logo"></span>  ТЕО-SALES</a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'notification-pjax']) ?>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <?php if(Yii::$app->user->isGuest === false): ?>
                    <?=Html::a('Баланс '.number_format(Yii::$app->user->identity->getCompanyInstance()->main_balance,2), ['payment/index'],['data-pjax'=>'0'])?>
                <?php endif; ?>
            </li>
            <li><?= Html::a('Профиль', 
                    ['/users/change', 'id' => Yii::$app->user->identity->id], 
                    [
                        'role'=>'modal-remote', 'title'=>'Изменить профиля',
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-intro'=>$intro[6]['hint'],'data-step'=>$intro[6]['step']

                    ]
                )?>
            </li>
            <?php if(Yii::$app->user->isGuest === false && Yii::$app->user->identity->isSuperAdmin()): ?>
                <li>
                    <?= Html::a(
                        '<i class="fa fa-cog fa-lg"></i>',
                        ['/settings'],
                        ['title' => 'Настройки']
                    ) ?>
                </li>
            <?php endif; ?>
            <!-- <li class="dropdown navbar-user">
                <?php /*Html::a(
                    'Выйти',
                    ['/site/logout'],
                    ['data-method' => 'post']
                )*/ ?>
                </a>
            </li> -->
        </ul>
        <?php Pjax::end() ?>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
</div>
<?php endif; ?>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
