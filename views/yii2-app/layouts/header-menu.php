<?php

use app\models\Users;

$tip = Yii::$app->user->identity->type;

?>
<?php
$intro = \app\models\Lessons::getLessonsGroup(1);
?>
<?php if(Yii::$app->user->isGuest == false): ?>
<div id="top-menu" class="top-menu">

        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Рабочий стол', '' => 'fa fa-desktop', 'url' => ['site/dashboard'],
                        'options' => ['data-intro'=>$intro[0]['hint'],'data-step'=>$intro[0]['step']]],
                    ['label' => 'Лиды',  'url' => ['clients/lids'],
                        'options' => ['class' => 'has-sub','data-intro'=>$intro[1]['hint'],'data-step'=>$intro[1]['step']]],
                    ['label' => 'Клиенты',  'url' => ['clients/index'],
                        'options' => ['class' => 'has-sub','data-intro'=>$intro[2]['hint'],'data-step'=>$intro[2]['step']]],
                    /*['label' => 'Обзвон ', '' => 'fa fa-users', 'url' => ['calling/index'], ],*/
                    /*['label' => 'Звонки', 'url' => ['relations-client/index'], ],*/
                    ['label' => 'Задачи',  'url' => ['tasks/index'],
                        'options' => ['class' => 'has-sub','data-intro'=>$intro[3]['hint'],'data-step'=>$intro[3]['step']]],
                    ['label' => 'Финансы',  'url' => ['payment/index'], ],
                    ['label' => 'Партнерская програма',  'url' => ['affiliate-program/index'], ],
    //                ['label' => 'Новости ','url' => ['/news/index'],  ],
                    ['label' => 'FAQ  ', 'url' => ['faq/index'],
                        'options' => ['class' => 'has-sub','data-intro'=>$intro[4]['hint'],'data-step'=>$intro[4]['step']]],

                    [
                        'label' => 'Администратор',
                        'icon' => '',
                        'url' => '#',
                        'options' => ['class' => 'has-sub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(), 
                        'items' => [
                            ['label' => 'Компании', 'url' => ['companies/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin() ],
                            ['label' => 'Статистика', 'url' => ['statistic/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin() ],
                            ['label' => 'Логи', 'url' => ['logs/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin() ],
                           // ['label' => 'Тарифы', 'url' => ['rates/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin() ],
                            ['label' => 'Шаблоны почтовых сообщений', 'url' => ['email-templates/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Страницы входа', 'url' => ['sliders/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Настройки', 'url' => ['settings/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Инструкции', 'url' => ['intro/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                            ['label' => 'Подсказки', 'url' => ['hints/index'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],

                        ]
                    ],
                    [
                        'label' => 'Настройка',
                        'icon' => '',
                        'url' => '#',
                        'options' => ['class' => 'has-sub','data-intro'=>$intro[5]['hint'],'data-step'=>$intro[5]['step']],
                        'visible' => Yii::$app->user->identity->type != Users::USER_TYPE_MANAGER,
                        'items' => [

                            ['label' => 'Пользователи', 'url' => ['users/index'], 'visible' => Yii::$app->user->identity->type != Users::USER_TYPE_MANAGER, ],
                            ['label' => 'Поля анкеты', 'url' => ['fields/index'], ],
                            ['label' => 'Шаблоны', 'url' => ['templates/index'], ],
                            ['label' => 'Настройка', 'url' =>  ['/companies/time-zone', 'id' => Yii::$app->user->identity->getCompanyInstance()->id],'role'=>'modal-remote', ],
                            ['label' => 'Сайты',  'url' => ['/sites/index'], 'visible' => ($tip == Users::USER_TYPE_ADMIN), ],
                            ['label' => 'Посетители',  'url' => ['/visitors/index'], 'visible' => ($tip == Users::USER_TYPE_ADMIN), ],
                            [
                                'label' => 'Справочники',
                                'icon' => '',
                                'url' => '#',
                                'options' => ['class' => 'has-sub'],
                                'visible' => Yii::$app->user->identity->type != Users::USER_TYPE_MANAGER,
                                'items' => [
                                    ['label' => 'Тип задачи', 'url' => ['tasks-type/index'], ],
                                    ['label' => 'Статус звонков', 'url' => ['relations/index'], ],
                                    ['label' => 'Статусы задач', 'url' => ['task-statuses/index'], ],
                                    ['label' => 'Статус клиента', 'url' => ['statuses/index'], ],
                                    ['label' => 'Статус лида', 'url' => ['status-lid/index'], ],
                                    ['label' => 'Проекты', 'url' => ['/project/index'], ],
                                    ['label' => 'Источники', 'url' => ['source/index'], ],
                                    ['label' => 'Этапы документов', 'url' => ['stages-document/index'],  ],
                                    ['label' => 'Тип документа ', 'url' => ['/documentstype/index'],  ],
                                ],
                            ],
                        ],
                    ],
    //                ['label' => 'Инструкция', 'url' => ['/site/instruksion'], ],
                    ['label' => 'Выйти', 'url' => ['/site/logout'], ],
                ],
            ]
        );
        ?>

</div>
<?php endif; ?>
