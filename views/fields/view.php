<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fields */
?>
<div class="fields-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'label',
            //'type',
            //'data:ntext',
            'project_id',
            //'company_id',
        ],
    ]) ?>

</div>
