<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if($model->data != null && !is_array($model->data))
    $model->datas = explode(',', $model->data);

$display_dropdown = 'display: none;';
if(!$model->isNewRecord && $model->type == 'dropdown') $display_dropdown = '';

?>

<div class="fields-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'project_id')->dropDownList($model->getProjectList()) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->getTypesList(), ['id' => 'type']) ?>

    <div id="data" style="<?=$display_dropdown?>;">
        <?= $form->field($model, 'datas')->widget(\kartik\select2\Select2::class, [
            'options' => ['multiple' => true,],
            'pluginOptions' => [
                'tags' => true,
            ],
        ]) ?>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'company_id')->textInput() ?>        
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; console.log(type);
        $('#data').hide();
        if(type == 'dropdown') $('#data').show();
    }
);
JS
);
?>