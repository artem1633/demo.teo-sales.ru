<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fields */
?>
<div class="fields-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
