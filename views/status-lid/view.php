<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StatusLid */
?>
<div class="status-lid-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number',
            'name',
            'color',
            //'company_id',
        ],
    ]) ?>

</div>
