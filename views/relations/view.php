<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Relations */
?>
<div class="relations-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
