<?php

use app\models\LessonsUsers;
use app\models\Settings;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Users;
use app\models\Clients;
use yii\helpers\Html;
use app\models\Sites;
use app\models\Visitors;
use app\models\Project;
$intro = \app\models\Lessons::getLessonsGroup(1);
$this->title = 'Рабочий стол';
?>

<button  type="button" class="btn btn-xs btn-warning" onclick='startIntro();'>Начать обучение</button>

<div class="row">

    <div class="col-md-3 col-sm-6" data-intro="<?=$intro[7]['hint']?>" data-step="<?=$intro[7]['step']?>">
        <div class="widget widget-stats bg-green">
            <div class="stats-icon"><i class="fa fa-desktop"></i></div>
            <div class="stats-info">
                <p>КЛИЕНТОВ</p>
                <!-- <h4>Всего \ За месяц \ За неделю \ За день </h4> -->
                <!-- <h4> -->
                    <span style="position: relative; float: left; text-align: center;">Всего \ <br><?= $clients['all']?></span>
                    <span style="position: relative; float: left; text-align: center;">За месяц \ <br><?= $clients['month']?></span>
                    <span style="position: relative; float: left; text-align: center;">За неделю \ <br><?= $clients['week']?></span>
                    <span style="position: relative; float: left; text-align: center;">За день <br><?= $clients['day']?></span>
                <!-- </h4> -->
                <br>
                <br>
                
            </div>
            <div class="stats-link">
                <a href="<?=Url::to(['/clients/lids'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6" data-intro="<?=$intro[8]['hint']?>" data-step="<?=$intro[8]['step']?>">
        <div class="widget widget-stats bg-blue">
            <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
            <div class="stats-info">
                <p>ОПЛАТЫ</p>
                <!-- <h4>Всего \ За месяц \ За неделю \ За день </h4> -->
                <!-- <h4> -->
                    <span style="position: relative; float: left; text-align: center;">Всего \ <br><?= $pays['all']?></span>
                    <span style="position: relative; float: left; text-align: center;">За месяц \ <br><?= $pays['month']?></span>
                    <span style="position: relative; float: left; text-align: center;">За неделю \ <br><?= $pays['week']?></span>
                    <span style="position: relative; float: left; text-align: center;">За день <br><?= $pays['day']?></span>
                <!-- </h4> -->
                <br>
                <br>
            </div>
            <div class="stats-link">
                <a href="<?=Url::to(['/pays/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>

</div>


<?php

$this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);

?>

<?php
$lessons_pass = LessonsUsers::getLessonsStatus(1);
if (!$lessons_pass) {
    $param = Settings::find()->where(['key'=>'instruction_text'])->one();
    Modal::begin([
        'header' => '<h2>Добро пожаловать в TEO SALES!</h2>',
        'clientOptions' => ['show' => true],
        'footer' => '<button style="margin-top: -4px;" type="button" class="btn btn-xs btn-warning" '.
            'data-dismiss="modal" onclick=\'startIntro();\'>Начать обучение</button>'
    ]);
    echo $param->value;
    Modal::end();
    LessonsUsers::setPassed(1);
}
?>

<script type="text/javascript">
    function startIntro(){
        var intro = introJs();
        intro.setOptions({
            nextLabel:"Дальше",
            prevLabel:"Назад",
            skipLabel:"Пропустить",
            doneLabel:"Понятно",
        });

        intro.start();
    }
</script>
