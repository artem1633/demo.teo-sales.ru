<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use kartik\sortable\Sortable;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;
use app\models\Sprints;
use app\assets\DetailBoardSprint;
use app\models\Tasks;
use app\models\Users;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Этапы задач';
$tip = Yii::$app->user->identity->type;

\johnitvn\ajaxcrud\CrudAsset::register($this);
DetailBoardSprint::register($this);

$dostup = 0;
if($tip == 1 | $tip == 6) $dostup = 1;
if($tip == 3) $dostup = 1;

$sprint = Sprints::findOne($sprint_id);

?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">
        <a href="<?= Url::to(['/site/boards-sprint',  'project_id' => $sprint->communications_project])?>"  style="color:red;" >
            <i class="glyphicon glyphicon-backward"></i> <b>К спринтам </b>
        </a>
        <i style="float: right; margin-top: -20px;"> Этапы задач </i>
        </h4>
    </div>
    <div class="panel-body">

<div class="desc-page-container" id="boards-sprint-item-ajax" style="background-color: #0079bf; margin-left:-13px; margin-top: -13px; margin-bottom: -15px; margin-right: -13px;">
    <div class="page-container-width">
        <div class="row">
            <?php if(!empty($board)):?>
            <?php foreach ($board as $key):
                $stepTitle = $key['name'];
                $stepId = $key['id'];
                
                if($tip == 5)
                {
                    $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId, 'executor' => Yii::$app->user->identity->id ])->count();
                    $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId, 'executor' => Yii::$app->user->identity->id])->all();
                    
                }

                if($tip == 3)
                {
                    $sprint = Sprints::findOne($boardId);
                    $countSteps = Tasks::find()->where(['stage' => $key['id'], 'project' => $sprint['communications_project'] , 'sprint' => $boardId, 'manager_project' => Yii::$app->user->identity->id ])->count();
                    $tasks = Tasks::find()->where(['stage' => $key['id'] , 'project' => $sprint['communications_project'] , 'sprint' => $boardId, 'manager_project' => Yii::$app->user->identity->id ])->all();
                    
                } 
                if($tip == 4)
                {
                   $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId ])->count();
                   $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId  ])->all(); 
                }
                if($tip == 1 | $tip == 6)
                {
                   $user = Users::findOne(Yii::$app->user->identity->id);
                   if($user->creator != null)
                   {
                        $countSteps = Tasks::find()->where(['stage' => $key['id'],'sprint' => $boardId,'company' => $user->creator ])->count();
                        $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId, 'company' => $user->creator ])->all(); 
                   }
                   else 
                   {
                        $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId , 'company' => Yii::$app->user->identity->id])->count();
                        $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId, 'company' => Yii::$app->user->identity->id])->all(); 
                   }

                }

            ?>
                <div class="step-column" id="<?= $key['id']?>">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                        <div class="step-title">

                            <div class="bold-title" style="font-size: 12px;">
                                <?=StringHelper::truncate($stepTitle, 24)?> 
                <?php if($dostup == 1){ ?>
                                <?= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['/stages-tasks/update-step', 'id' => $key['id']], [
                                    'role'=>'modal-remote', 'title'=>'Изменить',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                ]) ?>
                                <?= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['/stages-tasks/delete-step','id'=> $key['id']], [
                                    'role'=>'modal-remote', 'title'=>'Удалить',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                ]) ?>
                <?php } ?>
                            </div>

                            <span class="stagevalue">
                               <span class="value">
                                   <small>Задачи <span class="count-clients-item"><?= $countSteps?></span></small>
                               </span>
                            </span>
                        </div>
                    <?php Pjax::end() ?>
                    <?php
                    $items = [];
                    if($countSteps) {
                        
                        foreach ($tasks as $value) {                            

                            $task_id = $value->id;
                            $task_name = $value->task_title;
                            $meneger = $value->managerProject->fio;
                            $proyekt = $value->project0->name;
                            if($key['color'] == 1) $color = 'red';
                            if($key['color'] == 2) $color = 'green';
                            if($key['color'] == 3) $color = 'blue';
                            if($key['color'] == 4) $color = 'yellow';
                            if($key['color'] == 5) $color = 'black';
                            if($key['color'] == 6) $color = 'white';
                            if($key['color'] == 7) $color = 'red-green';
                            $kalit = $key['id'];

                            $url = Url::to(['tasks/view', 'id' => $task_id]);
                            
                               $items[]['content'] = '
                                <div class="panel " id="'.$task_id.'">                            
                                    <div class="panel-heading" >
                                        <div class="panel-heading-btn">                    
                                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-plus"></i>
                                            </a>
                                            <a href='.$url.'>
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <h4 class="panel-title"><b>Название задачи :</b><br><div class=" desc-page-container2">'.$task_name.'</div></h4>
                                    </div>
                                    <div class="panel-body p-0 page-container-width2 desc-page-container" style="display: none;">
                                        <ul class="todolist">
                                            <li>
                                                Проект: '.$proyekt.'<br>
                                                Менежер: '.$meneger.'
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                   ';
                        }                        
                    }

                    echo Sortable::widget([
                        'connected'=>true,
                        'options' => [
                            'data-step'=>$stepId,
                            'data-url-update' => Url::to(['ajax/update-step-client']),
                        ],
                        'items' => $items,
                        'pluginEvents' => [
                            'sortupdate' => "function(e, e2) {
                                var currentElementSelector = $(e2.item).find('div');
                                
                                var currentStep = {$stepId};
                                var currentId = currentElementSelector.attr('id');                                
                                var oldStepId =  $(currentElementSelector).data('parent-step');
                                
                                currentElementSelector.data('parent-step', currentStep + 'step');
                                                                                             
                                //Этап from
                                var oldStepObj = $('#' + oldStepId);
                                
                                var countClientsItemFrom = oldStepObj.find('.count-clients-item');
                                var countClientsItemFromValue = countClientsItemFrom.text();
                                countClientsItemFromValue--;
                                countClientsItemFrom.text(countClientsItemFromValue);
                                                              
                                //Этап to
                                var currentStepObj = $('#' + currentStep + 'step');
                                
                                var countClientsItemTo = currentStepObj.find('.count-clients-item');
                                var countClientsItemToValue = countClientsItemTo.text();
                                countClientsItemToValue++;
                                countClientsItemTo.text(countClientsItemToValue);
                                
                                $.ajax({ type: 'POST',
                                    url: '/ajax/update-step-tasks?etapId='+currentStep+'&taskId='+currentId,
                                    success: function(data){
                                        console.log(data);
                                        console.log(currentId);
                                        console.log(currentStep);
                                    }
                                });
                            }",
                        ],
                    ]);
                    ?>
                </div>
            <?php endforeach;?>
            <?php if($tip == 1 | $tip == 3 | $tip == 6){?>
                <div class="step-column add-client-button-column">
                    <a href="<?= Url::to(['ajax-step-client/create1', 'boardId' => $boardId])?>" id="create-step-sprint" class="btn">
                        Создать этапы задач
                    </a>
                </div>
            <?php } ?>
            <?php else:?>
                <?php if($tip == 1 | $tip == 3 | $tip == 6){?>
                    <div class="step-column add-client-button-column">
                        <a href="<?= Url::to(['ajax-step-client/create1', 'boardId' => $boardId])?>" id="create-step-sprint" class="btn">
                            Создать этапы задач
                        </a>
                    </div>
                <?php } ?>

            <?php endif;?>
        </div>
    </div>
</div>
 </div>
</div>

<?php
Modal::begin([
    'header' => 'Создать этапы задач',
    'id' => 'step-sprint-modal'
]);
Modal::end();
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>