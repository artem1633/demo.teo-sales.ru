<?php

use yii\helpers\Html;
use app\models\Settings;
app\assets\AppAsset::register($this);
?>

<div class="test-container lightmode">
    <div class="test-box animated fadeInDown">
            <?= Settings::find()->where(['key' => 'policy'])->one()->value ?>
    </div>
</div>