<?php

use yii\helpers\Html;
use app\models\Fields;


/* @var $this yii\web\View */
/* @var $model app\models\Templates */

$fields = Fields::find()->all();//$model->templateFields;

?>

<?php if( count($fields) > 0): ?>

    <div class="panel panel-inverse" data-sortable-id="ui-widget-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title="" data-init="true"><i class="fa fa-plus"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">Доступные поля</h4>
        </div>
        <div class="panel-body" style="display: none;">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Заголовок</th>
                    <th>Тег</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Порядковый номер документа</td>
                        <td>{number_doc}</td>
                    </tr>
                <?php foreach ($fields as $field): ?>
                    <tr>
                        <td><?=$field->label?></td>
                        <td>{<?=$field->name?>}</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

        <!-- <div class="panel panel-default overflow-hidden">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <i class="fa fa-plus-circle pull-right"></i>
                        Доступные поля
                    </a>
                </h3>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            <th>Тег</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php //foreach ($fields as $field): ?>
                            <tr>
                                <td><?php //$field->label?></td>
                                <td>{<?php //$field->name?>}</td>
                            </tr>
                        <?php //endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> -->

    <?php endif; ?>

<div class="templates-create">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Новый шаблон</h4>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
