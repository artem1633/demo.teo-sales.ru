<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Templates */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    	<div class="col-md-4">
    		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'sort_number')->textInput(['type' => 'number']) ?>
    	</div>
    	<div class="col-md-4">
    		<?= $form->field($model, 'project_id')->dropDownList($model->getProjectList(), ['prompt' => 'Выберите',]) ?>
    	</div>
    </div>

    
    
    

    <?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, []) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Готово' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
