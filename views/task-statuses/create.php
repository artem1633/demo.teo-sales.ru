<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskStatuses */

?>
<div class="task-statuses-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
