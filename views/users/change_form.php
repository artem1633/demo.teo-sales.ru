<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\UsersProjects;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	 <div class="row">
        <div class="col-md-12">
		    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
		</div>
	</div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
    </div>    

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'telegram_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'vk_id')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel" style="background-color:#d9e0e7">
                <div class="panel-heading" >
                    <div class="panel-heading-btn">                    
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"><b>Инструкция по получению Телеграм ид и Ид VK</b><br></h4>
                </div>
                <div class="panel-body p-0 page-container-width2 desc-page-container" style="display: none;">
                    1 - Нужно зайти в бот <span style="color:#339cd0;">@teo_sales_bot</span> и нажать кнопку старт<br>
                    2 - Нужно зайти в бот <span style="color:#339cd0;">@id_chatbot</span> и нажать кнопку старт и полученный код<br>
                    3 - Вставить в своем профиле полученный id в поле Телеграм чат ИД<br>
                    4 - <a style="width: 100px;" target="_blank" href="https://hinw.ru/yi/">https://hinw.ru/yi/</a> - это для того чтобы узнать id в вк
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel" style="background-color:#d9e0e7">
                <div class="panel-heading" >
                    <div class="panel-heading-btn">                    
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"><b>Ваши проекты</b><br></h4>
                </div>
                <div class="panel-body p-0 page-container-width2 desc-page-container" style="display: none;">
                    <?php 
                        $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
                        $project = "";
                        foreach ($projects as $value) {
                            $project .= $value->project->name . ', ';
                        }
                        echo $project;
                     ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>