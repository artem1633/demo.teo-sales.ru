<?php

use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'login',
            'vk_id',
            'telegram_id',
           // 'password',
            [
                'attribute' => 'type',
                'value' => function ($data) {
                   if($data->type == 1) return 'Администратор';
                   if($data->type == 2) return 'Менеджер';
                   if($data->type == 3) return 'Партнер';
                },
            ],
        ],
    ]) ?>

</div>
