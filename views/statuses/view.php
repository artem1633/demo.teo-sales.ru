<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Statuses */
?>
<div class="statuses-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number',
            'name',
        ],
    ]) ?>

</div>
