<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */
?>
<div class="contacts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'position',
            'phone',
            'email:email',
            'client_id',
        ],
    ]) ?>

</div>
