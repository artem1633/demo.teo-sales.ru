<?php

use app\models\Clients;
use app\models\Sites;
use app\models\Visitors;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'admin.fio',
            'access_end_datetime',
            'main_balance',
            'partner_balance',
            'prosent_referal',
            'cost_day',
            'cost_person',
        ],
    ]) ?>



</div>
