<?php

use app\models\Clients;
use app\models\Sites;
use app\models\Visitors;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'admin.fio',
            'access_end_datetime',
            'main_balance',
            'partner_balance',
            'prosent_referal',
            'cost_day',
            'cost_person',
        ],
    ]) ?>

    <div class="row">

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                <div class="stats-info">
                    <p>КЛИЕНТОВ</p>
                    <!-- <h4>Всего \ За месяц \ За неделю \ За день </h4> -->
                    <!-- <h4> -->
                    <span style="position: relative; float: left; text-align: center;">Всего \ <br><?= $clients['all']?></span>
                    <span style="position: relative; float: left; text-align: center;">За месяц \ <br><?= $clients['month']?></span>
                    <span style="position: relative; float: left; text-align: center;">За неделю \ <br><?= $clients['week']?></span>
                    <span style="position: relative; float: left; text-align: center;">За день <br><?= $clients['day']?></span>
                    <!-- </h4> -->
                    <br>
                    <br>

                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/clients/lids'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-chain-broken"></i></div>
                <div class="stats-info">
                    <p>ОПЛАТЫ</p>
                    <!-- <h4>Всего \ За месяц \ За неделю \ За день </h4> -->
                    <!-- <h4> -->
                    <span style="position: relative; float: left; text-align: center;">Всего \ <br><?= $pays['all']?></span>
                    <span style="position: relative; float: left; text-align: center;">За месяц \ <br><?= $pays['month']?></span>
                    <span style="position: relative; float: left; text-align: center;">За неделю \ <br><?= $pays['week']?></span>
                    <span style="position: relative; float: left; text-align: center;">За день <br><?= $pays['day']?></span>
                    <!-- </h4> -->
                    <br>
                    <br>
                </div>
                <div class="stats-link">
                    <a href="<?=Url::to(['/pays/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12"  >
            <div class="widget widget-stats bg-purple" >

                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info p-0 desc-page-container">
                    <p>Задачи</p>
                    <?php
                    $title = '';
                    $values = '';
                    $i = 0;
                    foreach ($allStatuses as $key => $value) {
                        if($i == 0) {
                            /*$title = $value['key'];
                            $values = $value['value'];*/

                            $values .='<span style="position: relative; float: left; text-align: center;"> ' .  $value['key'] . ' <br> ' . $value['value'] . '</span>' ;

                            /* echo $value['key'], "<br>";
                             echo $value['value'];*/

                            // echo "<div class='col-md-12'>", $value['key'], "</div>  ","<br>", $value['value'];
                            // echo "<br>", $value['value'];


                        }
                        else {
                            // $title .= ' / ' . $value['key'];
                            $values .='<span style="position: relative; float: left; text-align: center;"> / ' .  $value['key'] . ' <br> ' . $value['value'] . '</span>' ;

                            // echo  " / ",  $value['key'];
                            // echo "<i style='width: 50px; text-align: left'>" $value['value'], "</i>";


                        }
                        $i = 1;
                    }
                    ?>

                    <!-- <h4><?=$title?></h4> -->
                    <h4><?=$values?></h4>
                </div>

                <div class="stats-link">
                    <a href="<?=Url::to(['/tasks/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <!-- <div class="col-md-3 col-sm-6">
        <div class="widget widget-stats bg-red">
            <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
            <div class="stats-info">
                <p>СУММА ОБЗВОН</p>
                <h4>Всего \ За месяц \ За неделю \ За день </h4>
                <h4>
                    <span style="margin-left: 4%;"> <?php // $calling['all']?></span>
                    <span style="margin-left: 14%;"><?php // $calling['month']?></span>
                    <span style="margin-left: 18%;"><?php // $calling['week']?></span>
                    <span style="margin-left: 22%;"><?php // $calling['day']?></span>
                </h4>

            </div>
            <div class="stats-link">
                <a href="<?php //Url::to(['/calling/index'])?>">Подробнее... <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div> -->

    </div>

    <div class="row">

        <div class="col-md-8">
            <div class="panel panel-inverse" data-sortable-id="index-5">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Воронка</h4>
                </div>
                <div class="panel-body page-container-width desc-page-container">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                        <div  data-scrollbar="true" data-init="true" style="overflow: hidden; width: auto; height: auto;">
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed">
                                    <tr>
                                        <th>Статус / Проекты</th>
                                        <?php
                                        foreach ($projects as $project) {
                                            ?>
                                            <th><?=$project->name?></th>
                                        <?php  }  ?>
                                    </tr>

                                    <tr>
                                        <th>Посещение</th>
                                        <?php
                                        foreach ($projects as $project) {
                                            $count = 0;
                                            $sites = Sites::find()->where(['project_id' => $project->id])->all();
                                            foreach ($sites as $site) {
                                                $a = Visitors::find()->where(['site_id' => $site->id])->count();
                                                $count += $a;
                                            }
                                            ?>
                                            <td><?=$count?></td>
                                        <?php  }  ?>
                                    </tr>

                                    <tr>
                                        <th>Заявки</th>
                                        <?php
                                        foreach ($projects as $project) {
                                            $count = 0;
                                            $sites = Sites::find()->where(['project_id' => $project->id])->all();
                                            foreach ($sites as $site) {
                                                $visitors = Visitors::find()->where(['site_id' => $site->id])->all();
                                                $ips = [];
                                                foreach ($visitors as $visitor) {
                                                    $ips [] = $visitor->ip;
                                                }
                                                $ips = array_unique($ips);
                                                foreach ($ips as $ip) {
                                                    $a = Clients::find()->where(['ip' => $ip, 'project_id' => $project->id, 'client' => 0])->count();
                                                    $count += $a;
                                                }
                                            }
                                            ?>
                                            <td><?=$count?></td>
                                        <?php  }  ?>
                                    </tr>


                                    <?php
                                    foreach ($statuses as $status)
                                    {

                                        echo '<tr><th style="background-color:'.$status->color.';">'. $status->name."</th>";
                                        foreach ($projects as $project)
                                        {
                                            $count = Clients::find()->where(['lid_id' => $status->id, 'project_id' => $project->id, 'client' => 0 ])->count();
                                            echo '<td style="background-color:'.$status->color.';">'.$count."</td>";
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin col-8 -->
        <div class="col-md-4 ui-sortable">
            <div class="panel panel-inverse" data-sortable-id="index-8">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Задачи на сегодня</h4>
                </div>
                <div class="panel-body p-0 page-container-width desc-page-container">
                    <ul class="todolist">
                        <?php foreach ($tasks as $task) { $url = Url::to(['tasks/view-from-dashboard', 'id' => $task->id]); ?>

                            <li style="margin-left: 5px; background-color:<?=$task->status0->color;?>">
                                <a href="<?=$url?>" class="todolist-container" data-click="todolist" role="modal-remote" >
                                    <div class="todolist-title" ><?= '№ ' . $task->id . '; Клиент :' . $task->client0->fio ?></div>
                                </a>
                            </li>

                        <?php } ?>


                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php

    $this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);

    ?>

</div>
