<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Companies */

?>
<div class="companies-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
