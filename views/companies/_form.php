<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="companies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'admin_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'id', 'fio')) ?>

    <?= $form->field($model, 'access_end_datetime')->widget(\kartik\date\DatePicker::class, [
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]) ?>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'prosent_referal')->textInput()?></div>
        <div class="col-md-4"><?= $form->field($model, 'partner_balance')->textInput()?></div>
        <div class="col-md-4"><?= $form->field($model, 'main_balance')->textInput()?></div>
    </div>
<div class="row">
    <div class="col-md-6"><?= $form->field($model, 'cost_day')->textInput()?></div>
    <div class="col-md-6"><?= $form->field($model, 'cost_person')->textInput()?></div>
</div>





  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
