<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<!-- 
    
        <div class="panel panel-inverse" data-sortable-id="index-8">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Поиск</h4>
            </div>
            <div class="panel-body">
                
            </div>
        </div>
<br>
 --><div class="panel panel-inverse users-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Задачи</h4>
    </div>
    <div class="panel-body">
        <?php echo $this->render('_search', ['model' => $searchModel, 'post' => $post]); ?>
                <!-- <p style="margin-left: 41px;">
                    <a class="btn btn-warning" href="<?php // Url::toRoute(['tasks/urgent-tasks-notify'])?>">Сделать рассылку уведомлений срочных задач</a>
                </p> -->
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax'=>true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить задач', 'style' => 'margin-left:30px;', 'class'=>'btn btn-success']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
