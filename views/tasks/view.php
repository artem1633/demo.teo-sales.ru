<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
?>
<div class="tasks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'manager',
                'value' => function ($data) {
                   return $data->manager0->fio;
                },
            ],
            [
                'attribute' => 'client',
                'format' => 'raw',
                'value' => function ($data) {
                   //return $data->client0->fio;
                   return Html::a($data->client0->fio, ['/clients/view', 'id' => $data->client], ['data-pjax' => 0 ]);
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                   return $data->status0->name;
                },
            ],
            'data',
            'time',
            //'text:ntext',
            [
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function ($data) {
                   return '<div style="width:30%; white-space: normal;">
                                <p>
                                    <span style="color:rgb(0, 0, 0)">
                                       '.Html::decode($data->text).'
                                    </span>
                                </p>
                            </td>';
                },
            ],
            'date_cr',
            'date_up',
        ],
    ]) ?>

</div>
