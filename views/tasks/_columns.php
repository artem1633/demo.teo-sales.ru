<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\TaskStatuses;
use app\models\Clients;
use app\models\Users;

$company_id = Yii::$app->user->identity->company_id;

return [
    //[
        //'class' => 'kartik\grid\CheckboxColumn',
        //'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
        'filter' => ArrayHelper::map(Clients::find()->where(['company_id' => $company_id ])->orderBy('id DESC')->all(), 'id', 'fio'),
        'content' => function ($data) {
           if($data->client0->client == 1) return $data->client0->fio .' <a data-pjax=0 role="modal-remote" class="glyphicon glyphicon-eye-open" href="'.Url::toRoute(['clients/view-ajax-client', 'id' => $data->client]).'"></a>';
           else return $data->client0->fio .' <a data-pjax=0 role="modal-remote" class="glyphicon glyphicon-eye-open" href="'.Url::toRoute(['clients/view-ajax-lid', 'id' => $data->client]).'"></a>';
       },
    ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'manager',
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
         'filter' => ArrayHelper::map(Users::find()->where(['company_id' => $company_id ])->all(), 'id', 'fio'),
         'content' => function ($data) {
           return $data->manager0->fio;
       },
     ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
         'filter' => ArrayHelper::map(TaskStatuses::find()->where(['company_id' => $company_id ])->all(), 'id', 'name'),
         'content' => function ($data) {
           return $data->status0->name;
       },
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'tasks_type_id',
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
         //'filter' => ArrayHelper::map(TaskStatuses::find()->where(['company_id' => $company_id ])->all(), 'id', 'name'),
         'content' => function ($data) {
           return $data->tasksType->name;
       },
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'time',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
        'format' => 'raw',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'width:30%; white-space: normal;color:black;background-color:'.$model->status0->color ];
        },
        //'contentOptions' => ['style' => 'width:30%; white-space: normal;'],
        'content' => function ($data) {
            return substr($data->text, 0, 300);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'result_text',
        'format' => 'raw',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'width:30%; white-space: normal;color:black;background-color:'.$model->status0->color ];
        },
        //'contentOptions' => ['style' => 'width:30%; white-space: normal;'],
        'content' => function ($data) {
            return substr($data->result_text, 0, 300);
        },
    ],
    //[
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'date_cr',
    //],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_up',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'], 
    ],

];   