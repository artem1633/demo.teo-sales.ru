<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
?>
<div class="tasks-update">

    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>

</div>
