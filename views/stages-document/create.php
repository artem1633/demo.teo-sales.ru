<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StagesDocument */

?>
<div class="stages-document-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
