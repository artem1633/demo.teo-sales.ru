<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StagesDocument */
?>
<div class="stages-document-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
