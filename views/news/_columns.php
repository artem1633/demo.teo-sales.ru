<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Users;
use yii\helpers\Html; 
return [
    /*[
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'image',
        'label' => 'Изображение',
        'contentOptions' => ['style'=>'width: 100px;'],
        'content' => function ($data) {
            return '<center>' . Html::img('/uploads/'.$data->image, [ 'style' => 'height:100px;width:100px;' ]) . '</center>';
            //<img src="../assets/img/user/user-1.jpg" class="img-rounded height-30">
            //return Html::a($data->image, ['/news/send-file', 'file' => $data->image],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'header',
        /*'content' => function ($data) {
            if(Yii::$app->user->identity->type == 1 && Yii::$app->user->identity->company_id == null) return '<a role="modal-remote" class="btn btn-default btn-xs" href="'.Url::toRoute(['view', 'id' => $data->id]).'">'.$data->header.'</a>';
            else return $data->header;
        },*/
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'contentOptions' => ['style' => 'width:30%; white-space: normal;'],
        'attribute'=>'text_post',
        'content' => function ($data) {
            return substr($data->text_post, 0, 300) . '...';             
         },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function ($data) {
             return $data->user->fio;
         },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'attached_file',
        'label' => 'Файл',
        'contentOptions' => ['style'=>'width: 30px;'],
        'content' => function ($data) {
            return Html::a('<center><span style="font-size:16px;" class="fa fa-download"></span></center>', ['/news/send-file', 'file' => $data->attached_file],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDatetime($data->date, 'php:H:i d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'header',
        'label' => '',
        'content' => function ($data) {
            return '<center><a role="modal-remote" class="btn btn-default btn-xs" href="'.Url::toRoute(['view', 'id' => $data->id]).'">Подробно</a></center>';
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{up} {update} {delete}',
        'visible' => (Yii::$app->user->identity->type == 1 && Yii::$app->user->identity->company_id == null) ? true : false,
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного новости?'], 
        'buttons'  => [            
            'up' => function ($url, $model) {
                $url = Url::to(['up', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
        ]
    ],

];   