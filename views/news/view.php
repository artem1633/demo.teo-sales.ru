<?php

use yii\widgets\DetailView;
use yii\helpers\Html; 

?>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="table-responsive">                                    
                    <table class="table table-bordered">
                        <tbody >
                            <td rowspan="4" width="400px" height="400px" style="text-align: center;">
                                <?=($model->image)?Html::img('/uploads/'.$model->image,['style'=>'width:400px; height:400px']):'Не задано'; ?>
                            </td>                                      
                        </tbody>
                    </table>  
                </div>
            </div>   
            <div class="col-md-12 col-sm-12">
               <div >
                   <table class="table table-bordered">
                       <tbody>
                           <tr>
                               <th ><b><?=$model->getAttributeLabel('text_post')?></b></th>
                           </tr>                                                    
                           <tr>
                               <td style="width:30%; white-space: normal;">
                                    <p>
                                        <span style="color:rgb(0, 0, 0)">
                                            <?=Html::decode($model->text_post)?>
                                        </span>
                                    </p>
                                </td>            
                           </tr>
                       </tbody>
                   </table>  
               </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="table-responsive">                                    
                    <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th><b><?=$model->getAttributeLabel('header')?></b></th>
                                        <th><b><?=$model->getAttributeLabel('user_id')?></b></th>
                                    </tr>
                                    <tr>
                                        <td><?=Html::encode($model->header)?></td>
                                        <td><?=Html::encode($model->user->fio)?></td>
                                    </tr>
                                    <tr>
                                        <th><b><?=$model->getAttributeLabel('date')?></b></th>
                                        <th><b><?= $model->getAttributeLabel('attached_file')?></b></th>
                                    </tr>
                                    <tr>      
                                        <td><?=Html::encode(\Yii::$app->formatter->asDatetime($model->date, 'php:H:i d.m.Y'))?></td>
                                        <td><?= Html::a('Скачать', ['/news/send-file', 'file' => $model->attached_file],['title'=> 'Скачать', 'data-pjax' => 0])?></td>
                                    </tr>
                                </tbody>
                    </table>  
                </div>
            </div>
        </div>



<!-- <div class="news-view" style="width: 100%; overflow: auto">
    <?php /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'header',
            'text_post:html',
            [
                'attribute' => 'user_id',
                'value' => function ($data) {
                   return $data->user->fio;
                },
            ],
            [
                    'attribute' => 'date',
                    'value' => function ($data) {
                     if($data->date != null )  return \Yii::$app->formatter->asDate($data->date, 'php:H:i d.m.Y');
                    },
            ],
            [
                'attribute'=>'image',
                'format'=>'html', 
                //'visible' => $data->image == null) ? false : true, 
                'value' => function ($data) {
                    //return Html::a($data->image, ['/news/send-file', 'file' => $data->image],['title'=> 'Скачать', 'data-pjax' => 0]);
                    return Html::img('/uploads/'.$data->image, [ 'style' => 'height:30px;width:30px;' ]);
                },
            ],
            [
                'attribute'=>'attached_file',
                'format'=>'html',  
                //'visible' => $data->attached_file == null ? false : true,
                'value' => function ($data) {
                    return Html::a($data->attached_file, ['/news/send-file', 'file' => $data->attached_file],['title'=> 'Скачать', 'data-pjax' => 0]);
                },
            ],
        ],
    ])*/ ?>
</div> -->
