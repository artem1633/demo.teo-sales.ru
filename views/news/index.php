<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Новости";
$this->params['breadcrumbs'][] = $this->title;

if(Yii::$app->user->identity->type == 1 && Yii::$app->user->identity->company_id == null) $super_admin = true;
else $super_admin = false;

//echo "super_admin=".$super_admin;die;

CrudAsset::register($this);

?>
<div class="panel panel-inverse settings-index">
    <div class="panel-heading">
        <div class="btn-group pull-right">
            <?= $archive == 0 ? Html::a('Архив новости', ['index', 'archive' => 1],[ 'data-pjax' => 0, 'class'=>'btn btn-warning btn-xs']) : Html::a('Актуалные новости', ['index', 'archive' => 0],[ 'data-pjax' => 0, 'class'=>'btn btn-warning btn-xs'])?>
        </div>
        <h4 class="panel-title">Новости</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    $super_admin ? (Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить'])) : '',
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
