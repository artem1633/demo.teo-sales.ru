<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?> 

<div class="news-form"> 

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
     <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>        
        </div>
    </div>
     <div class="row">
         <div class="col-md-12">
                    <?php echo $form->field($model, 'text_post')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                            'height' => '200px',
                        ],
                    ]);
                    ?>
                </div>
    </div>
     <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_image')->fileInput() ?>            
        </div>
         <div class="col-md-4">
            <?= $form->field($model, 'other_attached_file')->fileInput() ?>            
        </div>
    </div>
     <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'fixing')->checkbox(); ?>
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'arxiv')->textInput(['value' => 0]) ?>
        <?= $form->field($model, 'user_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>        
    </div>
    
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
