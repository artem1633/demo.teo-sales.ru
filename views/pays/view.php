<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pays */
?>
<div class="pays-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'data',
            'summa',
            'comment:ntext',
            'user_id',
        ],
    ]) ?>

</div>
