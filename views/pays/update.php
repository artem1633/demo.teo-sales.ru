<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pays */
?>
<div class="pays-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
