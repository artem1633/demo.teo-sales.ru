<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pays */

?>
<div class="pays-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
