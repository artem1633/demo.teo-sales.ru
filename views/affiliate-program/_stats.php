<?php

use app\models\Companies;
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    app\models\USers,
    kartik\grid\GridView;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Партнерские отчисления</h3>
    </div>
    <div class="panel-body">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $retentionProvider,
            //'filterModel' => $retentionSearchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'toolbar' => false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'columns' => [

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'company_id',
                    'label' => '(Компания) Кому начисляем',
                    'content' => function ($model) {
                        $company = Users::findOne($model->company_id);
                        return $company->fio;
                    },
                    'visible' => Yii::$app->user->identity->type == 0,
                ],

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'referal_id',
                    'label' => 'Реферал',
                    'content' => function ($model) {
                        $referal = Companies::findOne($model->referal_id);
                        return $referal->admin->fio;
                        //return $model->referalModel ? $model->referalModel->company_name : null;
                    }
                ],
                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'date_transaction',
                    'content' => function ($model) {
                        return date('H:i d.m.Y', strtotime($model->date_transaction));
                    }
                ],

                [
                    'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'amount',
                    'content' => function ($model) {
                        return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                    },
                    'pageSummary' => function ($summary) {
                        return Html::tag(
                            'div',
                            $summary ? 'Итого: ' .number_format($summary, 2, '.', '') . ' <i class="fa fa-rub"></i>' : null,
                            ['data-sum' => $summary, 'class' => 'text-success']
                        );
                    }
                ],

            ],
            'showPageSummary' => true,
        ]); ?>
    </div>
    <div class="panel-footer"></div>
</div>

