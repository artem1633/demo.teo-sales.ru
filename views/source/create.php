<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Source */

?>
<div class="source-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
