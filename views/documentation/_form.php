<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Documentation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="documentation-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'client_id')->dropDownList($model->getClients(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'working')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]); 
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
           <?= $form->field($model, 'documents_type_id')->dropDownList($model->getDocumentstypes(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'stage_id')->dropDownList($model->getStagesList(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
           <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ownership')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>

    <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_file')->fileInput() ?>
            <?= $form->field($model, 'company_id')->textInput() ?>
        </div>
    </div>
     <div style="display: none;">
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>    
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
