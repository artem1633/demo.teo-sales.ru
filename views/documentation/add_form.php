<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
 
?>

<div class="call-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'working')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]); 
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'stage_id')->dropDownList($model->getStagesList(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
    </div>

    <div class="row">
         <div class="col-md-4">
           <?= $form->field($model, 'documents_type_id')->dropDownList($model->getDocumentstypes(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-4">
           <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ownership')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>

    <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_file')->fileInput() ?>            
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>    
        <?= $form->field($model, 'client_id')->textInput() ?>     
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
