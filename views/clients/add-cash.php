<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
?>
<div class="clients-update">

    <?= $this->render('cash_form', [
        'model' => $model,
    ]) ?>

</div>
