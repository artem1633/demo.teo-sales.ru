<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;
use dosamigos\datepicker\DatePicker;
use kartik\datetime\DateTimePicker;
use app\models\Contacts;
use app\models\Clients;

$users = \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->with('admin')->all(), 'id', 'admin.fio');
$statuses = \yii\helpers\ArrayHelper::map(\app\models\Statuses::find()->all(), 'id', 'name');
$visible = false;
if(\Yii::$app->user->identity->type == 1) $visible = true;
if(\Yii::$app->user->identity->company_id != null) $visible = false;

return [
    //[
        //'class' => 'kartik\grid\CheckboxColumn',
        //'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color,
             'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'content' => function ($data) {
            $contacts = Contacts::find()->where(['client_id' => $data->id ])->all();
            $text = '';
            $i = 0;
            foreach ($contacts as $value) {
                if($value->phone != null || $value->phone != '')
                {
                    if($i == 1) $text .= '<br>' . $value->phone;
                    else $text .= $value->phone;
                }
                $i = 1;
            }
           return $text;
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project_id',
        'filter' => Clients::getUsersProjects(),
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color,
                 'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'content' => function ($data) {
           return $data->project->name;
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'source_id',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'content' => function ($data) {
           return $data->source->name;
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'content' => function ($data) {
           return $data->status0->name;
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'planned_date',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'filter' => DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'planned_date',
            'template' => '{addon}{input}',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
        'format' => 'html',
    ],
     [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_cr',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'content' => function ($data) {
           return $data->date_cr;
       },
       'filter' => DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'date_cr',
            'template' => '{addon}{input}',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_up',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'filter' => DateTimePicker::widget([
            'model' => $searchModel,
            'attribute' => 'date_up',
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]),
        'content' => function ($data) {
           return $data->date_up;
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Компания',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->status0->color ,
                'onclick'=>'window.open("'.Url::to(['/clients/view', 'id' => $model->id]).'","_self");' ];
        },
        'visible' => $visible,
        'attribute'=>'company_id',
        'value' => 'company.admin.fio',
        'filter' => $users,
    ],
    /*[
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax' => 0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],*/

    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/clients/view', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['data-pjax'=>'0','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                if(Yii::$app->user->identity->type != 2){
                    $url = Url::to(['/clients/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этого клиента?'
                    ]);
                }
            },
        ]
    ]

];   