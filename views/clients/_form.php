<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'menejer')->dropDownList($model->getManager(), ['prompt' => 'Выберите менежера']) ?>
        </div>        
        <div class="col-md-4">
            <?= $form->field($model, 'project_id')->dropDownList($model->getProjects(), ['prompt' => 'Выберите проекта']) ?>
        </div>   
    </div>

    <div class="row">
        <div class="col-md-3">                
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>     
        <div class="col-md-3">
            <?= $form->field($model, 'source_id')->dropDownList($model->getSourceList(), ['prompt' => 'Выберите статуса']) ?>
        </div>
        <?php if($model->client == 1){ ?>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->getStatuses(), ['prompt' => 'Выберите статуса']) ?>
        </div>  
        <?php } else { ?>
        <div class="col-md-3">
            <?= $form->field($model, 'lid_id')->dropDownList($model->getStatusLids(), ['prompt' => 'Выберите статуса']) ?>
        </div>       
        <?php } ?> 
        <div class="col-md-3">
            <?= $form->field($model, 'planned_date')->widget(
                DatePicker::className(), [
                    'inline' => false,
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ?>
        </div>        
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'utm')->textInput(['maxlength' => true]) ?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>
        </div>        
        <div class="col-md-4">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        </div>   
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?> 
        </div>       
    </div>

   <!--  <div class="row">
       <div class="col-md-12">
       <?php 
      
           /*echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                   //'max' => 4,
                   //'min' => 3,
                   'id' => 'my_id',
                   'columns' => [
                       [
                           'name'  => 'fio',
                           'title' => 'ФИО',
                           'enableError' => true,
                           'options' => [
                               'class' => 'input-priority'
                           ]
                       ],
                       [
                           'name'  => 'position',
                           'title' => 'Должность',
                           //'defaultValue' => 'a',
                           'enableError' => true,
                           'options' => [
                               'class' => 'input-priority',
                           ]
                       ],
                       [
                           'name'  => 'phone',
                           'title' => 'Телефон',
                           'enableError' => true,
                           'options' => [
                               'class' => 'input-priority'
                           ]
                       ],
                       [
                           'name'  => 'email',
                           'title' => 'Email',
                           'enableError' => true,
                           'options' => [
                               'class' => 'input-priority'
                           ]
                       ],              
                   ]
               ])->label('Контакты');*/ ?>
       </div>
   </div> -->

    <div style="display: none;">
        <?= $form->field($model, 'date_cr')->textInput([]) ?>
        <?= $form->field($model, 'client')->dropDownList($model->getClient(), ['prompt' => 'Выберите']) ?>
    </div>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
