<?php

use app\models\Hints;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use yii\helpers\HtmlPurifier;
use app\models\Clients;
use app\models\Tasks;
use app\models\RelationsClient;
use rmrevin\yii\module\Comments;
use app\models\Templates;


\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Просмотр/Изменение';

$dostup = 0;
$tip = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;

if($tip == 6 | $tip == 1 | $tip == 3) $dostup = 1;

?>
    
<div class="box box-default">
    <div class="box-body">
        <div class="clients-view">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs">Общая информация</span>
                                <span class="hidden-xs">Общая информация</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Оплаты клиентом</span>
                                <span class="hidden-xs">Оплаты клиентом</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Анкета </span>
                                <span class="hidden-xs">Анкета </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Задачи </span>
                                <span class="hidden-xs">Задачи </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-5" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Документы </span>
                                <span class="hidden-xs">Документы </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-6" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Комментарии </span>
                                <span class="hidden-xs">Комментарии </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#default-tab-7" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">История с клиентом</span>
                                <span class="hidden-xs">История с клиентом</span>
                            </a>
                        </li>
                        <!-- <li class="">
                            <a href="#default-tab-8" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs">Шаблоны документов</span>
                                <span class="hidden-xs">Шаблоны документов</span>
                            </a>
                        </li> -->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="default-tab-1">
                            <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'base-pjax']) ?>
                                    <div class="col-md-7 col-sm-12">
                                    <h3>
                                        Общие элементы
                                        <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>

                                        <a class="btn btn-warning pull-right" data-pjax="0" href="<?=Url::toRoute(['set-lid', 'id' => $model->id])?>">Перевести клиент в лида</a>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success"> <b><i class="fa fa-print"></i> </b></button>
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu" role="menu">
                                                <?php
                                                $templates = Templates::find()->where(['project_id' => $model->project_id])->all();
                                                foreach ($templates as $value) { ?>
                                                <li><?= Html::a( $value->name, ['/clients/print', 'template_id' => $value->id, 'client_id' => $model->id ], ['data-pjax'=>'0','title'=>'', 'target' => '_blank', 'data-toggle'=>'tooltip'])?></li>
                                                <?php } ?>
                                            </ul>

                                        </div>
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-hint2">
                                            <i class="fa fa-question"></i>
                                        </button>
                                    </h3>
                                    <div style="margin-top: 10px" class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('fio')?></b></td>
                                                <td><?=Html::encode($model->fio)?></td>
                                                <td><b><?=$model->getAttributeLabel('date_cr')?></b></td>
                                                <td><?=\Yii::$app->formatter->asDate($model->date_cr, 'php:d.m.Y')?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('city')?></b></td>
                                                <td><?=Html::encode($model->city)?></td>
                                                <td><b><?=$model->getAttributeLabel('project_id')?></b></td>
                                                <td><?= $model->project->name?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('planned_date')?></b></td>
                                                <td><?=Html::encode($model->planned_date)?></td>
                                                <td><b><?=$model->getAttributeLabel('menejer')?></b></td>
                                                <td><?= $model->menejer0->fio?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('status')?></b></td>
                                                <td><?=Html::encode($model->status0->name)?></td>
                                                <td><b><?=$model->getAttributeLabel('client')?></b></td>
                                                <td><?= $model->client == 1 ? 'Да' : 'Нет'?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('source_id')?></b></td>
                                                <td><?= $model->source->name?></td>
                                                <td><b><?=$model->getAttributeLabel('utm')?></b></td>
                                                <td><?= $model->utm?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('ip')?></b></td>
                                                <td><?=$model->link?></td>
                                                <td><b><?=$model->getAttributeLabel('link')?></b></td>
                                                <td><?=$model->ip?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?=$model->getAttributeLabel('comment')?></b></td>
                                                <td colspan="3"><?=Html::encode($model->comment)?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12">
                                    <h3>Контакты <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['/contacts/add', 'client_id' => $model->id])?>"><i class="fa fa-plus"></i></a>
                                    </h3>
                                    <?=GridView::widget([
                                        'dataProvider' => $contactsdataProvider,
                                        //'filterModel' => $contactssearchModel,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_contacts_columns.php'),
                                        'panelBeforeTemplate' => '',
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'showPageSummary' => false,
                                    ])?>

                                    <?php if ($model->id_send) :?>
                                    <?php $myHistory = json_decode(file_get_contents("https://app.vkrassilka.ru/api/client/chatsls?id={$model->id_send}"));?>
                                    <div>
                                        <!-- DIRECT CHAT -->
                                        <div class="box box-warning direct-chat direct-chat-warning">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">
                                                    <?= Html::a('Переейти в диалог', 'https://app.vkrassilka.ru/dispatch-status/dialog?id='.$model->id_send, [ 'target' => '_blank',  'data-pjax'=>0]) ?>
                                                </h3>

                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <!-- Conversations are loaded here -->
                                                <div class="direct-chat-messages">

                                                    <?php foreach ($myHistory->history as $msg):?>
                                                        <!-- Message. Default to the left -->
                                                        <?php if($msg->from != 'client'):?>
                                                            <div class="direct-chat-msg">
                                                                <div class="direct-chat-info clearfix">
                                                                    <div class="direct-chat-text">
                                                                        <?= $msg->text ?>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php else:?>
                                                            <div class="direct-chat-msg right">
                                                                <div class="direct-chat-text">
                                                                    <?= $msg->text ?>
                                                                </div>
                                                            </div>
                                                        <?php endif;?>

                                                    <?php endforeach;?>
                                                </div>
                                                <!--/.direct-chat-messages-->

                                            </div>
                                            <!-- /.box-body -->
                                            <!--/.direct-chat -->
                                        </div>
                                    </div>

                            <?php endif; ?>
                                </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-2">
                            <div class="row">

                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'cash-pjax']) ?>
                                    <div class="col-md-11" style="margin-left: 40px;">
                                        <h3>Добавить
                                            <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['add-cash', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                                        </h3>
                                        <br>
                                        <?php $pays =\app\models\Pays::find()->where(['client_id' => $model->id])->all();
                                            if($pays == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет оплаты </b></center></span>';}
                                            else {
                                        ?>
                                        <table class="table table-bordered table-condensed">
                                            <tr>
                                                <th>Дата/Время</th>
                                                <th>Сумма</th>
                                                <th>Комментария</th>
                                            </tr>
                                            <?php
                                            foreach ($pays as $change) {
                                                $username = Users::findOne($change->user_id);
                                            ?>
                                            <tr>
                                                <td><?=\Yii::$app->formatter->asDate($change->data, 'php:H:i, d.m.Y')?></td>
                                                <td><?=$change->summa?></td>
                                                <td><?=$change->comment?></td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                        <?php } ?>
                                    </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-3">
                            <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'three-pjax']) ?>
                                    <div class="col-md-11">
                                        <h3>Создать / Изменить анкету
                                            <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['/clients-field/add', 'client_id' => $model->id ])?>"><i class="fa fa-pencil"></i></a>
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-hint1">
                                                <i class="fa fa-question"></i>
                                            </button>
                                        </h3>
                                        <br>
                                        <table class="table table-bordered table-condensed">
                                            <tr>
                                                <th>№</th>
                                                <th>Наименование</th>
                                                <th>Значение</th>
                                            </tr>
                                            <?php
                                            $i = 0;
                                            foreach ($fielddataProvider->getModels() as $field) {
                                                $i++;
                                            ?>
                                            <tr>
                                                <td><?=$i?></td>
                                                <td><?=$field->field->label?></td>
                                                <td><?=$field->value?></td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-4">
                            <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'tasks-pjax']) ?>
                                    <div class="col-md-11" style="margin-left: 40px;"><br>
                                        <h3>
                                           Добавить
                                            <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['/tasks/add', 'id' => $model->id])?>"><i class="fa fa-plus"></i></a>
                                        </h3>
                                        <?php
                                            $tasks = Tasks::find()->where(['client' => $model->id])->all();
                                            if($tasks == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет задачи </b></center></span>';}
                                            else{ ?>
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Дата</th>
                                                        <th>Время</th>
                                                        <th>Статус</th>
                                                        <th>Тип задачи</th>
                                                        <th>Текст</th>
                                                        <th>Результат звонка</th>
                                                        <th>Действие</th>
                                                    </tr>

                                        <?php
                                            foreach ($tasks as $task) {
                                        ?>
                                                    <tr>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=\Yii::$app->formatter->asDate($task->data, 'php:d.m.Y')?></td>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=$task->time?></td>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=$task->status0->name?></td>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=$task->tasksType->name?></td>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=$task->text?></td>
                                                        <td style="color:black;font-weight:bold;background-color:<?=$task->status0->color?>;"><?=$task->result_text?></td>
                                                        <td>
                                                            <?= Html::a('<i class="fa fa-pencil text-info" style="font-size: 16px;"></i>', ['/tasks/change','id'=>$task->id], [ 'role'=>'modal-remote', 'title'=>'Изменить', ]) ?>
                                                            <?= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['/clients/delete-tasks','id'=>$task->id], [
                                                                'role'=>'modal-remote', 'title'=>'Удалить',
                                                                'data-confirm'=>false, 'data-method'=>false,
                                                                'data-request-method'=>'post',
                                                                'data-confirm-title'=>'Вы уверены?',
                                                                'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                                            ]) ?>
                                                        </td>
                                                    </tr>
                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?>
                                    </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-5">
                            <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'documentation-pjax']) ?>
                                    <div class="col-md-12">
                                        <div class="panel-body">
                                            <div id="ajaxCrudDatatable">
                                                <?=GridView::widget([
                                                'id'=>'documentation-datatable',
                                                'dataProvider' => $documentationdataProvider,
                                                'pjax'=>true,
                                                'columns' => require(__DIR__.'/_documents_columns.php'),
                                                'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['/documentation/add', 'client_id' => $model->id],
                                                        ['role'=>'modal-remote','title'=> 'Добавить','class'=>'btn btn-success']),
                                                'striped' => true,
                                                'condensed' => true,
                                                'responsive' => true,
                                                'panel' => [
                                                'headingOptions' => ['style' => 'display: none;'],
                                                'after'=>'',
                                                ]
                                                ])?>
                                            </div>
                                        </div>
                                    </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-6">
                            <?php echo Comments\widgets\CommentListWidget::widget(['entity' => (string) $model->id,]);?>
                        </div>
                        <div class="tab-pane fade" id="default-tab-7">
                            <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'changing-pjax']) ?>
                                    <div class="col-md-11" style="margin-left: 40px;"><br>
                                    <br><br>
                                        <?php $changes =\app\models\Changing::find()->where(['table_name' => 'client', 'field' => 'Смена статуса', 'line_id' => $model->id])->all();
                                            if($changes == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет изменений </b></center></span>';}
                                            else {
                                        ?>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-condensed">
                                                <tr>
                                                    <th>№</th>
                                                    <th>Дата/Время</th>
                                                    <th>Пользователь (Логин)</th>
                                                    <th>Поле</th>
                                                    <th>Значение</th>
                                                    <th>Изменение</th>
                                                </tr>
                                                <?php $i=0;
                                                foreach ($changes as $change) {
                                                    $i++;
                                                    $username = Users::findOne($change->user_id);
                                                ?>
                                                <tr>
                                                    <td><?= $i?></td>
                                                    <td><?= \Yii::$app->formatter->asDate($change->date_time, 'php:H:i, d.m.Y')?></td>
                                                    <td><?= $username->login?></td>
                                                    <td><?= $change->field?></td>
                                                    <td><?= $change->old_value?></td>
                                                    <td><?= $change->new_value?></td>
                                                </tr>
                                                <?php  } ?>
                                            </table>
                                        </div>
                                        <?php  } ?>
                                    </div>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>

<?php Modal::begin([
    "id"=>"modal-hint1",
    "footer"=>"<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">ОК</button>",// always need it for jquery plugin

]);
echo Hints::getHintsById(1)->hint;
Modal::end(); ?>
<?php Modal::begin([
    "id"=>"modal-hint2",
    "footer"=>"<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">ОК</button>",// always need it for jquery plugin

]);
echo Hints::getHintsById(2)->hint;
Modal::end(); ?>
