<?php
use yii\helpers\Html;
use kartik\grid\GridView;

?>

<div class="table-responsive">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <td><b><?=$model->getAttributeLabel('fio')?></b></td>
            <td><?=Html::encode($model->fio)?></td>
            <td><b><?=$model->getAttributeLabel('date_cr')?></b></td>
            <td><?=\Yii::$app->formatter->asDate($model->date_cr, 'php:d.m.Y')?></td>                                    
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('city')?></b></td>
            <td><?=Html::encode($model->city)?></td>
            <td><b><?=$model->getAttributeLabel('project_id')?></b></td>
            <td><?= $model->project->name?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('planned_date')?></b></td>
            <td><?=Html::encode($model->planned_date)?></td>
            <td><b><?=$model->getAttributeLabel('menejer')?></b></td>
            <td><?= $model->menejer0->fio?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('status')?></b></td>
            <td><?=Html::encode($model->status0->name)?></td>
            <td><b><?=$model->getAttributeLabel('client')?></b></td>
            <td><?= $model->client == 1 ? 'Да' : 'Нет'?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('source_id')?></b></td>
            <td><?= $model->source->name?></td>
            <td><b><?=$model->getAttributeLabel('utm')?></b></td>
            <td><?= $model->utm?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('ip')?></b></td>
            <td><?=$model->link?></td>
            <td><b><?=$model->getAttributeLabel('link')?></b></td>
            <td><?=$model->ip?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('comment')?></b></td>
            <td colspan="3"><?=Html::encode($model->comment)?></td>
        </tr>                                   
        </tbody>
    </table>
    <div class="col-md-12 col-sm-12"> 
        <?=GridView::widget([
            'dataProvider' => $contactsdataProvider,
            //'filterModel' => $contactssearchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_client_view_ajax_contact.php'),
            'panelBeforeTemplate' => '',
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'showPageSummary' => false,
        ])?>
    </div>
</div>
