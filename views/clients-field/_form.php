<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ClientsField;
use app\models\Fields;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsField */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-field-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php foreach ($fields as $field_value): ?>

        <?php 
            $field_id = Fields::find()->where(['name' => $field_value->name, 'project_id' => $project_id])->one()->id;
            $client_field = ClientsField::find()->where(['field_id' => $field_id, 'client_id' => $client_id])->one();
            if($client_field == null) $value = null;
            else $value = $client_field->value;

/*            echo "<pre>";
                print_r($client_field);
                echo "</pre>";
                die;*/
        ?>

        <?php 
        	if($field_value->type == 'number') {
        		$fieldsModel[$field_value->name] = $value;
        ?>
        	<?= $form->field($fieldsModel, $field_value->name)->textInput(['type' => $field_value->type, 'value' => $value ])->label($field_value->label) ?>

    	<?php } ?>

    	<?php 
        	if($field_value->type == 'text') {
        ?>
        	<?= $form->field($fieldsModel, $field_value->name)->textInput(['type' => $field_value->type, 'value' => $value ])->label($field_value->label) ?>

    	<?php } ?>

    	<?php 
        	if($field_value->type == 'dropdown') {
        		$fieldsModel[$field_value->name] = $value;
        ?>
        	<?= $form->field($fieldsModel, $field_value->name)->dropDownList(array_combine(explode(',', $field_value->data), explode(',', $field_value->data)))->label($field_value->label) ?>

    	<?php } ?>

    <?php endforeach; ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
