<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsField */
?>
<div class="clients-field-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
