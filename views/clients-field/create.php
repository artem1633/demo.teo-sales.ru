<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClientsField */

?>
<div class="clients-field-create">
    <?= $this->render('_form', [
        'fields' => $fields,
        'fieldsModel' => $fieldsModel,
        'project_id' => $project_id,
        'client_id' => $client_id,
    ]) ?>
</div>
