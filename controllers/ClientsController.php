<?php

namespace app\controllers;

use Yii;
use app\models\Clients;
use app\models\ClientsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Pays;
use app\models\Tasks;
use app\models\RelationsClient;
use app\models\DocumentationSearch;
use app\models\Documentation;
use app\models\Contacts;
use app\models\ContactsSearch;
use app\models\ClientsFieldSearch;
use app\models\ClientsField;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use app\models\Templates;
/**
 * ClientsController implements the CRUD actions for Clients model.
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLids()
    {    
        $searchModel = new ClientsSearch();
        $dataProvider = $searchModel->searchLids(Yii::$app->request->queryParams);

        return $this->render('lids', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSendDocumentFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }


    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Клиент #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            $documentationsearchModel = new DocumentationSearch();
            $documentationdataProvider = $documentationsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $fieldsearchModel = new ClientsFieldSearch();
            $fielddataProvider = $fieldsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $contactssearchModel = new ContactsSearch();
            $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            return $this->render('view', [
                'model' => $this->findModel($id),
                'documentationdataProvider' => $documentationdataProvider,
                'contactssearchModel' => $contactssearchModel,
                'contactsdataProvider' => $contactsdataProvider,
                'fielddataProvider' => $fielddataProvider,
            ]);
        }
    }

    public function actionViewLid($id)
    {   
        $request = Yii::$app->request;
        $documentationsearchModel = new DocumentationSearch();
        $documentationdataProvider = $documentationsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        $contactssearchModel = new ContactsSearch();
        $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        $fieldsearchModel = new ClientsFieldSearch();
        $fielddataProvider = $fieldsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        return $this->render('view_lid', [
            'model' => $this->findModel($id),
            'documentationdataProvider' => $documentationdataProvider,
            'contactssearchModel' => $contactssearchModel,
            'contactsdataProvider' => $contactsdataProvider,
            'fielddataProvider' => $fielddataProvider,
        ]);
    }

    public function actionViewAjaxLid($id)
    {   
        $request = Yii::$app->request;
        $contactssearchModel = new ContactsSearch();
        $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "Клиент #".$id,
            'content'=>$this->renderAjax('lid_view_ajax', [
                'model' => $this->findModel($id),
                'contactsdataProvider' => $contactsdataProvider,
            ]),
        ];        
    }

    public function actionViewAjaxClient($id)
    {   
        $request = Yii::$app->request;
        $contactssearchModel = new ContactsSearch();
        $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "Клиент #".$id,
            'content'=>$this->renderAjax('client_view_ajax', [
                'model' => $this->findModel($id),
                'contactsdataProvider' => $contactsdataProvider,
            ]),
        ];        
    }

    public function actionSetClient($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->client = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionSetLid($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->client = 0;
        $model->save();

        return $this->redirect(['lids']);
    }

    /**
     * Creates a new Clients model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($client)
    {
        $request = Yii::$app->request;
        $model = new Clients();
        $model->client = $client;  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){

                $post = Yii::$app->request->post();
                $contact = $post['Clients']['contacts'];
               
                foreach ($contact as $value) {
                    $relative = new Contacts();
                    $relative->client_id = $model->id; 
                    $relative->fio = $value['fio'];
                    $relative->position = $value['position'];
                    $relative->phone = $value['phone'];
                    $relative->email = $value['email'];
                    $relative->save();
                    $error = $relative->errors;
                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно завершено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);     
        $last = $this->findModel($id);  
        $documentationsearchModel = new DocumentationSearch();
        $documentationdataProvider = $documentationsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        $contactssearchModel = new ContactsSearch();
        $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        $fieldsearchModel = new ClientsFieldSearch();
        $fielddataProvider = $fieldsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->setChanging($last, $model);
                return [
                    'forceReload'=>'#base-pjax',
                    'forceClose'=>true,
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('view', [
                        'documentationdataProvider' => $documentationdataProvider,
                        'contactssearchModel' => $contactssearchModel,
                        'contactsdataProvider' => $contactsdataProvider,
                        'fielddataProvider' => $fielddataProvider,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionAddCash($id)
    {
        $request = Yii::$app->request;
        $model = new Pays();
        $model->user_id = Yii::$app->user->identity->id;
        $model->client_id = $id;
        $client = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $client->balance = $client->balance + $model->summa;
                $client->save();
                return [
                    'forceReload'=>'#cash-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Добавить",
                    'size'=>'large',
                    'content'=>$this->renderAjax('add-cash', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
        else{
            if($model->load($request->post()) && $model->save()){
                $client->balance = $client->balance + $model->summa;
                $client->save();
                 return $this->redirect(['view', 'id' => $id]);
            }else{
                return $this->render('add-cash', [
                    'model' => $model,
                ]);
            }

        }
    }

    /**
     * Отдает документ в формате PDF
     * @param $id
     */
    public function actionPrint($template_id,$client_id)
    {
        $template = Templates::findOne($template_id);
        $tekst = $template->content;
        $tekst = str_replace ("{number_doc}", $template->sort_number, $tekst);
        $clientsField = ClientsField::find()->where([ 'client_id' => $client_id ])->all();
        foreach ($clientsField as $value) {
            $tekst = str_replace ("{".$value->field->name."}", $value->value, $tekst);
            /*if($value->field->type == 'checkbox' && $value->field->checkbox_type == 2)
            {
                $data = $value->field->images; 
                $data = json_decode($data, true);
                $images = '';
                foreach ($data as $image) 
                {
                    $images .= Html::img("http://".$_SERVER['SERVER_NAME'].'/uploads/'.$image['path'], ['style' => 'width: 100px; padding-top: 3px;']);
                }
                $tekst = str_replace ("{".$value->field->name."}", $images, $tekst);
            }
            else $tekst = str_replace ("{".$value->field->name."}", $value->value, $tekst);*/            
        }

        $mpdf = new Mpdf();
        $mpdf->SetTitle($model->name);
        $mpdf->WriteHTML($tekst);
        echo $mpdf->Output($model->name.'.pdf', Destination::INLINE);
    }

    /**
     * Delete an existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $tasks = Tasks::find()->where(['client' => $id])->all();
        foreach ($tasks as $value) {
            $value->delete();
        }

        $relations = RelationsClient::find()->where(['client' => $id])->all();
        foreach ($relations as $value) {
            $value->delete();
        }

        $documents = Documentation::find()->where(['client_id' => $id])->all();
        foreach ($documents as $value) {
            $value->delete();
        }

        $contact = Contacts::find()->where(['client_id' => $id])->all();
        foreach ($contact as $value) {
            $value->delete();
        }

        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Clients model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {

            $tasks = Tasks::find()->where(['client' => $pk])->all();
            foreach ($tasks as $value) {
                $value->delete();
            }

            $relations = RelationsClient::find()->where(['client' => $pk])->all();
            foreach ($relations as $value) {
                $value->delete();
            }

            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
