<?php

namespace app\controllers;

use Yii;
use app\models\Calling;
use app\models\CallingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\Statuses;
use app\models\UploadForm;
use app\models\Clients;
/**
 * CallingController implements the CRUD actions for Calling model.
 */
class CallingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calling models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CallingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => new UploadForm(),
        ]);
    }

    public function actionImport() 
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = $model->file->baseName . '.' . $model->file->extension;
                if ($model->load($request->post())) {
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $model->file->saveAs('uploads/' . $fileName);
                }
                
                $filePath = 'uploads/' . $fileName;
                $csvData = \PHPExcel_IOFactory::load($filePath);
                $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

                $count = 0;
                foreach ($csvData as $a) {
                    if ($count==0) {
                        $count ++;
                        continue;
                    }

                    $calling = new Calling();
                    $calling->fio = $a[0];
                    $calling->city = $a[1];
                    $calling->email = $a[2];
                    $calling->telephone = $a[3];
                    $calling->planned_date = date('Y-m-d', strtotime($a[5]));
                    $calling->user_id = Yii::$app->user->identity->id;
                    $calling->date_cr = date('Y-m-d');

                    $status = Statuses::find()->where(['name' => $a[4]])->one();
                    if($status == null){
                        $new_status = new Statuses();
                        $new_status->name = $a[4];
                        $new_status->number = 1;
                        $new_status->save();
                        $calling->status = $new_status->id;
                    }
                    else $calling->status = $status->id;
                    $call = Calling::find()->where(['fio'=>$calling->fio,'city'=>$calling->city,'email'=>$calling->email,'telephone'=>$calling->telephone,'planned_date'=>$calling->planned_date,'status'=>$calling->status])->one();
                    if($call == null) $calling->save(false);                    

                    $count++;
                }
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
        else
        { 
            return [
                'title'=> "Импорт",
                'size' => 'small',
                'content'=>$this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Импорт',['class'=>'btn btn-primary','type'=>"submit"])
            ]; 

        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * Displays a single Calling model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Обзвон #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Calling model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Calling();
        $model->date_cr = date('Y-m-d');

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новый вызов",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый вызов",
                    'content'=>'<span class="text-success">Создание вызова успешно завершено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать новый вызов",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Calling model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        $last = $this->findModel($id);     

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Обновить вызов #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->setChanging($last, $model);
                return [
                    'forceReload'=>'#base-pjax',
                    'forceClose'=>true,
                    'title'=> "Обзвон #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['обновить','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Обновить вызов #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionDateChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        $last = $this->findModel($id);     

        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->save()){
                $model->setChanging($last, $model);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'small',
                    'content'=>$this->renderAjax('update_date', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionStatusChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        $last = $this->findModel($id);     

        if($request->isAjax){            
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->save()){
                $model->setChanging($last, $model);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'small',
                    'content'=>$this->renderAjax('update_status', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    public function actionAddClient($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);  
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if($request->isAjax){            

            $client = Clients::find()->where(['fio' => $model->fio, 'status' => $model->status])->one();

            if($client == null){
                $client = new Clients();
                $client->date_cr = date('Y-m-d');
                $client->fio = $model->fio;
                $client->city = $model->city;
                $client->telephone = $model->telephone;
                $client->email = $model->email;
                $client->status = $model->status;
                $client->planned_date = $model->planned_date;
                $client->save();
            
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    /*'forceClose'=>true,*/
                    'title'=> "Обзвон",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('client', [
                        'id' => 1,
                    ]),
                ];    
            }else{
                 return [
                    'title'=> "Ошибка",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('client', [
                        'id' => 2,
                    ]),
                ];        
            }
        }
    }

    /**
     * Delete an existing Calling model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Calling model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Calling model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calling the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calling::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
