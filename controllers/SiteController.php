<?php

namespace app\controllers;

use app\models\AccountingReport;
use app\models\Logs;
use app\models\RegisterForm;
use app\models\ResetPasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Project;
use app\models\Board;
use app\models\Tasks;
use app\models\Users;
use app\models\Clients;
use app\models\UsersStatuses;
use app\models\UsersProjects;
use app\models\UsersLidStatuses;
use app\models\StatusLid;
use app\models\TaskStatuses;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $wrapperClass;

    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
                'only' => ['dashboard', 'index', 'instruksion','policy'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','dashboard'],
                'rules' => [
                    [
                        'actions' => ['dashboard', 'save-token', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('site/boards-project');
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Регистрирует нового пользователя
     * @return string|Response
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $log = new Logs([
            'user_id' => null,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_OPEN_REGISTRATION_PAGE,
        ]);
        $log->description = $log->generateEventDescription();
        $log->save();

        $this->layout = 'main-sign';

        $model = new RegisterForm();
        $ref = null;
        if (isset($_GET['ref'])) $ref = $_GET['ref'];
        if($model->load(Yii::$app->request->post()) && $model->register($ref))
        {
            Yii::$app->session->setFlash('register_success', 'Регистрация прошла успешно. Пожалуйста, авторизируйтесь');
            $login_model = new LoginForm();
            $login_model->username = $model->login;
            $login_model->password = $model->password;
            if ($login_model->login()) {
                return $this->goBack();
            }
            else return $this->redirect(['login']);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new ResetPasswordForm();

        if($model->load(Yii::$app->request->post()) && $model->reset())
        {
            Yii::$app->session->setFlash('register_success', 'На вашу почту был выслан временный пароль. Воспользуйтесь им для авторизации');
            return $this->redirect(['login']);
        } else {
            return $this->render('reset', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Рабочий стол
     * @return Response|string
     */
// через этот екшн заходим на Рабочий стол
    public function actionDashboard()
    {
      if (Yii::$app->user->isGuest) {
        $this->redirect(['login']);
      }
      $company_id = Yii::$app->user->identity->company_id;
      $user_permission = Yii::$app->user->identity->type;
      $projects = UsersProjects::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
      $project = [];
      foreach ($projects as $value) {
          $project [] = $value->project_id;
      }

      if($user_permission == 1) $clients = Clients::find()->all();
      else $clients = Clients::find()->where(['project_id' => $project])->all();
      $clients_id = [];
      foreach ($clients as $value) {
          $clients_id [] = $value->id;
      }

      $now = date('Y-m-d');
      $day = \Yii::$app->formatter->asDate($now, 'php:d');
      $month = \Yii::$app->formatter->asDate($now, 'php:m');
      $year = \Yii::$app->formatter->asDate($now, 'php:Y');
      $week = date('Y-m-d', mktime(0, 0, 0, $month, $day - 7,  $year));

      if($user_permission == 1) $tasksStatuses = TaskStatuses::find()->all();
      else {
        $statuses = UsersStatuses::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
        $result = [];
        foreach ($statuses as $value) {
          $result [] = $value->status_id;
        }
        $tasksStatuses = TaskStatuses::find()->where(['id' => $result])->all();
      }

      $allStatuses = [];
      if(Yii::$app->user->identity->type == 1) $count = Tasks::find()->where(['data' => date('Y-m-d')])->count();
      else $count = Tasks::find()->where([ 'data' => date('Y-m-d'), 'manager' => Yii::$app->user->identity->id, ])->count();
        
      $allStatuses [] = [
        'key' => 'Сегодня',
        'value' => $count,
      ];
      foreach ($tasksStatuses as $value) {
        if(Yii::$app->user->identity->type == 1) $count = Tasks::find()->where(['status' => $value->id, 'data' => date('Y-m-d')])->count();
        else $count = Tasks::find()->where(['status' => $value->id, 'data' => date('Y-m-d'), 'manager' => Yii::$app->user->identity->id, ])->count();
        $allStatuses [] = [
          'key' => $value->name,
          'value' => $count,
        ];
      }

      ///--------------Клиенты --------------//
      if($user_permission == 1) 
      {
        $all_client = Clients::find()->count();
        $month_client = Clients::find()->where(['between', 'date_cr', $year . '-' . $month . '-01', $now])->count();
        $clients = Clients::find()->all();
        $week_client = Clients::find()->where(['between', 'date_cr', $week, $now])->count();
        $day_client = Clients::find()->where(['date_cr' => $now])->count();
      }
      else {
        $all_client = Clients::find()->where(['project_id' => $project])->count();
        $month_client = Clients::find()->where(['between', 'date_cr', $year . '-' . $month . '-01' , $now])->andWhere(['project_id' => $project])->count();
        $week_client = Clients::find()->where(['between', 'date_cr', $week, $now])->andWhere(['project_id' => $project])->count();
        $clients = Clients::find()->where(['project_id' => $project])->all();
        $day_client = Clients::find()->where(['date_cr' => $now])->andWhere(['project_id' => $project])->count();
      }

      $clients = [
        'all' => $all_client,
        'month' => $month_client,
        'week' => $week_client,
        'day' => $day_client,
      ];
      ////---------------------------------//
      
      ///--------------Оплаты --------------//
      if(!$company_id)
      {
        $all_pay = AccountingReport::find()->sum('amount');
        $month_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])->sum('amount');
        $week_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])->sum('amount');
        $day_pay = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])->sum('amount');
      }
      else 
      {
        $all_pay = AccountingReport::find()->andWhere(['company_id' => $company_id])->sum('amount');
        $month_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])
            ->andWhere(['company_id' => $company_id])->sum('amount');
        $week_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])
            ->andWhere(['company_id' => $company_id])->sum('amount');
        $day_pay = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])
            ->andWhere(['company_id' => $company_id])->sum('amount');
      }

      $pays = [
        'all' => (int)$all_pay,
        'month' => (int)$month_pay,
        'week' => (int)$week_pay,
        'day' => (int)$day_pay,
      ];
      ////---------------------------------//      

      if(Yii::$app->user->identity->id == 1) $tasks = Tasks::find()->where(['data' => date('Y-m-d')])->all();
      else {
        if(Yii::$app->user->identity->type == 1) $tasks = Tasks::find()->where(['company_id' => Yii::$app->user->identity->company_id,  'data' => date('Y-m-d')])->all();
        else $tasks = Tasks::find()->where(['manager' => Yii::$app->user->identity->id,  'data' => date('Y-m-d')])->all();
      }

      $statuses = UsersLidStatuses::find()->where(['user_id' => Yii::$app->user->identity->id ])->all();
      $status = [];
      foreach ($statuses as $value) {
          $status [] = $value->status_lid_id;
      }


      if(Yii::$app->user->identity->type == 1){
        $projects = Project::find()->all();
        $statuses = StatusLid::find()->orderBy([ 'number' => SORT_ASC ])->all();
      }
      else{
          $projects = Project::find()->where(['id' => $project])->all();
          $statuses = StatusLid::find()->where(['id' => $status])->orderBy([ 'number' => SORT_ASC ])->all();
      }
    if (Yii::$app->user->identity->isSuperAdmin()) {
        return $this->render('dashboard_admin', [
            'pays' => $pays,
            'clients' => $clients,
            'tasks' => $tasks,
            'projects' => $projects,
            'statuses' => $statuses,
            'allStatuses' => $allStatuses,
        ]);
    }
      return $this->render('dashboard', [  
          'pays' => $pays,
          'clients' => $clients,
          'tasks' => $tasks,
          'projects' => $projects,
          'statuses' => $statuses,
          'allStatuses' => $allStatuses,
      ]);
    }

    /**
     * Сохраняет FCM токен авторизованному пользователю
     * @return mixed
     */
    public function actionSaveToken()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $post = Yii::$app->request->post();

        if(isset($post['token'])){
          $user->fcm_token = $post['token'];
          $user->save();
        }

        return $user->getFirstErrors();
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
         Yii::$app->user->logout();

        $this->redirect(['login']);
    }

    public function actionLogout1()
    {
         Yii::$app->user->logout();

        $this->redirect(['signup']);
    }
    public function actionAvtorizatsiya()
    {
      if(isset(Yii::$app->user->identity->id))
      {
        return $this->render('error');
      }        
       else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }

    }


    /**
     * Делать пуш в формате broadcast
     * То есть отправляет пуш всем пользователям авторизованных в системе
     * @return mixed
     */
    public function actionPush()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $users = Users::find()->hasToken()->all();
        $result = [];

        $url = 'https://fcm.googleapis.com/fcm/send';

        $YOUR_API_KEY = 'AAAAgRr2kLc:APA91bEzgweVGyzAtG7lRtQhAhiTc2sVMSCXcxjBlH7D5X4FR6OzS_0AejAb-AerriEl1xPkw5SIzzl5fTmgEtc3jRxbRG1OSoWXfz1T-GIKU3KYugpI4uQww4DSmEpJth7N0bE3rPL1'; // Server key

        foreach($users as $user)
        {
          $YOUR_TOKEN_ID = $user->fcm_token;

          $request_body = [
              'to' => $YOUR_TOKEN_ID,
              'notification' => [
                  'title' => 'Привет от Артема',
                  'body' => "Привет привет",
                  'icon' => 'https://eralash.ru.rsz.io/sites/all/themes/eralash_v5/logo.png?width=192&height=192',
                  'click_action' => 'http://eralash.ru/',
              ],
          ];
          $fields = json_encode($request_body);

          $request_headers = [
              'Content-Type: application/json',
              'Authorization: key=' . $YOUR_API_KEY,
          ];

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
          curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          $response = curl_exec($ch);
          $result[] = $response;
          curl_close($ch);
        }

        

        return $result;
    }


// через этот екшн можно войти на инструкцию
    public function actionInstruksion()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        return $this->render('instruksion', []);        
    }

    public function actionPolicy()
    {
        $this->layout = 'main-sign';
        return $this->render('policy', []);
    }

    public function actionSend()
    {
      $url = 'https://fcm.googleapis.com/fcm/send';
      $YOUR_API_KEY = 'AAAAgRr2kLc:APA91bEzgweVGyzAtG7lRtQhAhiTc2sVMSCXcxjBlH7D5X4FR6OzS_0AejAb-AerriEl1xPkw5SIzzl5fTmgEtc3jRxbRG1OSoWXfz1T-GIKU3KYugpI4uQww4DSmEpJth7N0bE3rPL1'; // Server key
      $YOUR_TOKEN_ID = 'db_gxTlYVNc:APA91bFfPn6TI1_lMkmdYKQq5EAN1bFbDYvHucAbZzdr_VnoYK4x1EMdhlMoYZt3sHM-YMK0uaXA1AqY6YP0_lcvnbZPDJ4yKuObwzAZ7QvJzYXindt7q0WsnD8JgLFvCAYzf89diDYD'; 

      $request_body = [
          'to' => $YOUR_TOKEN_ID,
          'notification' => [
              'title' => 'Привет от Артема',
              'body' => "Привет dgdf привет",
              'icon' => 'https://cdn1.iconfinder.com/data/icons/nuvola2/48x48/actions/mail_get.png',
              'click_action' => 'http://eralash.ru/',
          ],
      ];
      $fields = json_encode($request_body);

      $request_headers = [
          'Content-Type: application/json',
          'Authorization: key=' . $YOUR_API_KEY,
      ];

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      $response = curl_exec($ch);
      curl_close($ch);

      echo $response;
    }

    public function actionTest()
    {
        return $this->render('test', [
            
        ]);
    }

}
