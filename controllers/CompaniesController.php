<?php

namespace app\controllers;

use app\models\AccountingReport;
use app\models\Clients;
use app\models\Pays;
use app\models\Project;
use app\models\StatusLid;
use app\models\Tasks;
use app\models\TaskStatuses;
use app\models\Users;
use app\models\UsersLidStatuses;
use app\models\UsersProjects;
use app\models\UsersStatuses;
use Yii;
use app\models\Companies;
use app\models\CompaniesSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * CompaniesController implements the CRUD actions for Companies model.
 */
class CompaniesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (!Yii::$app->user->identity->isSuperAdmin()) {
                                return $this->redirect(['/']);
                            }
                            return true;

                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Companies models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CompaniesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Почтовое уведомлении о завершении действия лицензии
     * @return mixed
     */
    public function actionEndAccessEmailNotify()
    {
        $date = date('Y-m-d H:i:s');
        /** @var \app\models\Companies[] $companies */
        $companies = Companies::find()->with('admin')->where(['<', 'access_end_datetime', $date])->all();
        foreach ($companies as $company)
        {
            try {

            } catch (\Exception $e)
            {}
            $company->admin->login;
        }

        Yii::$app->session->setFlash('success', 'Уведомления отправлены');
        return $this->redirect(['index']);
    }

    /**
     * Displays a single Companies model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $user_permission = Yii::$app->user->identity->type;
        $company_id = $id;




        $now = date('Y-m-d');
        $day = \Yii::$app->formatter->asDate($now, 'php:d');
        $month = \Yii::$app->formatter->asDate($now, 'php:m');
        $year = \Yii::$app->formatter->asDate($now, 'php:Y');
        $week = date('Y-m-d', mktime(0, 0, 0, $month, $day - 7,  $year));

        $tasksStatuses = TaskStatuses::find()->andWhere(['company_id' => $company_id])->all();


        $allStatuses = [];

        $count = Tasks::find()->where(['data' => date('Y-m-d')])->andWhere(['company_id' => $company_id])->count();

        $allStatuses [] = [
            'key' => 'Сегодня',
            'value' => $count,
        ];
        foreach ($tasksStatuses as $value) {
            $count = Tasks::find()->where(['status' => $value->id, 'data' => date('Y-m-d')])
                ->andWhere(['company_id' => $company_id])->count();
            $allStatuses [] = [
                'key' => $value->name,
                'value' => $count,
            ];
        }

        ///--------------Клиенты --------------//
            $all_client = Clients::find()->andWhere(['company_id' => $company_id])->count();
            $month_client = Clients::find()->where(['between', 'date_cr', $year . '-' . $month . '-01', $now])
                ->andWhere(['company_id' => $company_id])->count();
            $week_client = Clients::find()->where(['between', 'date_cr', $week, $now])
                ->andWhere(['company_id' => $company_id])->count();
            $day_client = Clients::find()->where(['date_cr' => $now])
                ->andWhere(['company_id' => $company_id])->count();


        $clients = [
            'all' => $all_client,
            'month' => $month_client,
            'week' => $week_client,
            'day' => $day_client,
        ];
        ////---------------------------------//

        ///--------------Оплаты --------------//
        $all_pay = AccountingReport::find()->andWhere(['company_id' => $company_id])->sum('amount');
        $month_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])
            ->andWhere(['company_id' => $company_id])->sum('amount');
        $week_pay = AccountingReport::find()->where(['between', 'date_report', $year . '-' . $month . '-01' , date('Y-m-d H:i:s')])
            ->andWhere(['company_id' => $company_id])->sum('amount');
        $day_pay = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59')])
            ->andWhere(['company_id' => $company_id])->sum('amount');

        $pays = [
            'all' => $all_pay,
            'month' => $month_pay,
            'week' => $week_pay,
            'day' => $day_pay,
        ];
        ////---------------------------------//

        $tasks = Tasks::find()->where(['company_id' => $company_id,  'data' => date('Y-m-d')])->all();
        $projects = Project::find()->andWhere(['company_id' => $company_id])->all();
        $statuses = StatusLid::find()->andWhere(['company_id' => $company_id])->orderBy([ 'number' => SORT_ASC ])->all();



            return $this->render('view', [
                'model' => $this->findModel($id),
                'pays' => $pays,
                'clients' => $clients,
                'tasks' => $tasks,
                'projects' => $projects,
                'statuses' => $statuses,
                'allStatuses' => $allStatuses,
            ]);
    }

    /**
     * Creates a new Companies model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Companies();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить компании",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить компании",
                    'content'=>'<span class="text-success">Создание компании успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить компании",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Companies model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить компании #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "компания #".$id,
                    'content'=>$this->renderAjax('view_short', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить компании #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionTimeZone($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
	        Yii::$app->response->format = Response::FORMAT_JSON;
	        if($model->load($request->post()) && $model->save())
	        {
	            return ['forceClose'=>true];
	        }
	        else
	        {
	             return [
	                'title'=> "Изменить часовой пояс ",
	                'size' => 'normal',
	                'content'=>$this->renderAjax('time_zone', [
	                    'model' => $model,
	                ]),
	                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
	                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
	            ];        
	        }
	    }
	    else{
	    	if($model->load($request->post()) && $model->save())
	        {
	            return $this->redirect(['/site/dashboard']);
	        }
	        else{
	        	return $this->render('time_zone', [
                    'model' => $model,
                ]);
	        }
	    }
    }

    /**
     * Delete an existing Companies model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Companies model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Companies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
