<?php

namespace app\controllers;

use app\models\AccountingReport;
use app\models\AffiliateAccounting;
use app\models\accounting\AffiliateAccountingSearch;
use app\models\Companies;
use app\models\companies\CompaniesSearch;
use app\models\CompanySettings;
use app\models\EmailTemplates;
use app\models\PaymentOrders;
use app\models\PaymentOrdersSearch;
use Yii,
    yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\web\Response;
use yii\data\ActiveDataProvider;
use app\models\Users;
use app\models\Settings;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class AffiliateProgramController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $company_id = Yii::$app->user->identity->company_id;
        $referalsProvider = new ActiveDataProvider([
            'query' => Yii::$app->user->identity->type == 1
                ? Companies::find()->where(['referal_id' => $company_id])
                : Companies::find()->where(['not', ['referal_id' => null]])->orderBy(['referal_id' => SORT_DESC]),
        ]);
        $referalsProvider->sort->defaultOrder = ['id' => SORT_DESC];
        
        $retentionProvider = new ActiveDataProvider([
            'query' => Yii::$app->user->identity->type == 1
                ? AffiliateAccounting::find()->where(['company_id' => $company_id])->orderBy(['date_transaction' => SORT_DESC])
                : AffiliateAccounting::find()->orderBy(['date_transaction' => SORT_DESC]),
        ]);
        $retentionProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $refPaymentsProvider = new ActiveDataProvider([
            'query' => Yii::$app->user->identity->type == 1
                ? PaymentOrders::find()->where(['company_id' => $company_id])->orderBy(['date_order' => SORT_DESC])
                : PaymentOrders::find()->orderBy(['date_order' => SORT_DESC]),
        ]);
        $refPaymentsProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $refPaymentsSearchModel = new PaymentOrdersSearch;
        $setting = Settings::find()->where(['key' => 'partner_programm'])->one();


        /*$companyId = FunctionHelper::getCompanyId();
        $settings = CompanySettings::findOne(['company_id' => $companyId]);
        $company = Companies::findOne($companyId);
        $description = EmailTemplates::findOne(['key' => 'affiliate_program']);
        $retentionSearchModel = new AffiliateAccountingSearch;
        */

        return $this->render('index', [
            'referalsProvider' => $referalsProvider,
            'retentionProvider' => $retentionProvider,
            'refPaymentsProvider' => $refPaymentsProvider,
            'refPaymentsSearchModel' => $refPaymentsSearchModel,
            'description' => $setting->value,
            /*'company' => $company,
            'settings' => $settings,
            'referalsSearchModel' => $referalsSearchModel,
            'retentionSearchModel' => $retentionSearchModel,
            */

        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionAddOrder()
    {

        $model = new PaymentOrders(['scenario' => PaymentOrders::SCENARIO_SEND_ORDER]);

        $request = Yii::$app->request;
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $model->company_id = FunctionHelper::getCompanyId();
        }


        if ($request->isPost) {
//            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect('index');
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Новая заявка",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
            return $this->render('accounting/_add-order', ['model' => $model]);
            return [
                'title' => "Новая заявка",
                'size' => 'large',
                'content' => $this->renderAjax('accounting/_add-order', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else {
            return $this->render('accounting/_add-order', ['model' => $model]);
        }
    }

    public function actionDeleteOrder($id)
    {
        $request = Yii::$app->request;

        $order = $this->findOrder($id);
        if ($order->status != PaymentOrders::STATUS_SUCCESS) {
            $order->delete();
        }


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionExchange()
    {
        $request = Yii::$app->request;

//        Yii::warning($request->isAjax, __METHOD__);

//        if ($request->isPost) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            $order = $this->findOrder($request->get('id'));
            $company = Users::findOne($order->company_id);


            if ($company->partner_balance >= $order->amount) {

                Yii::warning($company->partner_balance);

                $company->partner_balance -= $order->amount;

                $order->status = PaymentOrders::STATUS_SUCCESS;
                $order->date_payment = date('Y-m-d H:i:s');
                $order->save();

                if ($order->type == PaymentOrders::TYPE_BALLANCE) {
                    $company->general_balance += $order->amount;
                    $report = new AccountingReport([
                        'company_id' => $order->company_id,
                        'operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE,
                        'amount' => $order->amount,
                        'description' => 'Перевод партнерских отчислений на основной счет',
                    ]);
                    $report->save();
                }
                $company->save();
                $res = ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            }
//            $res = ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            return  $this->redirect('index');
//        }

    }

    protected function findOrder($id)
    {
        if (($model = PaymentOrders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
