<?php

namespace app\controllers;

use app\models\SettingsAdm;
use yii\helpers\Html;
use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use app\models\accounting\AccountingReportSearch;
use app\models\Users;
use app\models\CompanySettings;
//use Faker\Provider\Company;
use app\models\HandBalance;
use app\models\Statuses;
use app\models\statuses\StatusesSearch;
use app\models\YandexLog;
use app\models\YandexMoney;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;
use yii\web\Response;

//use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class PaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу "Операции с основным счетом"
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $searchModel = new AccountingReportSearch;
        $dataProviderDebit = $searchModel->searchDebit(Yii::$app->request->queryParams);
        $dataProviderDebit->sort->defaultOrder = ['id' => SORT_DESC];
        $dataProviderCredit = $searchModel->searchCredit(Yii::$app->request->queryParams);
        $dataProviderCredit->sort->defaultOrder = ['id' => SORT_DESC];
        $dataProviderShop = $searchModel->searchShop(Yii::$app->request->queryParams);
        $dataProviderShop->sort->defaultOrder = ['id' => SORT_DESC];

        $configure = Settings::find()->where(['key' => 'cash_number'])->one();
        $company = Users::findOne(Yii::$app->user->identity->id);
        if (Yii::$app->request->post() && $configure->load(Yii::$app->request->post())) {
            $configure->save();
        }

        return $this->render('index', [
            'configure' => $configure,
            'company' => $company,
            'dataProviderDebit' => $dataProviderDebit,
            'dataProviderCredit' => $dataProviderCredit,
            'dataProviderShop' => $dataProviderShop,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @return string
     */
    public function actionAddBalance()
    {
        $request = Yii::$app->request;
        $model = new HandBalance;
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($request->isGet) {
                return [
                    'title' => "Пополнить счет компании",
                    'size' => 'large',
                    'content' => $this->renderAjax('_add-balance', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            } else if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'forceReload' => '#crud-datatable-1-pjax',
                        'title' => "Пользователи",
                        'size' => 'normal',
                        'content' => '<span class="text-success">Успешно выполнено</span>',
                        'footer' => Html::button('OK', [
                            'class' => 'btn btn-primary pull-left',
                            'data-dismiss' => "modal",
                        ])];
                }
            }
            $this->layout = 'modal';
            return $this->render('_add-balance', [
                'model' => $model,
            ]);
        }
    }

}