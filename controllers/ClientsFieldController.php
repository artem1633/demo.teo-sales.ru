<?php

namespace app\controllers;

use Yii;
use app\models\ClientsField;
use app\models\ClientsFieldSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Fields;
use app\models\Clients;
use yii\base\DynamicModel;
/**
 * ClientsFieldController implements the CRUD actions for ClientsField model.
 */
class ClientsFieldController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientsField models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ClientsFieldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single ClientsField model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ClientsField #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }*/

    /**
     * Creates a new ClientsField model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ClientsField();  

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new ClientsField",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new ClientsField",
                    'content'=>'<span class="text-success">Create ClientsField success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new ClientsField",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }*/

    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $client = Clients::findOne($client_id);
        $fields = Fields::find()->where(['project_id' => $client->project_id])->all();

        $fieldsNames = [];
        foreach ($fields as $field)
        {
            $fieldsNames[] = $field->name;
        }
        $fieldsModel = new DynamicModel($fieldsNames);

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                $post = $request->post();
                $fields = $request->post()['DynamicModel'];
                foreach ($fields as $key => $value) 
                {
                    $field = Fields::find()->where([ 'project_id' => $client->project_id, 'name' => $key ])->one();
                    $client_field = ClientsField::find()->where([ 'client_id' => $client_id, 'field_id' => $field->id ])->one();
                    if($client_field == null)
                    {
                        $client_field = new ClientsField();
                        $client_field->client_id = $client_id;
                        $client_field->field_id = $field->id;
                        $client_field->value = $value;
                        $client_field->save();
                    }
                    else 
                    {
                        $client_field->value = $value;
                        $client_field->save();
                    }                    
                }
                return [
                    'forceReload'=>'#three-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Добавить поля",
                    'content'=>$this->renderAjax('create', [
                        'fields' => $fields,
                        'fieldsModel' => $fieldsModel,
                        'project_id' => $client->project_id,
                        'client_id' => $client_id,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            
            if ($request->post()) {
                $post = $request->post();
                $fields = $request->post()['DynamicModel'];
                /*echo "<pre>";
                print_r($post);
                echo "</pre>";

                echo "<pre>";
                print_r($fields);
                echo "</pre>";
                die;*/

                foreach ($fields as $key => $value) 
                {
                    $field = Fields::find()->where([ 'project_id' => $client->project_id, 'name' => $key ])->one();
                    $client_field = ClientsField::find()->where([ 'client_id' => $client_id, 'field_id' => $field->id ])->one();
                    if($client_field == null)
                    {
                        $client_field = new ClientsField();
                        $client_field->client_id = $client_id;
                        $client_field->field_id = $field->id;
                        $client_field->value = $value;
                        $client_field->save();
                    }
                    else 
                    {
                        $client_field->value = $value;
                        $client_field->save();
                    }                    
                }
                return $this->redirect(['/clients/view', 'id' => $client_id]);
            } else {
                return $this->render('create', [
                    'fields' => $fields,
                    'fieldsModel' => $fieldsModel,
                    'project_id' => $client->project_id,
                    'client_id' => $client_id,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing ClientsField model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update ClientsField #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "ClientsField #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update ClientsField #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }*/

    /**
     * Delete an existing ClientsField model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{

            return $this->redirect(['index']);
        }
    }*/

     /**
     * Delete multiple existing ClientsField model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }       
    }*/

    /**
     * Finds the ClientsField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientsField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientsField::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
