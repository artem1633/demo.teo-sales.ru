<?php

use yii\db\Migration;

/**
 * Class m180411_114641_add_company_id_to_clients_table
 */
class m180411_114641_add_company_id_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-clients-created_by',
            'clients'
        );

        $this->dropIndex(
            'idx-clients-created_by',
            'clients'
        );

        $this->dropColumn('clients', 'created_by');

        $this->addCompanyIdField('clients');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('clients');

        $this->addColumn('clients', 'created_by', $this->integer()->comment('Пользователь, который создал клиента'));

        $this->createIndex(
            'idx-clients-created_by',
            'clients',
            'created_by'
        );

        $this->addForeignKey(
            'fk-clients-created_by',
            'clients',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "SET NULL"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
