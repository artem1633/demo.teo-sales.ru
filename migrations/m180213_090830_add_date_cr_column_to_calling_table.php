<?php

use yii\db\Migration;

/**
 * Handles adding date_cr to table `calling`.
 */
class m180213_090830_add_date_cr_column_to_calling_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('calling', 'date_cr', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('calling', 'date_cr');
    }
}
