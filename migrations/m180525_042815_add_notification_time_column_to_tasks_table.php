<?php

use yii\db\Migration;

/**
 * Handles adding notification_time to table `tasks`.
 */
class m180525_042815_add_notification_time_column_to_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'notification_time', $this->time());
        $this->addColumn('tasks', 'notification_status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'notification_time');
        $this->dropColumn('tasks', 'notification_status');
    }
}
