<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `parent_company_id_column_to_users`.
 */
class m180410_142045_drop_parent_company_id_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-users-parent_company_id',
            'users'
        );

        $this->dropIndex(
            'idx-users-parent_company_id',
            'users'
        );

        $this->dropColumn('users', 'parent_company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('users', 'parent_company_id', $this->integer()->comment('Компания, к которой пренадлежит пользователь'));

        $this->createIndex(
            'idx-users-parent_company_id',
            'users',
            'parent_company_id'
        );

        $this->addForeignKey(
            'fk-users-parent_company_id',
            'users',
            'parent_company_id',
            'users',
            'id',
            'SET NULL'
        );
    }
}
