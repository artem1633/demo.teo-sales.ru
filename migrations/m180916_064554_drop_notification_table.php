<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `notification`.
 */
class m180916_064554_drop_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('notification');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'site_id' => $this->integer(),
            'status' => $this->integer(),
        ]);
    }
}
