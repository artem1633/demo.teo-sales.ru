<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `pays`.
 */
class m180213_081654_add_user_id_column_to_pays_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pays', 'user_id', $this->integer());

        $this->createIndex('idx-pays-user_id', 'pays', 'user_id', false);
        $this->addForeignKey("fk-pays-user_id", "pays", "user_id", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-pays-user_id','pays');
        $this->dropIndex('idx-pays-user_id','pays');

        $this->dropColumn('pays', 'user_id');
    }
}
