<?php

use yii\db\Migration;

/**
 * Handles adding created_by to table `clients`.
 */
class m180324_063252_add_created_by_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'created_by', $this->integer()->comment('Пользователь, который создал клиента'));

        $this->createIndex(
            'idx-clients-created_by',
            'clients',
            'created_by'
        );

        $this->addForeignKey(
            'fk-clients-created_by',
            'clients',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-clients-created_by',
            'clients'
        );

        $this->dropIndex(
            'idx-clients-created_by',
            'clients'
        );

        $this->dropColumn('clients', 'created_by');
    }
}
