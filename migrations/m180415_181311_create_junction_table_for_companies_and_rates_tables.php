<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_rates`.
 * Has foreign keys to the tables:
 *
 * - `companies`
 * - `rates`
 */
class m180415_181311_create_junction_table_for_companies_and_rates_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies_rates', [
            'companies_id' => $this->integer(),
            'rates_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(companies_id, rates_id)',
        ]);

        // creates index for column `companies_id`
        $this->createIndex(
            'idx-companies_rates-companies_id',
            'companies_rates',
            'companies_id'
        );

        // add foreign key for table `companies`
        $this->addForeignKey(
            'fk-companies_rates-companies_id',
            'companies_rates',
            'companies_id',
            'companies',
            'id',
            'CASCADE'
        );

        // creates index for column `rates_id`
        $this->createIndex(
            'idx-companies_rates-rates_id',
            'companies_rates',
            'rates_id'
        );

        // add foreign key for table `rates`
        $this->addForeignKey(
            'fk-companies_rates-rates_id',
            'companies_rates',
            'rates_id',
            'rates',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `companies`
        $this->dropForeignKey(
            'fk-companies_rates-companies_id',
            'companies_rates'
        );

        // drops index for column `companies_id`
        $this->dropIndex(
            'idx-companies_rates-companies_id',
            'companies_rates'
        );

        // drops foreign key for table `rates`
        $this->dropForeignKey(
            'fk-companies_rates-rates_id',
            'companies_rates'
        );

        // drops index for column `rates_id`
        $this->dropIndex(
            'idx-companies_rates-rates_id',
            'companies_rates'
        );

        $this->dropTable('companies_rates');
    }
}
