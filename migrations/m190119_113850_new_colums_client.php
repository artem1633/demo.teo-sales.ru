<?php

use yii\db\Migration;

/**
 * Class m190119_113850_new_colums_client
 */
class m190119_113850_new_colums_client extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'id_send', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('clients', 'id_send');
    }
}
