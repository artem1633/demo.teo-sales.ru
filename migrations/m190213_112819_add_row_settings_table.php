<?php

use yii\db\Migration;

/**
 * Class m190213_112819_add_row_settings_table
 */
class m190213_112819_add_row_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'label' => 'Балы на счет при регистрации',
            'key' => 'bonus_register',
            'value' =>'500',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings',['key' => 'bonus_register']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190213_112819_add_row_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
