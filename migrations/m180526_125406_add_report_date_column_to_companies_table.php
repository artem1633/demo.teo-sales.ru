<?php

use yii\db\Migration;

/**
 * Handles adding report_date to table `companies`.
 */
class m180526_125406_add_report_date_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'report_date', $this->date()->defaultValue( date('Y-m-d') ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'report_date');
    }
}
