<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies`.
 */
class m180410_141501_create_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'admin_id' => $this->integer()->comment('Id администратора компании'),
        ]);
        $this->addCommentOnTable('companies', 'Компании');

        $this->createIndex(
            'idx-companies-admin_id',
            'companies',
            'admin_id'
        );

        $this->addForeignKey(
            'fk-companies-admin_id',
            'companies',
            'admin_id',
            'users',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-companies-admin_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-admin_id',
            'companies'
        );

        $this->dropTable('companies');
    }
}
