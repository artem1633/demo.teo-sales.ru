<?php

use yii\db\Migration;

/**
 * Handles adding report_time to table `companies`.
 */
class m180526_093751_add_report_time_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'report_time', $this->time()->defaultValue('09:00'));

        $report_time = \app\models\Settings::find()->where(['key' => 'report_time'])->one();
        $report_time->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'report_time');
    }
}
