<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180512_061020_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-project-company_id', 'project', 'company_id', false);
        $this->addForeignKey("fk-project-company_id", "project", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-project-company_id','project');
        $this->dropIndex('idx-project-company_id','project');  

        $this->dropTable('project');
    }
}
