<?php

use yii\db\Migration;

/**
 * Handles adding position to table `users`.
 */
class m180411_042948_add_position_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'position', $this->string()->comment('Должность'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users','position');
    }
}
