<?php

use yii\db\Migration;

/**
 * Handles adding utm to table `clients`.
 */
class m180516_100400_add_utm_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'utm', $this->string(255));
        $this->addColumn('clients', 'link', $this->string(255));
        $this->addColumn('clients', 'ip', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('clients', 'utm');
        $this->dropColumn('clients', 'link');
        $this->dropColumn('clients', 'ip');
    }
}
