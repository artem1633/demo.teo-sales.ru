<?php

use yii\db\Migration;

/**
 * Handles the creation of table `changing`.
 */
class m180211_160430_create_changing_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('changing', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255),
            'line_id' => $this->integer(),
            'date_time' => $this->datetime(),
            'old_value' => $this->string(1000),
            'new_value' => $this->string(1000),
            'user_id' => $this->integer(),
            'field' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('changing');
    }
}
