<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `templates`.
 */
class m180801_063543_add_project_id_column_to_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('templates', 'project_id', $this->integer());

        $this->createIndex('idx-templates-project_id', 'templates', 'project_id', false);
        $this->addForeignKey("fk-templates-project_id", "templates", "project_id", "project", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-templates-project_id','templates');
        $this->dropIndex('idx-templates-project_id','templates');
        
        $this->dropColumn('templates', 'project_id');
    }
}
