<?php

use yii\db\Migration;

/**
 * Class m190216_161428_alter_slider_table
 */
class m190216_161428_alter_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sliders','text',$this->binary());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('sliders','text',$this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190216_161428_alter_slider_table cannot be reverted.\n";

        return false;
    }
    */
}
