<?php

use yii\db\Migration;

/**
 * Class m180522_104426_changing_email_value
 */
class m180522_104426_changing_email_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $loginAddress = 'http://demo.teo-sales.ru/site/login';
        $this->update('email_templates', 
            array(
                'body' => 'Поздравляем вас с успешной регистрацией.<br>Можете авторизоваться по этой ссылке: <a href="'.$loginAddress.'">'.$loginAddress.'</a>',
            ), 
            'id=1'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
