<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_projects`.
 */
class m180527_073232_create_users_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_projects', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'user_id' => $this->integer(),
        ]);

        $this->createIndex('idx-users_projects-project_id', 'users_projects', 'project_id', false);
        $this->addForeignKey("fk-users_projects-project_id", "users_projects", "project_id", "project", "id");

        $this->createIndex('idx-users_projects-user_id', 'users_projects', 'user_id', false);
        $this->addForeignKey("fk-users_projects-user_id", "users_projects", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_projects-project_id','users_projects');
        $this->dropIndex('idx-users_projects-project_id','users_projects');  

        $this->dropForeignKey('fk-users_projects-user_id','users_projects');
        $this->dropIndex('idx-users_projects-user_id','users_projects');  

        $this->dropTable('users_projects');
    }
}
