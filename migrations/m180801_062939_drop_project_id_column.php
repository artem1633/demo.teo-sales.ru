<?php

use yii\db\Migration;

/**
 * Class m180801_062939_drop_project_id_column
 */
class m180801_062939_drop_project_id_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-documentation-project_id','documentation');
        $this->dropIndex('idx-documentation-project_id','documentation');

        $this->dropColumn('documentation', 'project_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('documentation', 'project_id', $this->integer());

        $this->createIndex('idx-documentation-project_id', 'documentation', 'project_id', false);
        $this->addForeignKey("fk-documentation-project_id", "documentation", "project_id", "project", "id");
    }
}
