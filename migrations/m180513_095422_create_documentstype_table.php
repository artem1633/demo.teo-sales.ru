<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documentstype`.
 */
class m180513_095422_create_documentstype_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documentstype', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-documentstype-company_id', 'documentstype', 'company_id', false);
        $this->addForeignKey("fk-documentstype-company_id", "documentstype", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-documentstype-company_id','documentstype');
        $this->dropIndex('idx-documentstype-company_id','documentstype');  
        
        $this->dropTable('documents_type');
    }
}
