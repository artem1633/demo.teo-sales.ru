<?php

use yii\db\Migration;

/**
 * Handles the creation of table `source`.
 */
class m180211_154517_create_source_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('source', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('source');
    }
}
