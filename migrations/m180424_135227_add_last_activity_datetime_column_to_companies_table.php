<?php

use yii\db\Migration;

/**
 * Handles adding last_activity_datetime to table `companies`.
 */
class m180424_135227_add_last_activity_datetime_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'last_activity_datetime');
    }
}
