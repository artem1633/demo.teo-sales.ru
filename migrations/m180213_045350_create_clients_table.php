<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m180213_045350_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'date_cr' => $this->date(),
            'fio' => $this->string(255)->notNull(),
            'city' => $this->string(255),
            'telephone' => $this->string(255),
            'email' => $this->string(255),
            'menejer' => $this->integer(),
            'comment' => $this->text(),
            'client' => $this->boolean(),
            'source_id' => $this->integer(),
            'status' => $this->integer(),
            'access' => $this->text(),
            'planned_date' => $this->date(),
        ]);

        $this->createIndex('idx-clients-menejer', 'clients', 'menejer', false);
        $this->addForeignKey("fk-clients-menejer", "clients", "menejer", "users", "id");

        $this->createIndex('idx-clients-source_id', 'clients', 'source_id', false);
        $this->addForeignKey("fk-clients-source_id", "clients", "source_id", "source", "id");

        $this->createIndex('idx-clients-status', 'clients', 'status', false);
        $this->addForeignKey("fk-clients-status", "clients", "status", "statuses", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-clients-menejer','clients');
        $this->dropIndex('idx-clients-menejer','clients');  

        $this->dropForeignKey('fk-clients-source_id','clients');
        $this->dropIndex('idx-clients-source_id','clients');
        
        $this->dropForeignKey('fk-clients-status','clients');
        $this->dropIndex('idx-clients-status','clients');
        
        $this->dropTable('clients');
    }
}
