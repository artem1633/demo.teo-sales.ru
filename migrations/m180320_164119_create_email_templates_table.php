<?php

use yii\db\Migration;
use yii\helpers\Url;

/**
 * Handles the creation of table `email_templates`.
 */
class m180320_164119_create_email_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('email_templates', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->comment('Ключ'),
            'key_ru' => $this->string()->comment('Ключ на русском'),
            'body' => $this->text()->comment('Тело шаблона'),
            'deletable' => $this->boolean()->defaultValue(true)->comment('Удаляема сущность или нет'),
        ]);
        $this->addCommentOnTable('email_templates', 'Шаблоны для email уведомлений');

        $loginAddress = 'http://zvonki.teo-stroy.ru/site/login';
        $this->insert(
            'email_templates',
            [
                'key' => 'success_registration',
                'key_ru' => 'Успешная регистрация',
                'body' => 'Поздравляем вас с успешной регистрацией.<br>Можете авторизоваться по этой ссылке: <a href="'.$loginAddress.'">'.$loginAddress.'</a>',
                'deletable' => false,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('email_templates');
    }
}
