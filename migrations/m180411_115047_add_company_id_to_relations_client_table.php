<?php

use yii\db\Migration;

/**
 * Class m180411_115047_add_company_id_to_relations_client_table
 */
class m180411_115047_add_company_id_to_relations_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-relations_client-created_by',
            'relations_client'
        );

        $this->dropIndex(
            'idx-relations_client-created_by',
            'relations_client'
        );

        $this->dropColumn('relations_client', 'created_by');

        $this->addCompanyIdField('relations_client');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('relations_client');

        $this->addColumn('relations_client', 'created_by', $this->integer()->comment('Пользователь, который создал отношение'));

        $this->createIndex(
            'idx-relations_client-created_by',
            'relations_client',
            'created_by'
        );

        $this->addForeignKey(
            'fk-relations_client-created_by',
            'relations_client',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "SET NULL"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
