<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fields`.
 */
class m180727_062804_create_fields_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fields', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'label' => $this->string(255),
            'type' => $this->string(255),
            'data' => $this->text(),
            'project_id' => $this->integer(),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-fields-project_id', 'fields', 'project_id', false);
        $this->addForeignKey("fk-fields-project_id", "fields", "project_id", "project", "id");

        $this->createIndex('idx-fields-company_id', 'fields', 'company_id', false);
        $this->addForeignKey("fk-fields-company_id", "fields", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-fields-project_id','fields');
        $this->dropIndex('idx-fields-project_id','fields');

        $this->dropForeignKey('fk-fields-company_id','fields');
        $this->dropIndex('idx-fields-company_id','fields');

        $this->dropTable('fields');
    }
}
