<?php

use yii\db\Migration;

/**
 * Handles adding access_end_datetime to table `users`.
 */
class m180320_161803_add_access_end_datetime_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'access_end_datetime', $this->datetime()->comment('Дата и время потери доступа к системе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'access_end_datetime');
    }
}
