<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses`.
 */
class m180211_153505_create_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('statuses');
    }
}
