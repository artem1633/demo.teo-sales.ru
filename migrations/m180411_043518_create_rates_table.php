<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rates`.
 */
class m180411_043518_create_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
        ]);
        $this->addCommentOnTable('rates', 'Тарифы лицензий для компаний');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rates');
    }
}
