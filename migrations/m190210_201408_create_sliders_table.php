<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sliders`.
 */
class m190210_201408_create_sliders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sliders', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->comment('Заголовок'),
            'text' => $this->text()->comment('Текст'),
            'fone' => $this->string(255)->comment('Фон'),
            'order' => $this->integer()->comment('Сортировка'),
            'view' => $this->boolean()->comment('Показ'),
            'view_time' => $this->integer()->comment('Время показа в секундах'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sliders');
    }
}
