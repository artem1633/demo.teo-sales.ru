<?php

use yii\db\Migration;

/**
 * Handles adding lid_id to table `clients`.
 */
class m180707_083552_add_lid_id_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'lid_id', $this->integer());

        $this->createIndex('idx-clients-lid_id', 'clients', 'lid_id', false);
        $this->addForeignKey("fk-clients-lid_id", "clients", "lid_id", "status_lid", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-clients-lid_id','news');
        $this->dropIndex('idx-clients-lid_id','news');
        
        $this->dropColumn('clients', 'lid_id');
    }
}
