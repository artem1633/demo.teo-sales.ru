<?php

use yii\db\Migration;

/**
 * Handles adding id_vk to table `users`.
 */
class m180516_163025_add_id_vk_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'vk_id', $this->string(255));
        $this->addColumn('users', 'telegram_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'vk_id');
        $this->dropColumn('users', 'telegram_id');
    }
}
