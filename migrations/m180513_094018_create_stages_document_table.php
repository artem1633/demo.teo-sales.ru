<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stages_document`.
 */
class m180513_094018_create_stages_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('stages_document', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-stages_document-company_id', 'stages_document', 'company_id', false);
        $this->addForeignKey("fk-stages_document-company_id", "stages_document", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-stages_document-company_id','stages_document');
        $this->dropIndex('idx-stages_document-company_id','stages_document');  
        
        $this->dropTable('stages_document');
    }
}
