<?php

use yii\db\Migration;

/**
 * Handles the creation of table `templates`.
 */
class m180730_142420_create_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'content' => $this->text()->comment('Контент'),
            'created_at' => $this->dateTime(),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-templates-company_id', 'templates', 'company_id', false);
        $this->addForeignKey("fk-templates-company_id", "templates", "company_id", "companies", "id");

        $this->addCommentOnTable('templates', 'Шаблоны документов');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-templates-company_id','templates');
        $this->dropIndex('idx-templates-company_id','templates');
        
        $this->dropTable('templates');
    }
}
