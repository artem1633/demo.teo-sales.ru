<?php

use yii\db\Migration;

/**
 * Handles the creation of table `calling`.
 */
class m180211_154258_create_calling_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('calling', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'city' => $this->string(255),
            'email' => $this->string(255),
            'telephone' => $this->string(255),
            'status' => $this->integer(),
            'user_id' => $this->integer(),
            'planned_date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('calling');
    }
}
