<?php

use yii\db\Migration;

/**
 * Handles adding task_type_id to table `tasks`.
 */
class m180726_050502_add_task_type_id_column_to_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'tasks_type_id', $this->integer());

        $this->createIndex('idx-tasks-tasks_type_id', 'tasks', 'tasks_type_id', false);
        $this->addForeignKey("fk-tasks-tasks_type_id", "tasks", "tasks_type_id", "tasks_type", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-tasks_type_id','tasks');
        $this->dropIndex('idx-tasks-tasks_type_id','tasks');  
        
        $this->dropColumn('tasks', 'tasks_type_id');
    }
}
