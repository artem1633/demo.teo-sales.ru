<?php

use yii\db\Migration;

/**
 * Handles adding url to table `sites`.
 */
class m180819_054320_add_url_column_to_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sites', 'url', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sites', 'url');
    }
}
