<?php

use yii\db\Migration;

/**
 * Handles the creation of table `relations`.
 */
class m180215_170650_create_relations_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('relations', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'color' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('relations');
    }
}
