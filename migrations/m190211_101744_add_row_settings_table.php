<?php

use yii\db\Migration;

/**
 * Class m190211_101744_add_row_settings_table
 */
class m190211_101744_add_row_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'label' => 'Стоимость в день',
            'key' => 'cost_day',
            'value' =>'',
        ));
        $this->insert('settings',array(
            'label' => 'Стоимость за человека',
            'key' => 'cost_person',
            'value' =>'',
        ));
        $this->insert('settings',array(
            'label' => 'Принципы и условия работы партнерской программы',
            'key' => 'partner_programm',
            'value' =>'',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('settings',['key' => 'partner_programm']);
        $this->delete('settings',['key' => 'cost_person']);
        $this->delete('settings',['key' => 'cost_day']);



    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190211_101744_add_row_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
