<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `and_add_foreign_key_to_sites`.
 */
class m180707_103912_drop_and_add_foreign_key_to_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-sites-status','sites');
        $this->dropIndex('idx-sites-status','sites');

        $this->dropColumn('sites', 'status');
        $this->addColumn('sites', 'status', $this->integer());

        $this->createIndex('idx-sites-status', 'sites', 'status', false);
        $this->addForeignKey("fk-sites-status", "sites", "status", "status_lid", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('idx-sites-status', 'sites', 'status', false);
        $this->addForeignKey("fk-sites-status", "sites", "status", "statuses", "id");

        $this->dropForeignKey('fk-sites-status','sites');
        $this->dropIndex('idx-sites-status','sites');
    }
}
