<?php

use yii\db\Migration;

/**
 * Class m180213_070115_add_foreign_key
 */
class m180213_070115_add_foreign_key extends Migration
{
     public function up()
    {
        $this->createIndex('idx-calling-status', 'calling', 'status', false);
        $this->addForeignKey("fk-calling-status", "calling", "status", "statuses", "id");

    }

    public function down()
    {
        $this->dropForeignKey('fk-calling-status','calling');
        $this->dropIndex('idx-calling-status','calling');
        
    }
}
