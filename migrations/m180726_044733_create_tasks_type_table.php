<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks_type`.
 */
class m180726_044733_create_tasks_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks_type', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'color' => $this->string(10),
            'name' => $this->string(255),
        ]);

        $this->createIndex('idx-tasks_type-company_id', 'tasks_type', 'company_id', false);
        $this->addForeignKey("fk-tasks_type-company_id", "tasks_type", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks_type-company_id','tasks_type');
        $this->dropIndex('idx-tasks_type-company_id','tasks_type');  
        
        $this->dropTable('tasks_type');
    }
}
