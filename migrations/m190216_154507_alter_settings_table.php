<?php

use yii\db\Migration;

/**
 * Class m190216_154507_alter_settings_table
 */
class m190216_154507_alter_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('settings','value',$this->binary());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('settings','value',$this->text());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190216_154507_alter_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
