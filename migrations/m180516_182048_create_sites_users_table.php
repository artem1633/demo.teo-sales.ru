<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sites_users`.
 */
class m180516_182048_create_sites_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sites_users', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer(),
            'user_id' => $this->integer(),
            'type' => $this->string(20),
        ]);

        $this->createIndex('idx-sites_users-site_id', 'sites_users', 'site_id', false);
        $this->addForeignKey("fk-sites_users-site_id", "sites_users", "site_id", "sites", "id");

        $this->createIndex('idx-sites_users-user_id', 'sites_users', 'user_id', false);
        $this->addForeignKey("fk-sites_users-user_id", "sites_users", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-sites_users-site_id','sites_users');
        $this->dropIndex('idx-sites_users-site_id','sites_users');  

        $this->dropForeignKey('fk-sites_users-user_id','sites_users');
        $this->dropIndex('idx-sites_users-user_id','sites_users');  

        $this->dropTable('sites_users');
    }
}
