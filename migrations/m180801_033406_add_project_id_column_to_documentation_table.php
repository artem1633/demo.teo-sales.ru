<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `documentation`.
 */
class m180801_033406_add_project_id_column_to_documentation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documentation', 'project_id', $this->integer());

        $this->createIndex('idx-documentation-project_id', 'documentation', 'project_id', false);
        $this->addForeignKey("fk-documentation-project_id", "documentation", "project_id", "project", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-documentation-project_id','documentation');
        $this->dropIndex('idx-documentation-project_id','documentation');

        $this->dropColumn('documentation', 'project_id');
    }
}
