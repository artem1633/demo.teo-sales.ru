<?php

use yii\db\Migration;

/**
 * Class m180211_160607_add_foreign_keys
 */
class m180211_160607_add_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createIndex('idx-clients-menejer', 'clients', 'menejer', false);
        $this->addForeignKey("fk-clients-menejer", "clients", "menejer", "users", "id");

        $this->createIndex('idx-clients-source_id', 'clients', 'source_id', false);
        $this->addForeignKey("fk-clients-source_id", "clients", "source_id", "source", "id");

        $this->createIndex('idx-clients-status', 'clients', 'status', false);
        $this->addForeignKey("fk-clients-status", "clients", "status", "statuses", "id");

        $this->createIndex('idx-calling-user_id', 'calling', 'user_id', false);
        $this->addForeignKey("fk-calling-user_id", "calling", "user_id", "users", "id");
    }

    public function down()
    {
        $this->dropForeignKey('fk-clients-menejer','clients');
        $this->dropIndex('idx-clients-menejer','clients');  

        $this->dropForeignKey('fk-clients-source_id','clients');
        $this->dropIndex('idx-clients-source_id','clients');         

        $this->dropForeignKey('fk-clients-status','clients');
        $this->dropIndex('idx-clients-status','clients');

        $this->dropForeignKey('fk-calling-user_id','calling');
        $this->dropIndex('idx-calling-user_id','calling');          
    }
}
