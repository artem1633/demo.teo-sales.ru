<?php

use yii\db\Migration;

/**
 * Class m190211_081256_alter_users_table
 */
class m190211_081256_alter_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies','main_balance',
            $this->decimal(8,3)->comment('Баланс основной'));
        $this->addColumn('companies','partner_balance',
            $this->decimal(8,3)->comment('Баланс партнерский'));
        $this->addColumn('companies','prosent_referal',
            $this->decimal(8,3)->comment('Процент с оплат рефералов'));
        $this->addColumn('companies','cost_day',
            $this->decimal(8,3)->comment('Стоимость в день'));
        $this->addColumn('companies','cost_person',
            $this->decimal(8,3)->comment('Стоимость за человека'));
        $this->addColumn('companies','referal_id',
            $this->integer()->comment('Реферал'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies','main_balance');
        $this->dropColumn('companies','partner_balance');
        $this->dropColumn('companies','prosent_referal');
        $this->dropColumn('companies','cost_day');
        $this->dropColumn('companies','cost_person');
        $this->dropColumn('companies','referal_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190211_081256_alter_users_table cannot be reverted.\n";

        return false;
    }
    */
}
