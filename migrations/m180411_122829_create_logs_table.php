<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logs`.
 */
class m180411_122829_create_logs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logs', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'event_datetime' => $this->dateTime()->comment('Дата и время события'),
            'event' => $this->string()->comment('Наименование события'),
            'description' => $this->string()->comment('Описание события'),
        ]);
        $this->addCommentOnTable('logs', 'Логи действий пользователей');

        $this->createIndex(
            'idx-logs-user_id',
            'logs',
            'user_id'
        );

        $this->addForeignKey(
            'fk-logs-user_id',
            'logs',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-logs-user_id',
            'logs'
        );

        $this->dropIndex(
            'idx-logs-user_id',
            'logs'
        );

        $this->dropTable('logs');
    }
}
