<?php

use yii\db\Migration;

/**
 * Handles adding temporary_access_code to table `users`.
 */
class m180320_162116_add_temporary_access_code_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'temporary_access_code', $this->string()->comment('Временный код доступа, при потере пароля'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'temporary_access_code');
    }
}
