<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sites`.
 */
class m180514_062951_create_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sites', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'source_id' => $this->integer(),
            'project_id' => $this->integer(),
            'status' => $this->integer(),
            'menejer' => $this->integer(),
            'key' => $this->string(255),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-sites-menejer', 'sites', 'menejer', false);
        $this->addForeignKey("fk-sites-menejer", "sites", "menejer", "users", "id");

        $this->createIndex('idx-sites-source_id', 'sites', 'source_id', false);
        $this->addForeignKey("fk-sites-source_id", "sites", "source_id", "source", "id");

        $this->createIndex('idx-sites-status', 'sites', 'status', false);
        $this->addForeignKey("fk-sites-status", "sites", "status", "statuses", "id");

        $this->createIndex('idx-sites-project_id', 'sites', 'project_id', false);
        $this->addForeignKey("fk-sites-project_id", "sites", "project_id", "project", "id");

        $this->createIndex('idx-sites-company_id', 'sites', 'company_id', false);
        $this->addForeignKey("fk-sites-company_id", "sites", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-sites-menejer','sites');
        $this->dropIndex('idx-sites-menejer','sites');  

        $this->dropForeignKey('fk-sites-source_id','sites');
        $this->dropIndex('idx-sites-source_id','sites');
        
        $this->dropForeignKey('fk-sites-status','sites');
        $this->dropIndex('idx-sites-status','sites');

        $this->dropForeignKey('fk-sites-project_id','sites');
        $this->dropIndex('idx-sites-project_id','sites');

        $this->dropForeignKey('fk-sites-company_id','sites');
        $this->dropIndex('idx-sites-company_id','sites');

        $this->dropTable('sites');
    }
}
