<?php

use yii\db\Migration;

/**
 * Handles adding color to table `statuses`.
 */
class m180213_075641_add_color_column_to_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('statuses', 'color', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('statuses', 'color');
    }
}
