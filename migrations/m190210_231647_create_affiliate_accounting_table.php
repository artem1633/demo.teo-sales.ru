<?php

use yii\db\Migration;

/**
 * Handles the creation of table `affiliate_accounting`.
 */
class m190210_231647_create_affiliate_accounting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('affiliate_accounting', [
            'id' => $this->primaryKey(),
            'date_transaction' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время зачисления"',
            'company_id' => $this->integer()->notNull()->comment('ID компании (Кому начисляем)'),
            'referal_id' => $this->integer()->notNull()->defaultValue(0)->comment('ID реферала'),
            'amount' => $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Сумма партнерских отчислений'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('affiliate_accounting');
    }
}
