<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visitors`.
 */
class m180516_151847_create_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('visitors', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer(),
            'ip' => $this->string(255),
            'number' => $this->string(255),
            'utm' => $this->string(255),
            'link' => $this->string(255),
            'date_cr' => $this->date(),
        ]);

        $this->createIndex('idx-visitors-site_id', 'visitors', 'site_id', false);
        $this->addForeignKey("fk-visitors-site_id", "visitors", "site_id", "sites", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-visitors-site_id','visitors');
        $this->dropIndex('idx-visitors-site_id','visitors');  

        $this->dropTable('visitors');
    }
}
