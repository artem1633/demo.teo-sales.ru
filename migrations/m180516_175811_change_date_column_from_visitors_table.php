<?php

use yii\db\Migration;

/**
 * Class m180516_175811_change_date_column_from_visitors_table
 */
class m180516_175811_change_date_column_from_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('visitors', 'date_cr', 'datetime');//timestamp new_data_type
    }

    public function down()
    {
        $this->alterColumn('visitors', 'date_cr', 'date' );//int is old_data_type
    }
}
