<?php

use yii\db\Migration;

/**
 * Class m180422_114952_insert_in_email_templates_table
 */
class m180422_114952_insert_in_email_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('email_templates', [
            'key' => 'licence_expire',
            'key_ru' => 'Истечение лицензии',
            'body' => 'Уважаемая, компания «{admin.fio}». Ваш срок лицензии истек',
            'deletable' => 0,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
