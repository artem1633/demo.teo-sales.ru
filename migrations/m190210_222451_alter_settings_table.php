<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190210_222451_alter_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('settings','label',$this->string());
        $this->alterColumn('settings','value',$this->text());
        $this->insert('settings',array(
            'label' => 'Секретный ключ яндекс кошелька',
            'key' => 'key_yandex_kesh',
            'value' =>'',
        ));
        $this->insert('settings',array(
            'label' => 'Яндекс кошелек',
            'key' => 'cash_number',
            'value' =>'',
        ));
        $this->insert('settings',array(
            'label' => 'Партнерский процент при регистрации',
            'key' => 'partner_percent_register',
            'value' =>'',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       return true;
    }
}
