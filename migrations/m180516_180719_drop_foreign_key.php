<?php

use yii\db\Migration;

/**
 * Class m180516_180719_drop_foreign_key
 */
class m180516_180719_drop_foreign_key extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropForeignKey('fk-sites-applications_user','sites');
        $this->dropIndex('idx-sites-applications_user','sites');  

        $this->dropForeignKey('fk-sites-visitors_user','sites');
        $this->dropIndex('idx-sites-visitors_user','sites');  

        $this->dropColumn('sites', 'applications_user');
        $this->dropColumn('sites', 'visitors_user');

    }

    public function down()
    {
        $this->createIndex('idx-sites-applications_user', 'sites', 'applications_user', false);
        $this->addForeignKey("fk-sites-applications_user", "sites", "applications_user", "users", "id");

        $this->createIndex('idx-sites-visitors_user', 'sites', 'visitors_user', false);
        $this->addForeignKey("fk-sites-visitors_user", "sites", "visitors_user", "users", "id");

        $this->addColumn('sites', 'applications_user', $this->integer());
        $this->addColumn('sites', 'visitors_user', $this->integer());

        

    }
}
