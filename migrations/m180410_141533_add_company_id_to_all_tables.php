<?php

use yii\db\Migration;

/**
 * Class m180410_141533_add_company_id_to_all_tables
 */
class m180410_141533_add_company_id_to_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addCompanyIdField('relations');
        $this->addCompanyIdField('task_statuses');
        $this->addCompanyIdField('statuses');
        $this->addCompanyIdField('source');
        $this->addCompanyIdField('tasks');
        $this->addCompanyIdField('faq');
        $this->addCompanyIdField('changing');
        $this->addCompanyIdField('calling');
        $this->addCompanyIdField('pays');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('relations');
        $this->dropCompanyIdField('task_statuses');
        $this->dropCompanyIdField('statuses');
        $this->dropCompanyIdField('source');
        $this->dropCompanyIdField('tasks');
        $this->dropCompanyIdField('faq');
        $this->dropCompanyIdField('changing');
        $this->dropCompanyIdField('calling');
        $this->dropCompanyIdField('pays');
    }


    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "SET NULL"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
