<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `clients`.
 */
class m180512_092233_add_project_id_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'project_id', $this->integer());

        $this->createIndex('idx-clients-project_id', 'clients', 'project_id', false);
        $this->addForeignKey("fk-clients-project_id", "clients", "project_id", "project", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-clients-project_id','clients');
        $this->dropIndex('idx-clients-project_id','clients');  
        
        $this->dropColumn('clients', 'project_id');
    }
}
