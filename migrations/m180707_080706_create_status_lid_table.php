<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_lid`.
 */
class m180707_080706_create_status_lid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('status_lid', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'name' => $this->string(255),
            'color' => $this->string(255),
            'company_id' => $this->integer(),
        ]);

        $this->createIndex('idx-status_lid-company_id', 'status_lid', 'company_id', false);
        $this->addForeignKey("fk-status_lid-company_id", "status_lid", "company_id", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-status_lid-company_id','status_lid');
        $this->dropIndex('idx-status_lid-company_id','status_lid');

        $this->dropTable('status_lid');
    }
}
