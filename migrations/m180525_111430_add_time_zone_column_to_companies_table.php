<?php

use yii\db\Migration;

/**
 * Handles adding time_zone to table `companies`.
 */
class m180525_111430_add_time_zone_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'time_zone', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'time_zone');
    }
}
