<?php

use yii\db\Migration;

/**
 * Handles adding documents_type_id to table `documentation`.
 */
class m180513_100000_add_documents_type_id_column_to_documentation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documentation', 'documents_type_id', $this->integer());

        $this->createIndex('idx-documentation-documents_type_id', 'documentation', 'documents_type_id', false);
        $this->addForeignKey("fk-documentation-documents_type_id", "documentation", "documents_type_id", "documentstype", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-documentation-documents_type_id','documentation');
        $this->dropIndex('idx-documentation-documents_type_id','documentation');

        $this->dropColumn('documentation', 'documents_type_id');
    }
}
