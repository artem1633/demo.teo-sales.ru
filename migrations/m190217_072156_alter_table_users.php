<?php

use yii\db\Migration;

/**
 * Class m190217_072156_alter_table_users
 */
class m190217_072156_alter_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users','agree',$this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users','agree');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_072156_alter_table_users cannot be reverted.\n";

        return false;
    }
    */
}
