<?php

use yii\db\Migration;

/**
 * Handles dropping access_end_datetime from table `users`.
 */
class m180411_043143_drop_access_end_datetime_column_from_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('users', 'access_end_datetime');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('users', 'access_end_datetime', $this->datetime()->comment('Дата и время потери доступа к системе'));
    }
}
