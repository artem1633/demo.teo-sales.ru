<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `clients`.
 */
class m180213_045202_drop_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('clients');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'date_cr' => $this->date(),
            'fio' => $this->string(255)->notNull(),
            'city' => $this->string(255),
            'telephone' => $this->string(255),
            'email' => $this->string(255),
            'menejer' => $this->integer()->notNull(),
            'comment' => $this->text(),
            'client' => $this->boolean(),
            'source_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'access' => $this->text(),
            'planned_date' => $this->date(),
        ]);
    }
}
