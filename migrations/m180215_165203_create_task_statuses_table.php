<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_statuses`.
 */
class m180215_165203_create_task_statuses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task_statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'color' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task_statuses');
    }
}
