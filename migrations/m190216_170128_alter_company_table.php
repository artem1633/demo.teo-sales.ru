<?php

use yii\db\Migration;

/**
 * Class m190216_170128_alter_company_table
 */
class m190216_170128_alter_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies','start_fill',$this->smallInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('companies','start_fill');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190216_170128_alter_company_table cannot be reverted.\n";

        return false;
    }
    */
}
