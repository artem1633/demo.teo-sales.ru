<?php

use yii\db\Migration;

/**
 * Handles adding balance to table `clients`.
 */
class m180213_081234_add_balance_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'balance', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'balance');
    }
}
