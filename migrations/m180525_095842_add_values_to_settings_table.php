<?php

use yii\db\Migration;

/**
 * Class m180525_095842_add_values_to_settings_table
 */
class m180525_095842_add_values_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'key' => 'report_time',
            'value' => '09:00',
            'label' => 'Утренное время отчёта',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
