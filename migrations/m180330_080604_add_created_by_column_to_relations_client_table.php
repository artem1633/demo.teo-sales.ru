<?php

use yii\db\Migration;

/**
 * Handles adding created_by to table `relations_client`.
 */
class m180330_080604_add_created_by_column_to_relations_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('relations_client', 'created_by', $this->integer()->comment('Пользователь, который создал отношение'));

        $this->createIndex(
            'idx-relations_client-created_by',
            'relations_client',
            'created_by'
        );

        $this->addForeignKey(
            'fk-relations_client-created_by',
            'relations_client',
            'created_by',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-relations_client-created_by',
            'relations_client'
        );

        $this->dropIndex(
            'idx-relations_client-created_by',
            'relations_client'
        );

        $this->dropColumn('relations_client', 'created_by');
    }
}
