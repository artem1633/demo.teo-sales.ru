<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180424_174752_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'vk_access_token',
            'value' => null,
            'label' => 'Токен для бота VK',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
