<?php

use yii\db\Migration;

/**
 * Handles adding access_end_datetime to table `companies`.
 */
class m180411_043244_add_access_end_datetime_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'access_end_datetime', $this->datetime()->comment('Дата и время потери доступа к системе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'access_end_datetime');
    }
}
