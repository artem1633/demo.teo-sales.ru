<?php

use yii\db\Migration;

/**
 * Handles dropping telephone from table `clients`.
 */
class m180512_143610_drop_telephone_column_from_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('clients', 'telephone');
        $this->dropColumn('clients', 'email');
        $this->dropColumn('clients', 'access');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('clients', 'telephone', $this->string(255));
        $this->addColumn('clients', 'email', $this->string(255));
        $this->addColumn('clients', 'access', $this->text());
    }
}
