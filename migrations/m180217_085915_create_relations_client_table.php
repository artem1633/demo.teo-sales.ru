<?php

use yii\db\Migration;

/**
 * Handles the creation of table `relations_client`.
 */
class m180217_085915_create_relations_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('relations_client', [
            'id' => $this->primaryKey(),
            'data' => $this->date(),
            'time' => $this->time(),
            'relation' => $this->integer(),
            'text' => $this->text(),
            'date_cr' => $this->datetime(),
            'date_up' => $this->datetime(),
            'manager' => $this->integer(),
            'client' => $this->integer(),
        ]);

        $this->createIndex('idx-relations_client-relation', 'relations_client', 'relation', false);
        $this->addForeignKey("fk-relations_client-relation", "relations_client", "relation", "relations", "id");

        $this->createIndex('idx-relations_client-client', 'relations_client', 'client', false);
        $this->addForeignKey("fk-relations_client-client", "relations_client", "client", "clients", "id");

        $this->createIndex('idx-relations_client-manager', 'relations_client', 'manager', false);
        $this->addForeignKey("fk-relations_client-manager", "relations_client", "manager", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-relations_client-relation','relations_client');
        $this->dropIndex('idx-relations_client-relation','relations_client');  

        $this->dropForeignKey('fk-relations_client-client','relations_client');
        $this->dropIndex('idx-relations_client-client','relations_client'); 

        $this->dropForeignKey('fk-relations_client-manager','relations_client');
        $this->dropIndex('idx-relations_client-manager','relations_client'); 

        $this->dropTable('relations_client');
    }
}
