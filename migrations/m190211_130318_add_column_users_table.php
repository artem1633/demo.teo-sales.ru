<?php

use yii\db\Migration;

/**
 * Class m190211_130318_add_column_users_table
 */
class m190211_130318_add_column_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'register_date', $this->datetime()->comment('Дата регистрации'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('users','register_date');
    }


}
