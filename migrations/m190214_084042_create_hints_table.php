<?php

use yii\db\Migration;

/**
 * Handles the creation of table `hints`.
 */
class m190214_084042_create_hints_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('hints', [
            'id' => $this->primaryKey(),
                'name' => $this->string()->comment('Название'),
                'hint' => $this->text()->comment('Текст подсказки'),
        ]);
        $this->insert('hints',['name'=>'Создание анкеты','hint'=>'Создание анкеты']);
        $this->insert('hints',['name'=>'Общие елементы','hint'=>'Общие елементы']);
        $this->insert('hints',['name'=>'Поля','hint'=>'Поля']);
        $this->insert('hints',['name'=>'Шаблоны','hint'=>'Шаблоны']);
        $this->insert('hints',['name'=>'Сайты','hint'=>'Сайты']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hints');
    }
}
