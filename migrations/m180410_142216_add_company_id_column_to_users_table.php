<?php

use yii\db\Migration;

/**
 * Handles adding company_id to table `users`.
 */
class m180410_142216_add_company_id_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addCompanyIdField('users');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('users');
    }

    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "SET NULL"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
