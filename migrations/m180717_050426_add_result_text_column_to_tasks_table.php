<?php

use yii\db\Migration;

/**
 * Handles adding result_text to table `tasks`.
 */
class m180717_050426_add_result_text_column_to_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'result_text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'result_text');
    }
}
