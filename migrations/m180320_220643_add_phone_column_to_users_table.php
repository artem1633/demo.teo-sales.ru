<?php

use yii\db\Migration;

/**
 * Handles adding phone to table `users`.
 */
class m180320_220643_add_phone_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'phone', $this->string()->comment('Телефон'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'phone');
    }
}
