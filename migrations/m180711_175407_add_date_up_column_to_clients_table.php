<?php

use yii\db\Migration;

/**
 * Handles adding date_up to table `clients`.
 */
class m180711_175407_add_date_up_column_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'date_up', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('clients', 'date_up');
    }
}
