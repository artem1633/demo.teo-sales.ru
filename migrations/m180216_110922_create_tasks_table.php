<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m180216_110922_create_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'data' => $this->date(),
            'time' => $this->time(),
            'status' => $this->integer(),
            'text' => $this->text(),
            'date_cr' => $this->datetime(),
            'date_up' => $this->datetime(),
            'manager' => $this->integer(),
            'client' => $this->integer(),
        ]);
        $this->createIndex('idx-tasks-status', 'tasks', 'status', false);
        $this->addForeignKey("fk-tasks-status", "tasks", "status", "task_statuses", "id");

        $this->createIndex('idx-tasks-client', 'tasks', 'client', false);
        $this->addForeignKey("fk-tasks-client", "tasks", "client", "clients", "id");

        $this->createIndex('idx-tasks-manager', 'tasks', 'manager', false);
        $this->addForeignKey("fk-tasks-manager", "tasks", "manager", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-tasks-status','tasks');
        $this->dropIndex('idx-tasks-status','tasks');  

        $this->dropForeignKey('fk-tasks-client','tasks');
        $this->dropIndex('idx-tasks-client','tasks'); 

        $this->dropForeignKey('fk-tasks-manager','tasks');
        $this->dropIndex('idx-tasks-manager','tasks'); 

        $this->dropTable('tasks');
    }
}
