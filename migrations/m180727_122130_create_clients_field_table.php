<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_field`.
 */
class m180727_122130_create_clients_field_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients_field', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'field_id' => $this->integer(),
            'value' => $this->string(255),
        ]);

        $this->createIndex('idx-clients_field-client_id', 'clients_field', 'client_id', false);
        $this->addForeignKey("fk-clients_field-client_id", "clients_field", "client_id", "clients", "id");

        $this->createIndex('idx-clients_field-field_id', 'clients_field', 'field_id', false);
        $this->addForeignKey("fk-clients_field-field_id", "clients_field", "field_id", "fields", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-clients_field-client_id','clients_field');
        $this->dropIndex('idx-clients_field-client_id','clients_field');

        $this->dropForeignKey('fk-clients_field-field_id','clients_field');
        $this->dropIndex('idx-clients_field-field_id','clients_field');
        
        $this->dropTable('clients_field');
    }
}
