<?php

use yii\db\Migration;

/**
 * Class m180519_075602_add_proxy_server_to_settings_table
 */
class m180519_075602_add_proxy_server_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'key' => 'proxy_server',
            'value' => '148.251.238.124:1080',
            'label' => 'Proxy',
        ));
    }

    public function down()
    {

    }
}
