<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents_field`.
 */
class m180730_194332_create_documents_field_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documents_field', [
            'id' => $this->primaryKey(),
            'document_id' => $this->integer(),
            'field_id' => $this->integer(),
            'value' => $this->string(500),
        ]);

        $this->createIndex('idx-documents_field-document_id', 'documents_field', 'document_id', false);
        $this->addForeignKey("fk-documents_field-document_id", "documents_field", "document_id", "documentation", "id");

        $this->createIndex('idx-documents_field-field_id', 'documents_field', 'field_id', false);
        $this->addForeignKey("fk-documents_field-field_id", "documents_field", "field_id", "fields", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-documents_field-document_id','documents_field');
        $this->dropIndex('idx-documents_field-document_id','documents_field');

        $this->dropForeignKey('fk-documents_field-field_id','documents_field');
        $this->dropIndex('idx-documents_field-field_id','documents_field');
        
        $this->dropTable('documents_field');
    }
}
