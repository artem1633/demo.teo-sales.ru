<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lessons`.
 */
class m190214_084011_create_lessons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lessons}}', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer()->comment('Група'),
            'step' => $this->integer()->comment('№ шага'),
            'hint' => $this->text()->comment('Текст подсказки'),
        ]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 1,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 2,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 3,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 4,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 5,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 6,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 7,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 8,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 9,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 10,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 11,'hint' => 'Lesson1',]);
        $this->insert('{{%lessons}}',['group_id' => 1,'step' => 12,'hint' => 'Lesson1',]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lessons');
    }
}
