<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pays`.
 */
class m180211_155741_create_pays_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pays', [
            'id' => $this->primaryKey(),
            'data' => $this->datetime(),
            'summa' => $this->float()->notNull(),
            'comment' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pays');
    }
}
