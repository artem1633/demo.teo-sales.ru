<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_lid_statuses`.
 */
class m180709_062013_create_users_lid_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_lid_statuses', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status_lid_id' => $this->integer(),
        ]);

        $this->createIndex('idx-users_lid_statuses-status_lid_id', 'users_lid_statuses', 'status_lid_id', false);
        $this->addForeignKey("fk-users_lid_statuses-status_lid_id", "users_lid_statuses", "status_lid_id", "status_lid", "id");

        $this->createIndex('idx-users_lid_statuses-user_id', 'users_lid_statuses', 'user_id', false);
        $this->addForeignKey("fk-users_lid_statuses-user_id", "users_lid_statuses", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_lid_statuses-status_lid_id','users_lid_statuses');
        $this->dropIndex('idx-users_lid_statuses-status_lid_id','users_lid_statuses');  

        $this->dropForeignKey('fk-users_lid_statuses-user_id','users_lid_statuses');
        $this->dropIndex('idx-users_lid_statuses-user_id','users_lid_statuses');  
        
        $this->dropTable('users_lid_statuses');
    }
}
