<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_statuses`.
 */
class m180527_073555_create_users_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_statuses', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status_id' => $this->integer(),
        ]);

        $this->createIndex('idx-users_statuses-status_id', 'users_statuses', 'status_id', false);
        $this->addForeignKey("fk-users_statuses-status_id", "users_statuses", "status_id", "statuses", "id");

        $this->createIndex('idx-users_statuses-user_id', 'users_statuses', 'user_id', false);
        $this->addForeignKey("fk-users_statuses-user_id", "users_statuses", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_statuses-status_id','users_statuses');
        $this->dropIndex('idx-users_statuses-status_id','users_statuses');  

        $this->dropForeignKey('fk-users_statuses-user_id','users_statuses');
        $this->dropIndex('idx-users_statuses-user_id','users_statuses');  
        
        $this->dropTable('users_statuses');
    }
}
