<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `pays`.
 */
class m180706_074750_add_client_id_column_to_pays_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pays', 'client_id', $this->integer());

        $this->createIndex('idx-pays-client_id', 'pays', 'client_id', false);
        $this->addForeignKey("fk-pays-client_id", "pays", "client_id", "clients", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-pays-client_id','pays');
        $this->dropIndex('idx-pays-client_id','pays');

        $this->dropColumn('pays', 'client_id');
    }
}
