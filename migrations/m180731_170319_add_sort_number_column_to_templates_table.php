<?php

use yii\db\Migration;

/**
 * Handles adding sort_number to table `templates`.
 */
class m180731_170319_add_sort_number_column_to_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('templates', 'sort_number', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('templates', 'sort_number');
    }
}
