<?php

use yii\db\Migration;

/**
 * Class m180518_064128_add_telegram_access_token_to_settings_table
 */
class m180518_064128_add_telegram_access_token_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'key' => 'telegram_access_token',
            'value' => '508288308:AAFjfqet_ndbpgUbxPFm2b9gycn0XAy4mNU',
            'label' => 'Токен для бота телеграм',
        ));

        $this->update('settings', 
            array(
                'value' => 'c63737affb195896217c0a1fec77646b030cbe434c0629e79c8bfea94a3daad8265b07be8b5c8030591ac'
            ), 
            'id=1'
        );
    }

    public function down()
    {

    }
}
