<?php

use yii\db\Migration;

/**
 * Handles adding parent_company_id to table `users`.
 */
class m180320_161334_add_parent_company_id_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'parent_company_id', $this->integer()->comment('Компания, к которой пренадлежит пользователь'));

        $this->createIndex(
            'idx-users-parent_company_id',
            'users',
            'parent_company_id'
        );

        $this->addForeignKey(
            'fk-users-parent_company_id',
            'users',
            'parent_company_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-users-parent_company_id',
            'users'
        );

        $this->dropIndex(
            'idx-users-parent_company_id',
            'users'
        );

        $this->dropColumn('users', 'parent_company_id');
    }
}
