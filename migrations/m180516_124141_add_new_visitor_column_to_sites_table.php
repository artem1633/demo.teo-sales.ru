<?php

use yii\db\Migration;

/**
 * Handles adding new_visitor to table `sites`.
 */
class m180516_124141_add_new_visitor_column_to_sites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sites', 'new_visitor', $this->boolean());
        $this->addColumn('sites', 'new_application', $this->boolean());
        $this->addColumn('sites', 'notice_vk', $this->boolean());
        $this->addColumn('sites', 'notice_telegram', $this->boolean());
        $this->addColumn('sites', 'applications_user', $this->integer());
        $this->addColumn('sites', 'visitors_user', $this->integer());

        $this->createIndex('idx-sites-applications_user', 'sites', 'applications_user', false);
        $this->addForeignKey("fk-sites-applications_user", "sites", "applications_user", "users", "id");

        $this->createIndex('idx-sites-visitors_user', 'sites', 'visitors_user', false);
        $this->addForeignKey("fk-sites-visitors_user", "sites", "visitors_user", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-sites-applications_user','sites');
        $this->dropIndex('idx-sites-applications_user','sites');  

        $this->dropForeignKey('fk-sites-visitors_user','sites');
        $this->dropIndex('idx-sites-visitors_user','sites');  

        $this->dropColumn('sites', 'new_visitor');
        $this->dropColumn('sites', 'new_application');
        $this->dropColumn('sites', 'notice_vk');
        $this->dropColumn('sites', 'notice_telegram');
        $this->dropColumn('sites', 'applications_user');
        $this->dropColumn('sites', 'visitors_user');
    }
}
