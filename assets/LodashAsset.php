<?php

namespace app\assets;

use yii\web\AssetBundle;


/**
 * Class GraphAsset
 * @package app\assets
 */
class LodashAsset extends AssetBundle
{
    public $sourcePath = '@bower/lodash';
    public $css = [

        // external libs
    ];
    public $js = [
        'lodash.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
        'app\assets\ColorAdminAsset'
    ];
}