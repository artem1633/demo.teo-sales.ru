<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/AdminLTE.min.css',
        'css/style.css',
        'css/introjs.min.css',
    ];
    public $js = [
        'https://www.gstatic.com/firebasejs/4.9.1/firebase-app.js',
        'https://www.gstatic.com/firebasejs/4.9.1/firebase-auth.js',
        'https://www.gstatic.com/firebasejs/4.9.1/firebase-database.js',
        'https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js',
        'https://www.gstatic.com/firebasejs/4.9.1/firebase-messaging.js',
        'js/common.js',
        'firebase_subscribe.js',
        'js/intro.min.js',
        'app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
