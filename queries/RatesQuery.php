<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * Class RatesQuery
 * @package app\queries
 * @see \app\models\Rates
 */
class RatesQuery extends ActiveQuery
{
    /**
     * Делает выборку по тем тарифам, которые активны
     * @return $this
     */
    public function active()
    {


        return $this;
    }
}