<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Controller;


use app\models\Clients;
use app\models\Contacts;
use app\models\Sites;
use app\models\Visitors;
use app\models\SitesUsers;
use app\models\Settings;
use app\models\Tasks;
use app\models\Users;
use app\models\Companies;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['newclient','visitor','senduser', 'application', 'testpush', 'send-notification', 'send-tasks', 'test'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return ['' => ''];
    }

    public function actionVisitor()
    {
        if (isset($_GET['access']))
        {
            $site = Sites::find()->where([ 'key' => $_GET['access'] ])->one();
            if($site == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];

            //теперь нам нужно получить статус стоит ли у сайта получать информацию ссайта
            if (!$site->new_visitor) return ['status' => false, 'errors' => 'У вас отключено получение уведомлений'];
            //не забудьте еще сделать такуюже проверку в получении заявок
            $request1 = file_get_contents("http://api.sypexgeo.net/json/".$_SERVER['REMOTE_ADDR']);
            $ipCity = json_decode($request1);

            $visitor = new Visitors();
            $visitor->site_id = $site->id;
            $visitor->ip = $_SERVER['REMOTE_ADDR']; //$_GET['ip'];
            $visitor->utm = $ipCity->city->name_ru; //$_GET['utm'];
            $visitor->link = $_GET['link'];
            $visitor->date_cr = date('Y-m-d H:i:s');

            //Должен быть поиск среди предыдущих записях в Visitors, который возвращает количество записей(поиск делаеться по ip)
            $lastVisitor = Visitors::find()->where(['link'=> $_GET['link'], 'ip' => $_SERVER['REMOTE_ADDR']])->orderBy('id DESC')->one();
            //echo "f=".$lastVisitor;
            if($lastVisitor == null) $visitor->number = '1';
            else $visitor->number = ''.((integer)$lastVisitor->number + 1);
            $status = $visitor->save();

            if ($status) {
                //Делаем проверку на то что нужно ли отправялть заявки в вк
                if ($site->notice_vk) {
                    //получаем список пользователей которые выбраны для получения заявок
                    $users = SitesUsers::find()->where(['site_id' => $site->id, 'type' => 'visitor'])->all();
                    foreach ($users as $user) {
                        //после того как получили список пользователей отправляем их в функцию для отправки
                        $text  =
                            '_______________________________
                        посетитель c сайта '.$site->name.'
                        count: '.(string)$visitor->number.'    
                        ip: ' .$visitor->ip.'
                        sity: '.$ipCity->city->name_ru.'
                        link:'.$visitor->link.'
                        date time: '.$visitor->date_cr.'
                        ________________________________';
                        if(isset($user->user->vk_id))$this->pushvk($user->user->vk_id, $text);
                    }

                }
                //Делаем проверку на то что нужно ли отправялть заявки в телеграмм
                if ($site->notice_telegram) {
                    //получаем список пользователей которые выбраны для получения заявок
                    $users = SitesUsers::find()->where(['site_id' => $site->id, 'type' => 'visitor'])->all();
                    foreach ($users as $value) {
                        //после того как получили список пользователей отправляем их в функцию для отправки
                        $text  = '_______________________________%0Aпосетитель c сайта '.$site->name.'%0A';
                        $text .= "count: ".$visitor->number."%0A";
                        $text .= "ip: " .$visitor->ip."%0A";
                        $text .= "sity: " .$ipCity->city->name_ru."%0A";
                        $text .= "link: ".$visitor->link."%0A";
                        $text .= "date time: ".$visitor->date_cr."%0A________________________________%0A";

                        if($value->user->telegram_id != '') $this->pushtelegram($value->user->telegram_id, $text);
                    }
                }
            }

            return ['status' => $status, 'errors' => $visitor->errors];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

    public function actionNewclient()
    {
        if (isset($_GET['access']))
        {
            $site = Sites::find()->where([ 'key' => $_GET['access'] ])->one();
            if($site == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];

            //тут нужно сделать поиск по ключу среди сайтов и получить данные, о компании
            // если нету такого ключа выдаем ошибку доступа
            // после того как получили код компании начинаем создовать клиента

            //Нужна еще проверка существует ли данная компания
            // Данные все кроме -телефона,имя, и емаила берем из найденого ключа
            $request1 = file_get_contents("http://api.sypexgeo.net/json/".$_SERVER['REMOTE_ADDR']);
            $ipCity = json_decode($request1);

            $client = new Clients();
            $client->date_cr = date('Y-m-d');
            $client->fio = $_GET['name'];
            $client->utm = $_GET['utm'];
            if (isset($_GET['id_send'])) {
                $client->id_send = $_GET['id_send'];
            }
            $client->menejer = $site->menejer;
            $client->client = 0;
            if ($_GET['city']) {
                $client->city = $_GET['city'];
            } else {
                $client->city = $ipCity->city->name_ru;
            }
            $client->source_id = $site->source_id;
            $client->lid_id = $site->status;
            $client->project_id = $site->project_id;
            $client->company_id = $site->company_id;

            $visitor = Visitors::find()->where([ 'site_id' => $site->id ])->orderBy('id DESC')->one();
            if($visitor != null){
                $client->ip = $visitor->ip;
                //$client->utm = $visitor->utm;
                $client->link = $visitor->link;
            }

            // не забудьте то телефона,имя, и емаила еще сохраняем как контакт клиента

            // должна подставляться компания из найденого ключа
            $status = $client->save();
            if($status)
            {
                $contact = new Contacts();
                $contact->fio = $_GET['name'];
                $contact->phone = $_GET['phone'];
                $contact->email = $_GET['email'];
                $contact->position = ' ';
                $contact->client_id = $client->id;
                $contact->save();

                //Делаем проверку на то что нужно ли отправялть заявки в вк
                if ($site->notice_vk) {
                    //получаем список пользователей которые выбраны для получения заявок
                    $users = SitesUsers::find()->where(['site_id' => $site->id, 'type' => 'application'])->all();
                    foreach ($users as $user) {
                        //после того как получили список пользователей отправляем их в функцию для отправки
                        $text  =
                            '_______________________________
                        Заявка c сайта '.$site->name.'
                        Имя:'.$contact->fio.'    
                        E-mail:'.$contact->email.'
                        phone:'.$contact->phone.'
                        utm:'.$client->utm.'
                        ip:'.$client->ip.'
                        link:'.$client->link.'
                        date time: '.$visitor->date_cr.'
                        ________________________________';
                        if(isset($user->user->vk_id))$this->pushvk($user->user->vk_id, $text);
                    }

                }
                //Делаем проверку на то что нужно ли отправялть заявки в телеграмм
                if ($site->notice_telegram) {
                    //получаем список пользователей которые выбраны для получения заявок
                    $users = SitesUsers::find()->where(['site_id' => $site->id, 'type' => 'application'])->all();
                    foreach ($users as $value) {
                        //после того как получили список пользователей отправляем их в функцию для отправки
                        $text  = '_______________________________%0AЗаявка c сайта '.$site->name.'%0A';
                        $text .= "Имя: ".$contact->fio."%0A";
                        $text .= "E-mail: ".$contact->email."%0A";
                        $text .= "phone: ".$contact->phone."%0A";
                        $text .= "utm: ".$client->utm."%0A";
                        $text .= "ip: " .$client->ip."%0A";
                        $text .= "link: ".$client->link."%0A";
                        $text .= "date time: ".$visitor->date_cr."%0A________________________________%0A";

                        if($value->user->telegram_id != '') $this->pushtelegram($value->user->telegram_id, $text);
                    }
                }
            }

            return ['status' => $status, 'errors' => $client->errors];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

    public function actionSendNotification()
    {
        $tasks = Tasks::find()->where(['data' => date('Y-m-d'), 'notification_status' => null])->all();
        $massiv = [];

        foreach($tasks as $task)
        {
            $time_zone = $task->manager0->company->time_zone;
            $report_time = $task->manager0->company->report_time;
            //$currentDateTime = strtotime(date('d.m.Y ' . $report_time, strtotime( $time_zone . ' hour')));
            $currentDateTime = strtotime(date('Y-m-d H:i:s')) + 3600 * $time_zone;
            $taskDateTime = strtotime($task->notification_time);
            //$timeDiff = $taskDateTime - $currentDateTime;
            /*echo "date = " . $taskDateTime;
            echo "<br>date = " . $currentDateTime;die;*/
            if($taskDateTime <= $currentDateTime)
            {
                $massiv [] = $task->id;
                $text  =
                    '_______________________________
                Сегодня у вас есть задача которого вы должны сделать до '.$task->data . ' ' . $task->time.'
                Задача №:'.$task->id.'
                Клиент:'.$task->client0->fio.'
                Статус:'.$task->status0->name.'
                Текст:'.$task->text.'
                Ссылка : '. 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id.'
                ________________________________';

                if(isset($task->manager0->vk_id)) $this->pushvk($task->manager0->vk_id, $text);

                $text  = '_______________________________%0AСегодня у вас есть задача которого вы должны сделать до '.$task->data . ' ' . $task->time.'%0A';
                $text .= "Задача №: ".$task->id."%0A";
                $text .= "Клиент: ".$task->client0->fio."%0A";
                $text .= "Статус: ".$task->status0->name."%0A";
                $text .= "Текст: ".$task->text."%0A";
                $text .= "Ссылка: ".'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id."%0A";
                $text .= "%0A________________________________%0A";

                if(isset($task->manager0->telegram_id)) $this->pushtelegram($task->manager0->telegram_id, $text);

                $task->notification_status = 1;
                $task->save();
            }
        }

        return ['status' => 'Успешно выполнено', 'tasks' => $massiv];
    }

    public function actionSendTasks()
    {
        $companies = Companies::find()->all();
        $massiv = [];

        foreach ($companies as $company)
        {
            if($company->access_end_datetime === null || (new \DateTime($company->access_end_datetime))->getTimestamp() > time())
            {
                if($company->report_date == date('Y-m-d'))
                {
                    $company_report_time = strtotime( date('Y-m-d ') .  $company->report_time ) /*+ 3600 * $company->time_zone*/;
                    $now = strtotime(date('d.m.Y H:i:s', strtotime( $company->time_zone . ' hour')));
                    //echo "noc=".date('Y-m-d H:i:s', $now) . '<br>sec=' . $company_report_time;die;

                    if( $company_report_time < $now )
                    {
                        $users = Users::find()->where(['type' => Users::USER_TYPE_MANAGER, 'company_id' => $company->id])->all();
                        foreach ($users as $user)
                        {
                            $today_tasks_vk = '_______________________________<br> '. date('Y-m-d') . ' План на день <br> У вас задачи на сегодня<br>';
                            $last_tasks_vk = 'Просроченные задачи<br>';
                            $today_tasks_tl = '_______________________________%0A ' . date('Y-m-d') . ' План на день %0A У вас задачи на сегодня%0A';
                            $last_tasks_tl = 'Просроченные задачи%0A';

                            $tasks = Tasks::find()->where(['manager' => $user->id])->all();
                            foreach($tasks as $task)
                            {
                                $massiv [] = $task->id;
                                $currentDateTime = strtotime(date('Y-m-d H:i:s')) + 3600 * $company->time_zone;
                                $taskDateTime = strtotime($task->data . ' ' . $task->time);

                                if($task->data == date('Y-m-d'))
                                {
                                    $today_tasks_vk .= '- Задача №' . $task->id . '; Клиент :' . $task->client0->fio . '; Ссылка : '. 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id . '<br>';
                                    $today_tasks_tl .= '- Задача №' . $task->id . '; Клиент :' . $task->client0->fio . '; Ссылка : '. 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id . '%0A';
                                }

                                if($currentDateTime > $taskDateTime)
                                {
                                    $last_tasks_vk .= '- Задача №' . $task->id . '; Клиент :' . $task->client0->fio . '; Ссылка : '. 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id . '<br>';
                                    $last_tasks_tl .= '- Задача №' . $task->id . '; Клиент :' . $task->client0->fio . '; Ссылка : '. 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id . '%0A';
                                }
                            }

                            $today_tasks_vk .= '<br><br>' . $last_tasks_vk. '<br>' . '_______________________________';
                            $today_tasks_tl .= '%0A%0A' . $last_tasks_tl. '%0A' . '_______________________________';

                            if(isset($user->vk_id)) $this->pushvk($user->vk_id, $today_tasks_vk);
                            if(isset($user->telegram_id)) $this->pushtelegram($user->telegram_id, $today_tasks_tl);

                            $company->report_date = date('Y-m-d', strtotime('+1 day'));
                            $company->save();
                        }
                    }
                }
                else
                {
                    $company->report_date = date('Y-m-d', strtotime('+1 day'));
                    $company->save();
                }
            }
        }

        return ['status' => 'Успешно выполнено', 'tasks' => $massiv];
    }

    public function actionTestpush()
    {
        $userVk = "252341158";
        $userTel = '247187885';
        $text = 'testPush';

        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => $userVk,    // Кому отправляем
            'message' => $text,   // Что отправляем
            'access_token' => $token->value, //'c63737affb195896217c0a1fec77646b030cbe434c0629e79c8bfea94a3daad8265b07be8b5c8030591ac',  // берем из настроек
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));



        $token = Settings::find()->where(['key' => 'telegram_access_token'])->one();
        //token=508288308:AAFjfqet_ndbpgUbxPFm2b9gycn0XAy4mNU;
        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one();
        //$proxy = "148.251.238.124:1080";
        $proxy = $proxy_server->value;
        $url = 'https://api.telegram.org/bot'.$token->value.'/sendMessage?chat_id='.$userTel.'&disable_web_page_preview=true&text='.$text;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }

    public function Pushtelegram($user_id, $text)
    {
        $token = Settings::find()->where(['key' => 'telegram_access_token'])->one();
        //token=508288308:AAFjfqet_ndbpgUbxPFm2b9gycn0XAy4mNU;
        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one();
        //$proxy = "148.251.238.124:1080";
        $proxy = $proxy_server->value;
        $url = 'https://api.telegram.org/bot'.$token->value.'/sendMessage?chat_id='.$user_id.'&disable_web_page_preview=true&text='.$text;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        //echo $curl_scraped_page;
    }

    public function Pushvk($user_id, $text)
    {
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            'user_id' => $user_id,    // Кому отправляем
            'message' => $text,   // Что отправляем
            'access_token' => $token->value, //'c63737affb195896217c0a1fec77646b030cbe434c0629e79c8bfea94a3daad8265b07be8b5c8030591ac',  // берем из настроек
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
    }

    public function actionNotification()
    {
        $sites = Sites::find()->all();
        foreach ($sites as $site)
        {
            //$url = 'http://teo-pact.ru/';
            $url = $site->url;
            $site_id = $site->id;
            $vk_message = $site->name . ' сайт не доступен';
            $telegram_message = $site->name . ' сайт не доступен';
            $result = file_get_contents($url, false);
            if(!$result)
            {
                $sites_user = SitesUsers::find()->where(['type' => 'application', 'site_id' => $site_id])->all();
                foreach ($sites_user as $user)
                {
                    $user_id = $user->user_id;
                    if(isset($user->user->vk_id)) $this->pushvk($user->user->vk_id, $vk_message);
                    if(isset($user->user->telegram_id)) $this->pushtelegram($user->user->telegram_id, $telegram_message);
                }

                $admin = Users::find()->where(['company_id' => $site->company_id, 'type' => 1 ])->one();
                if(isset($admin->vk_id)) $this->pushvk($admin->vk_id, $vk_message);
                if(isset($admin->telegram_id)) $this->pushtelegram($admin->telegram_id, $telegram_message);
            }
        }
    }

    public function actionCheckLicense()
    {
        //Проверить лицензию
    $companies = Companies::find()->all();
    $resultMessage = '';
    foreach ($companies as $company)
    {
            //Если просрочка
            if($company->access_end_datetime === null || (new \DateTime($company->access_end_datetime))->getTimestamp() > time()){
            } else {
                //Определить цену и проверить баланс
                $costCompany = $company->cost_day;
                $costPerson = $company->cost_person;
                $countPerson = Users::find()->where(['company_id'=>$companyId])->count();
                $allCost = $costCompany + $countPerson*$costPerson;
                $balance = $company->main_balance;
                //Если баланс позволяет то снять средства
                if ($balance>$allCost) {
                    $company->main_balance -= $allCost;
                    $endDate = time() + 26*60*60;
                    $company->access_end_datetime = date("Y-m-d H:i:s",$endDate);
                    $company->save();
                    $report = new AccountingReport([
                        'company_id' => $company->id,
                        'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                        'amount' => $allCost,
                        'description' => 'Списание с личного счета. Плата за день',
                    ]);
                    $report->save();
                    $resultMessage.= 'Списание: '.$allCost. ' Компания: '. Users::findOne($company->admin_id)->fio.'<br>';
                } else {
                    $resultMessage.= 'Не достаточно средств: '.$allCost. ' Компания: '. Users::findOne($company->admin_id)->fio.'<br>';
                }
            }
    }
return $resultMessage;



    }

}
