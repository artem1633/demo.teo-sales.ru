<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use app\models\AffiliateAccounting;
use app\models\Companies;
use app\models\Settings;
use Yii,
    yii\rest\ActiveController,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException,
    app\models\YandexMoney,
    app\models\YandexLog,
    app\models\CompanySettings,
    app\models\Users;


/**
 * Class YandexController
 * @package app\modules\api\controllers
 *
 * @property Client $model
 */
class YandexController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['confirm-pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * @return string
     */
    public function actionConfirmPay()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $configure = Settings::find()->where(['key'=>'key_yandex_kesh'])->one();
            if ($post['label']) echo $post['label'];
            if ($company = Companies::findOne($post['label'])) {
                $sha1_res = sha1($post['notification_type']
                    . '&' . $post['operation_id']
                    . '&' . $post['amount']
                    . '&' . $post['currency']
                    . '&' . $post['datetime']
                    . '&' . $post['sender']
                    . '&' . $post['codepro']
                    . '&' . $configure->value
                    . '&' . $post['label']);

                if ($sha1_res == $post['sha1_hash']) {
                    $company->main_balance += floatval($post['withdraw_amount']);
                    if ($company->save()) {
                        $report = new AccountingReport([
                            'company_id' => $company->id,
                            'operation_type' => AccountingReport::TYPE_INCOME_BALANCE,
                            'amount' => floatval($post['withdraw_amount']),
                            'description' => 'Пополнение личного счета. Квитанция №' . $post['operation_id']
                        ]);

                        /*$vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->text;
                        $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->text;
                        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->text;

                        ClientController::request('messages.send', $proxy, [
                            'access_token' => $token,
                            'user_id' => $vk_id,
                            'message' => "Пополнение лицевого счета компании «{$company->company_name}» на сумму ".floatval($post['withdraw_amount'])." руб
                        Квитанция №{$post['operation_id']}",
                        ]);*/

                        $report->save();
                    }

                    $referal = Companies::findOne( $company->referal_id);
                    if ($referal) {
                        $user =Users::findOne( $referal->admin_id);
                        $persent = ($post['withdraw_amount'] / 100 * floatval($referal->prosent_referal));
                        $referal->partner_balance += $persent;
                        if ($referal->save()) {
                            $reportRef = new AccountingReport([
                                'company_id' => $referal->id,
                                'operation_type' => AccountingReport::TYPE_INCOME_AFFILIATE,
                                'amount' => $persent,
                                'description' => 'Партнерские отчисления от компании ' . $user->fio,
                            ]);
                            $reportAff = new AffiliateAccounting([
                                'company_id' => $referal->id,
                                'referal_id' => $company->id,
                                'amount' => $persent,
                            ]);
                            $reportAff->save();
                            $reportRef->save();
                        }
                    }
                }
            }
        }
    }

}
