<?php

namespace app\modules\pushing\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Tasks;

/**
 * Default controller for the `pushing` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Уведомляет всех пользователей о срочных задачах
     * По умолчанию срочная задачей, является та задача,
     * которая начнется через 5 минут (300 милисекунд)
     * @return mixed
     */
    public function actionUrgentTasksNotify($reduceTime = 300)
    {
    	Yii::$app->response->format = Response::FORMAT_JSON;
    	$response = [];
        $user = Yii::$app->user->identity;
        $users = Users::find()->hasToken()->all();
        $usersTokens = array_values(ArrayHelper::map($users, 'id', 'fcm_token'));
        $tasks = Tasks::find()->where(['data' => date('Y-m-d')])->all();

        foreach($tasks as $task)
        {
            $currentDateTime = strtotime(date('Y-m-d H:i:s'));
            $taskDateTime = strtotime($task->data.' '.$task->time);
            $timeDiff = $taskDateTime - $currentDateTime;

            if(($timeDiff < $reduceTime) && ($taskDateTime > $currentDateTime))
            {
                $reduce = $timeDiff > 60 ? round($timeDiff/60, 0).' мин.' : $timeDiff.' сек.';

                $response = Yii::$app->pusher
                  ->addReceivers($usersTokens)
                  ->compose([
                      'title' => 'Задача начнется через '.$reduce,
                      'body' => $task->text,
                      'icon' => 'https://eralash.ru.rsz.io/sites/all/themes/eralash_v5/logo.png?width=192&height=192',
                      'click_action' => 'http://eralash.ru/',
                  ])->send();
            }
        }

        return $response;
    }
}
