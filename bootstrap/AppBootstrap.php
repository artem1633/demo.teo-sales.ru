<?php

namespace app\bootstrap;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class AppBootstrap
 * @package app\bootstrap
 */
class AppBootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if(Yii::$app->user->isGuest === false)
        {
            $user = Yii::$app->user->identity;
            $company = $user->getCompanyInstance();
            $user->last_activity_datetime = date('Y-m-d H:i:s');
            $user->save(false);

            if($company != null){
                $company->last_activity_datetime = date('Y-m-d H:i:s');
                $company->save(false);
            }
        }
    }
}